<?php
	/**
	 * Validator
	 *
	 * Extending the Laravel Validator class allows us to define more powerful validation functions than
	 * with the Validator::register method.
	 */
	class Validator extends Laravel\Validator {
	
	
	
	
		/**
		 * Implicit Attributes
		 *
		 * Attributes with these rules will be validated even if no value is supplied.
		 */
		protected $implicit_attributes = array(
			'required',
			'accepted',
			'required_if_attribute'
		);
	
	
	
	
		/**
		 * Implicit
		 *
		 * By default Laravel will only validate an attribute if a value was supplied, or if the rule set is 'required' or 'accepted'.
		 * It'll just skip the validation, which makes 'require' being conditional impossible. Let's overwrite that to be more flexible.
		 */
		protected function implicit($rule) {
			return (in_array($rule, $this->implicit_attributes));
		}
		/**
		 * Required if attribute
		 *
		 * Validate that a required attribute exists, only if another
		 * attribute satisfies the supplied conditions.
		 *
		 * i.e. 'What is your favourite PHP framework?' is only required if
		 * 'Do you use a framework?' is set to '1'.
		 */
		public function validate_required_if_attribute($attribute, $value, $parameters) {
		
			$required = false;
		
			switch($parameters[1]) {
			
				case '==':
					$required = $this->attributes[$parameters[0]] == $parameters[2];
					
				case '!=':
					$required = $this->attributes[$parameters[0]] != $parameters[2];
				
				case '===':
					$required = $this->attributes[$parameters[0]] === $parameters[2];
				
				case '!==':
					$required = $this->attributes[$parameters[0]] !== $parameters[2];
					
				case '<':
					$required = $this->attributes[$parameters[0]] < $parameters[2];
					
				case '<=':
					$required = $this->attributes[$parameters[0]] <= $parameters[2];
					
				case '>':
					$required = $this->attributes[$parameters[0]] > $parameters[2];
					
				case '>=':
					$required = $this->attributes[$parameters[0]] >= $parameters[2];
				
			}
			
			return $required ? $this->validate_required($attribute, $value) : true;
		}
	
	
	
	
	}
?>