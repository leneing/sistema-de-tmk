<?php

include "conectasql.php";

$nomes_filtro = "SELECT * FROM `evento` WHERE NOME LIKE '%".trim(strip_tags(utf8_decode($_GET['term'])))."%'  UNION SELECT * FROM `evento_nome_popular` WHERE NOME LIKE '%".trim(strip_tags(utf8_decode($_GET['term'])))."%' ";

$res_nomes = $conexao ->query($nomes_filtro);
$nomes = array();

$i = 0;
while ($n = $res_nomes -> fetch_assoc()) {
    $nomes[$i]["id"] = utf8_encode($n['id']);
    $nomes[$i]["label"] = utf8_encode($n['NOME']);
    $nomes[$i]["value"] = utf8_encode($n['NOME']);
    $i = $i + 1;
}


echo json_encode($nomes);

?>