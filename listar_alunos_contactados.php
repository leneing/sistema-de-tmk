<?php
error_reporting(E_WARNING);
include "conectasql.php";
session_start();

$cursos_ativos = "SELECT DISTINCT e.* FROM `evento_ativo` e where e.ativo = 1";
$res_curso_ativo = $conexao ->query($cursos_ativos);
$res_curso_ativo2 = $conexao ->query($cursos_ativos);
$res_curso_ativo3 = $conexao ->query($cursos_ativos);
$res_curso_ativo4 = $conexao ->query($cursos_ativos);

$cursos_cap  = "SELECT DISTINCT e.id, e.nome, e.ativo, MAX( t.data_inicial ) 
                    FROM evento_ativo e
                    INNER JOIN turma t ON t.id_evento = e.id
                    WHERE t.cidade = LOWER(  'Belo Horizonte' ) 
                    AND t.data_inicial >= DATE_SUB( NOW( ) , INTERVAL 4 YEAR ) 
                    AND e.nome LIKE  '%CAP%'
                    GROUP BY e.id, e.nome, e.ativo";
$res_cursos_cap = $conexao ->query($cursos_cap);

$cursos_cl = "SELECT DISTINCT e.id, e.nome, e.ativo, MAX( t.data_inicial ) 
                FROM evento_ativo e
                INNER JOIN turma t ON t.id_evento = e.id
                WHERE t.cidade = LOWER(  'Belo Horizonte' ) 
                AND t.data_inicial >= DATE_SUB( NOW( ) , INTERVAL 4 YEAR ) 
                AND e.nome LIKE  '%CL %'
                GROUP BY e.id, e.nome, e.ativo";
$res_cursos_cl = $conexao ->query($cursos_cl);

?>

<HTML>

<HEAD>
        <TITLE>IIPC BH - TMK</TITLE>

        <link rel="stylesheet" href="jQuery-Plugin-For-Multi-Select-Checboxes-multiselect/css/jquery.multiselect.css">

        <link rel="stylesheet" href="bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="estilo.css">

        <script src="jquery-3.3.1.min.js" crossorigin="anonymous"></script>
        <script src="jQuery-Plugin-For-Multi-Select-Checboxes-multiselect/js/jquery.multiselect.js"></script>
        <script src="jquery-ui.js"></script>
        <script src="combobox.js"></script>


        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

        <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

        <style type="text/css">
            .form-center{   
                width: 20%;
                padding: .375rem .75rem;
                font-size: 1rem;
                line-height: 1.5;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                border-radius: .25rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            }
            .form-radius{  
                font-size: 1rem;
                line-height: 1.5;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                border-radius: .25rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            }

            fieldset.scheduler-border {   
                text-align: center;
                background-color: #f7f9fc;
                min-width: 200px;             
                padding: 10px !important;
                -webkit-box-shadow:  0px 0px 0px 0px #000;
                        box-shadow:  0px 0px 0px 0px #000;
            }
        </style>

        <script>
                var counterE = 1;
                var id_e = [];
                function addInputE(divName){
                        counterE++;
                        var newdiv = document.createElement('div');
                        newdiv.id = "input_evento_child"+counterE;
                        newdiv.innerHTML = " <br><input id='select_evento"+ counterE +"' style='background-color: white;' type='text' class='form-control input-lg interesse_extra' name='select_evento[]'>";
                        
                       document.getElementById(divName).appendChild(newdiv);

                        $( "#select_evento"+counterE+"").autocomplete({
                                source: "listacurso.php",
                                minLength: 3,
                                select: function (event, ui) {
                                        id_e[counterE] = ui.item.id;
                                }
                        });  
                }

                function removeInputE(){
                        if (counterE > 1) {
                                $( "#input_evento_child"+counterE ).remove();
                                id_e[counterE] = '';
                                counterE--;
                        }else{alert("É necessário preencher ao menos um campo de interesse!");}
                }

                var counterCI = 1;
                var id_CI = [];
                function addInputCI(divName){
                        counterCI++;
                        var newdiv = document.createElement('div');
                        newdiv.id = "input_curso_interesse_child"+counterCI;
                        newdiv.innerHTML = " <br><input id='select_curso_interesse"+ counterCI +"' style='background-color: white;' type='text' class='form-control input-lg interesse_extra' name='select_curso_interesse[]'>";
                        
                       document.getElementById(divName).appendChild(newdiv);

                        $( "#select_curso_interesse"+counterCI+"").autocomplete({
                                source: "listacurso.php",
                                minLength: 3,
                                select: function (event, ui) {
                                        id_CI[counterCI] = ui.item.id;
                                }
                        });  
                }



                 function removeInputCI(){
                        if (counterCI > 1) {
                                $( "#input_curso_interesse_child"+counterCI ).remove();
                                id_CI[counterCI] = '';
                                counterCI--;
                        }else{alert("É necessário preencher ao menos um curso de interesse!");}
                }
      

                function busca_turma(){
                    if(id_e[1] != "" ){
                         $("#select_evento1").val(id_e[1]); 
                         $.ajax({
                                url: 'busca_turma.php',
                                data: "select_evento="+id_e[1],
                                type: "post",
                                success: function(resusltado_busca_turma){
                                     $("#div_turma_lista").html(resusltado_busca_turma);
                                }
                        }); 
                    }
                }

                $(document).ready(function () {   

                        $("#resultados").click(function () {                            

                            $("#frm_resultado")[0].reportValidity();
                            if ($("#frm_resultado")[0].checkValidity()) {
                                for (i=1;i<counterCI+1;i++){
                                    $("#select_curso_interesse"+i+"").val(id_CI[i]); 
                                } 
                                
                                $("#select_evento1").val(id_e[1]); 
                                $.ajax({
                                    url: 'resultadoListaContactados.php',
                                    type: 'post',
                                    data: $("#frm_resultado").serialize(),
                                    success: function(resultado_filtro){
                                        alert("Lista gerada com sucesso!");
                                        $("#frm_resultado")[0].reset();
                                        //location.href = "geralistas.php";
                                    
                                    }

                                });
                            }
                                return false;
                        });  
                        $("#data_inicial").datepicker({
                                dateFormat: 'dd/mm/yy',
                                dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                                dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                                dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                                monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                                monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                                nextText: 'Próximo',
                                prevText: 'Anterior'
                        });             
                        $("#data_fim").datepicker({
                                dateFormat: 'dd/mm/yy',
                                dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                                dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                                dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                                monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                                monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                                nextText: 'Próximo',
                                prevText: 'Anterior'
                        }); 
                        
                        $( "#select_evento1" ).autocomplete({
                                source: "listacurso.php",
                                minLength: 3,
                                select: function (event, ui) {
                                        id_e[1] = ui.item.id; 
                                }
                        });  
                        $( "#select_curso_interesse1" ).autocomplete({
                                source: "listacurso.php",
                                minLength: 3,
                                select: function (event, ui) {
                                        id_CI[1] = ui.item.id; 
                                }
                        });   
                    });
        </script>
</HEAD>

<BODY class="bg-light">
        <?php
            include "navbar_adm.php";
        ?> 
        <div class="container" style="margin-top: 15px;">
            <input type="hidden" id="aluno_codigo"/>
            <div class="text-center" id="div_titulo">       
                <img class="d-block mx-auto mb-4" src="Images/icocontactados.png" alt="" width="72" height="72">          
                <h2>Listar Alunos Contatados que Demonstraram Interesse</h2><br/>
                <p class="lead" style="font-size: 16">TODOS os campos são OBRIGATÓRIOS </p>                                    
            </div>                                   
            
            <div class="row">
                <div class="col-md-12">                            
                    <form id="frm_resultado">                        
                        <hr class="mb-4">

                        <div class="card">
                            <div class="card-header" style="background-color: #c7d3e0"><b>1. PERÍODO do Contato</b></div>
                        <div class="card-body" style="background-color: #edf3f9">
                            <div class="row">
                            <div class="col-md-2 md-3">
                                <label for="data_inicial" class="required">Data inicial</label>
                                <input class="custom-select d-block w-100" id="data_inicial" style="background-color: white;"  type="text" name="data_inicial" required=""/>               
                            </div>
                            
                            <div class="col-md-2 md-3">
                                <label for="data_fim" class="required">Data final</label>
                                <input class="custom-select d-block w-100" id="data_fim" style="background-color: white;"  type="text" name="data_fim" required=""/>               
                            </div>
                            
                            

                            <div class="col-md-5">
                                    <h6 for="select_curso_interesse1" class="required">Demonstrou interesse no <b>CURSO</b> abaixo:</h6>
                                    <div>
                                        <input type="text" style="background-color: white;" class="form-control input-lg" id="select_curso_interesse1" name="select_curso_interesse[]" required="">
                                    </div>                                                            
                                    <div id="input_curso_interesse"></div>

                                </div>                       
                        </div>

                        <hr class="mb-4">
                            <div class="row">
                                <div class="col-md-3 mb-3">
                                    <label class="required" for="nome_lista">Nome da Lista:</label>
                                    <input type="text" style="background-color: white;" class="form-control" id="nome_lista" required="" placeholder="" value="" name="nome_lista">       
                                 </div>
                            </div>
                            <div class="row">
                               <div class="col-md-4 mb-3">
                                    <h6 class="required" for="select_evento1">Essa lista é para qual evento?</h6>
                                    <div>
                                        <input type="text" style="background-color: white;" class="form-control input-lg" id="select_evento1" name="select_evento[]" onblur="busca_turma();" required="">
                                    </div>                                                            
                                    
                                </div>
                                <div class="col-md-3 mb-3" id="div_turma_lista"> </div>
                            </div>

                            <div class="row">
                               <div class="col-md-4 mb-3">
                                    <h6 class="required" for="descricao">Descrição da Lista</h6>
                                    <div>
                                        <textarea type="text" class="form-control input-lg"  id="descricao" name="descricao" required="" rows="5" style="overflow:hidden"></textarea>
                                    </div>                                                            
                                </div>
                                <div class="col-md-3 mb-3" id="div_turma_lista"> </div>
                            </div>
                        
                        
                        <hr class="mb-4">

                        <div class="row" style="margin-top:10px; margin-left: 5px">
                            <button class="btn btn-primary btn-lg" id="resultados">Gerar Lista</button>
                        </div>
                        <hr class="mb-4">
                        </div>  
                        </div></div>  
                        </div>

                    </form>    
                                    
            </div>
        </div>
        <script src="popper.min.js" crossorigin="anonymous"></script>
        <script src="bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>

      <?php include "rodape.php"; ?>
</BODY>

</HTML>