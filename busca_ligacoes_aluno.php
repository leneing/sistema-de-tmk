<?php

include "conectasql.php";

$sql_filtro = "SELECT l.comentarios,l.tipo_resultado,DATE_FORMAT(l.data,'%d/%m/%Y') as data
FROM ligacoes_tmk l, select_lista sl WHERE l.`aluno_CODIGO`= ".$_POST["aluno_selecionado"]." 
AND l.id_select_lista = sl.id AND sl.id_turma = ".$_POST['turma'];


$res_ligacoes = $conexao ->query($sql_filtro);
$ligacoes = array();

$i = 0;
while ($n = $res_ligacoes -> fetch_assoc()) {
    $ligacoes[$i]["id"] = utf8_encode($n['data']);
    $ligacoes[$i]["value"] = utf8_encode($n['comentarios']);

    if ($n['tipo_resultado'] == 1){
        $ligacoes[$i]["label"] = 'Não atendeu';
    }elseif ($n['tipo_resultado'] == 2){
        $ligacoes[$i]["label"] = 'Não demonstrou interesse';
    }elseif ($n['tipo_resultado'] == 3){
        $ligacoes[$i]["label"] = 'Demonstrou interesse';
    }elseif ($n['tipo_resultado'] == 4){
        $ligacoes[$i]["label"] = 'Número não existe';
    }elseif ($n['tipo_resultado'] == 5){
        $ligacoes[$i]["label"] = 'Parar de ligar';
    }elseif ($n['tipo_resultado'] == 6){
        $ligacoes[$i]["label"] = 'Pediu para ligar depois';
    }else{
        $ligacoes[$i]["label"] = 'Ligação Pulada';
    }
    $i = $i + 1;
}

echo json_encode($ligacoes);

?>