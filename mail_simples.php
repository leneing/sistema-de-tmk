#!/usr/bin/php
<?php
include "conectasql.php";


$jornada = $conexao->prepare("SELECT v.nome as voluntario, v.codigo as id_voluntario, e.NOME as curso,
	SUM(TIMESTAMPDIFF(MINUTE, s.inicio, s.termino)) DIV 60 AS horas,
	SUM(TIMESTAMPDIFF(MINUTE, s.inicio, s.termino)) % 60 AS minutos,
    count(l.id) as ligacoesfeitas,     
    IFNULL(( SELECT count(l1.id)
     FROM
        sessaotmk s1 inner join ligacoes_tmk l1 on l1.sessaotmk_id = s1.id
        inner join voluntario v1 on v1.codigo = s1.voluntario_id
        INNER join evento e1 on e1.id = l1.evento_id
        WHERE
     v1.codigo = v.codigo
        AND e1.id = e.id
     	and l1.tipo_resultado in (1,4) 
		GROUP BY v.nome, e.NOME),0) as naoatendeu,
   IFNULL((SELECT count(l1.id)
     FROM
        sessaotmk s1 inner join ligacoes_tmk l1 on l1.sessaotmk_id = s1.id
        inner join voluntario v1 on v1.codigo = s1.voluntario_id
        INNER join evento e1 on e1.id = l1.evento_id
        WHERE 
    v1.codigo = v.codigo
        AND e1.id = e.id
     	and l1.tipo_resultado in (2,3,6) 
		GROUP BY v.nome, e.NOME),0) as atendeu,
   IFNULL((SELECT count(l1.id)
     FROM
        sessaotmk s1 inner join ligacoes_tmk l1 on l1.sessaotmk_id = s1.id
        inner join voluntario v1 on v1.codigo = s1.voluntario_id
        INNER join evento e1 on e1.id = l1.evento_id
        WHERE 
    v1.codigo = v.codigo
        AND e1.id = e.id
     	and l1.tipo_resultado in (5) 
		GROUP BY v.nome, e.NOME),0) as pararligar,
   IFNULL((SELECT count(l1.id)
     FROM
        sessaotmk s1 inner join ligacoes_tmk l1 on l1.sessaotmk_id = s1.id
        inner join voluntario v1 on v1.codigo = s1.voluntario_id
        INNER join evento e1 on e1.id = l1.evento_id
        WHERE 
    	v1.codigo = v.codigo
        AND e1.id = e.id
     	and l1.tipo_resultado = 7 
		GROUP BY v1.nome, e.NOME), 0) as pulou     
FROM
sessaotmk s inner join ligacoes_tmk l on l.sessaotmk_id = s.id
inner join voluntario v on v.codigo = s.voluntario_id
INNER join evento_ativo e on e.id = l.evento_id
WHERE s.inicio > (?) AND s.termino < (?)
GROUP BY v.nome, e.NOME");

$inicio = '2018-03-01 00:00:00';
$fim = '2018-03-20 23:59:00';

$jornada -> bind_param("ss", $inicio,$fim);
$jornada -> execute();

$res_jornada = $jornada->get_result();
$contar = mysqli_num_rows($res_jornada);
$jornada -> close(); 

$dt =  date("d/m/Y");
$cabecalho .= "Relatório do dia: $dt  ";

$i = 1;
while($ret = $res_jornada -> fetch_assoc()){    
	 $r1 = $ret['voluntario'];
	 $r2 = $ret['curso'];
	 $r3 = $ret['horas'];
	 $r3b = $ret['minutos'];
	 $r4 = $ret['ligacoesfeitas'];
	 $r5 = $ret['naoatendeu'];
	 $r6 = $ret['atendeu'];
	 $r7 = $ret['pararligar'];
	 $r8 = $ret['pulou'];
	 $voluntario = $ret['id_voluntario'];

    $html[$i] .= "$r1 - Foco: $r2 - Tempo de TMK: $r3 horas $r3b min ";
    $html[$i] .= "Total ligacoes: $r4 ";
    $html[$i] .= "Nao Atendidas/Numero nao existe: $r5";
    $html[$i] .= "Atendidas: $r6 ";
    $html[$i] .= "Parar de ligar: $r7 ";
    $html[$i] .= "Ligacoes puladas: $r8  ";


    $ligacoes_do_dia_por_voluntario =  $conexao->prepare("SELECT l.id as id_ligacao FROM ligacoes_tmk l inner join sessaotmk s on l.sessaotmk_id = s.id WHERE s.inicio > (?) AND s.termino < (?) AND s.voluntario_id = (?)");
	$ligacoes_do_dia_por_voluntario -> bind_param("ssi", $inicio,$fim,$voluntario);
	$ligacoes_do_dia_por_voluntario -> execute();

	$res_ligacoes_do_dia_por_voluntario = $ligacoes_do_dia_por_voluntario->get_result();
	$ligacoes_do_dia_por_voluntario -> close(); 

	$zero_interessados = true;

	while($lig = $res_ligacoes_do_dia_por_voluntario -> fetch_assoc()){  
	    $interessados = $conexao->prepare("SELECT a.CODIGO as codigo, a.nome as nome, e.NOME as evento, l.comentarios as comentario
		FROM ligacoes_tmk l inner join sessaotmk s on s.id = l.sessaotmk_id
		INNER JOIN voluntario v on v.codigo = s.voluntario_id
		INNER JOIN evento_interesse ei on ei.id_ligacao = l.id
		INNER JOIN evento e on e.id = ei.evento_id
		INNER JOIN aluno a on a.CODIGO = l.aluno_CODIGO
		WHERE l.id = (?)");
		$interessados -> bind_param("i", $lig['id_ligacao']);
		$interessados -> execute();
		$res_interessados = $interessados->get_result();
		if(mysqli_num_rows($res_interessados) > 0){
			$zero_interessados = false;			
		}
		while($res = $res_interessados -> fetch_assoc()){  
			var_dump($res);
			$ei1 = $res['codigo'];
			$ei2 = $res['nome'];
			$ei3 = $res['evento'];
			$ei4 = $res['comentario'];	
			
		    $html[$i] .= "$ei1 - $ei2 - $ei3   - $ei4   ";    
		}
		$interessados -> close(); 
	}
	if($zero_interessados){
		$html[$i] .= "Interessados: 0   ";
	}

    $i++;
}
/*
$tabela = "";

for($i=0;$i<=$contar;$i++){ 
    $tabela .= $html[$i];
}
*/


$to = "lene.ingrid@gmail.com";
$subject = "IIPC - Relatório do TMK ";
$message = "Relatório do TMK";

mail($to,$subject,$message);

?>