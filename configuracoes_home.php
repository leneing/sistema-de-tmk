<?php
include "conectasql.php";
session_start();

$ligacoes_filtro = "SELECT DATE_FORMAT(data,'%d/%m/%Y') as data, 
  DATE_FORMAT(data,'%H:%i') as hora,
    e.NOME as nmevento,
    e.id as id_evento,
  a.NOME as nmaluno,
  l.aluno_CODIGO as aluno_CODIGO,
    v.nome as nmvoluntario,
    comentarios,
    DATE_FORMAT(l.data_retorno, '%d/%m/%Y - %H:%i') as data_retorno
  from ligacoes_tmk l INNER JOIN aluno a on a.CODIGO = l.aluno_CODIGO
    inner join evento_ativo e on e.id = l.evento_id
    inner join sessaotmk s on s.id = l.sessaotmk_id
    inner join voluntario v on v.codigo = s.voluntario_id    
  WHERE fl_pendente_retorno = 1
  order by data_retorno";



$res_ligacoes_pendentes = $conexao ->query($ligacoes_filtro);

?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>IIPC BH - TMK - Configurações</title>

        <link rel="stylesheet" href="bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="jquery-ui.css">
        <script src="jquery-3.3.1.min.js" crossorigin="anonymous"></script>
        <script src="jquery-ui.js"></script>
        <link href="gijgo.min.css" rel="stylesheet" type="text/css"/>
        <script src="gijgo.min.js" type="text/javascript"></script>
        <script src="combobox.js"></script>
        
        <link rel="stylesheet" href="estilo.css">
    <link href="form-validation.css" rel="stylesheet">

    <script>
      $(document).ready(function () { 
        $(".btn_ligar").click(function () {
          location.href = "redirecionaComCodigoAluno.php?aluno_CODIGO="+$(this).attr("aluno_CODIGO")+"&curso_selecionado="+$(this).attr("curso_selecionado")+"&tela_origem=ligacoesPendentes.php"; 

          return false;
        });
      });
    </script>
  </head>

  <body class="bg-light">

    <?php
            include "navbar_adm.php";
        ?> 

    <div class="container">
      <div class="col-md-12">
      <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="Images/icoconfig.png" alt="" width="72" height="72">
        <h2>Coordenação de TMK</h2>
        </div>

      <?php include "rodape.php"; ?>
    </div>

  </body>
</html>




      