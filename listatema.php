<?php

include "conectasql.php";

$nomes_filtro = "SELECT * FROM tema WHERE NOME COLLATE UTF8_GENERAL_CI LIKE '%".trim(strip_tags(utf8_decode($_GET['term'])))."%'";
//$nomes_filtro = "SELECT * FROM tema WHERE NOME LIKE LOWER('%".trim(strip_tags(utf8_decode($_GET['term'])))."%') ORDER BY NOME";

$res_nomes = $conexao ->query($nomes_filtro);
$nomes = array();

$i = 0;
while ($n = $res_nomes -> fetch_assoc()) {
    $nomes[$i]["id"] = utf8_encode($n['id']);
    $nomes[$i]["label"] = utf8_encode($n['NOME']);
    $nomes[$i]["value"] = utf8_encode($n['NOME']);
    $i = $i + 1;
}


echo json_encode($nomes);

?>