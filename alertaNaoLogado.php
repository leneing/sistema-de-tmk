<?php
?>
<!DOCTYPE html>
<html>
<head>
        <link rel="stylesheet" href="bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="estilo.css">
        <script src="jquery-3.3.1.min.js" crossorigin="anonymous"></script>
        <script src="jquery-ui.js"></script>

</head>
<BODY class="bg-light">
        <?php
            include "navbar.php";
        ?> 
        <div class="container" style="margin-top: 15px;">
        	<input type="hidden" id="aluno_codigo"/>
              <div class="text-center" id="div_titulo">       
                    <img class="d-block mx-auto mb-4" src="Images/icoalert.png" alt="" width="72" height="72">          
                    <h3>Você não está logado ou sua sessão expirou.</h3>  
                    <div style="margin-top: 30px">                  
                      <button class="btn btn-primary btn-md" id="btndataultimaligacao"
                      onclick="window.location.href='index.php'">Fazer login</button>   
                    </div>                          
             </div>
         </div>
</body>
</html>