<?php

include "conectasql.php";


$alunos_filtro = "SELECT * FROM (SELECT l.id_aluno as aluno_codigo,
l.nm_aluno as nome,
(SELECT COUNT(e.id_aluno) FROM aluno_turma_acompanhar e where e.id_aluno = l.id_aluno AND e.id_turma = l.id_turma) as estrela,
DATE_FORMAT(MAX(l.data_ligacao),'%d/%m/%Y') as ultimo_contato,
COUNT(*) as contatos,
(SELECT COUNT(*) FROM vw_ligacao l2 WHERE l.id_aluno = l2.id_aluno AND l.id_turma = l2.id_turma AND l2.id_tipo_resultado = 1) AS nao_atendeu,
(SELECT COUNT(*) FROM vw_ligacao l2 WHERE l.id_aluno = l2.id_aluno AND l.id_turma = l2.id_turma AND l2.id_tipo_resultado = 2) AS sem_interesse,
(SELECT COUNT(*) FROM vw_ligacao l2 WHERE l.id_aluno = l2.id_aluno AND l.id_turma = l2.id_turma AND l2.id_tipo_resultado = 3) AS interesse,
(SELECT COUNT(*) FROM vw_ligacao l2 WHERE l.id_aluno = l2.id_aluno AND l.id_turma = l2.id_turma AND l2.id_tipo_resultado = 4) AS numero_nao_existe,
(SELECT COUNT(*) FROM vw_ligacao l2 WHERE l.id_aluno = l2.id_aluno AND l.id_turma = l2.id_turma AND l2.id_tipo_resultado = 5) AS parar_ligar,
(SELECT COUNT(*) FROM vw_ligacao l2 WHERE l.id_aluno = l2.id_aluno AND l.id_turma = l2.id_turma AND l2.id_tipo_resultado = 6) AS ligar_depois,
(SELECT COUNT(*) FROM vw_ligacao l2 WHERE l.id_aluno = l2.id_aluno AND l.id_turma = l2.id_turma AND l2.id_tipo_resultado = 7) AS pulada
FROM vw_ligacao l WHERE l.id_turma = ".$_POST['turma']." GROUP BY nome ORDER BY estrela DESC, interesse DESC, sem_interesse ASC, nome ASC) todas_ligacoes    ";

$filtros = explode(",",$_POST['filtros_checked']);

if(in_array("1",$filtros)){
    $alunos_filtro = $alunos_filtro . "WHERE estrela > 0 OR ";
}elseif(in_array("2",$filtros)){
    $alunos_filtro = $alunos_filtro . "WHERE interesse > 0 OR ";
}elseif(in_array("3",$filtros)){
    $alunos_filtro = $alunos_filtro . "WHERE sem_interesse > 0 OR ";
}elseif(in_array("4",$filtros)){
    $alunos_filtro = $alunos_filtro . "WHERE ligar_depois > 0 OR ";
}elseif(in_array("5",$filtros)){
    $alunos_filtro = $alunos_filtro . "WHERE nao_atendeu > 0 OR ";
}elseif(in_array("6",$filtros)){
    $alunos_filtro = $alunos_filtro . "WHERE pulada > 0 OR ";
}elseif(in_array("7",$filtros)){
    $alunos_filtro = $alunos_filtro . "WHERE numero_nao_existe > 0 OR ";
}elseif(in_array("8",$filtros)){
    $alunos_filtro = $alunos_filtro . "WHERE parar_ligar > 0 OR ";
}else{
    $alunos_filtro = "SELECT * FROM aluno WHERE 1 = 2";
}

if(in_array("2",$filtros)){
    $alunos_filtro = $alunos_filtro . "interesse > 0 OR ";
}
if(in_array("3",$filtros)){
    $alunos_filtro = $alunos_filtro . "sem_interesse > 0 OR ";
}
if(in_array("4",$filtros)){
    $alunos_filtro = $alunos_filtro . "ligar_depois > 0 OR ";
}
if(in_array("5",$filtros)){
    $alunos_filtro = $alunos_filtro . "nao_atendeu > 0 OR ";
}
if(in_array("6",$filtros)){
    $alunos_filtro = $alunos_filtro . "pulada > 0 OR ";
}
if(in_array("7",$filtros)){
    $alunos_filtro = $alunos_filtro . "numero_nao_existe > 0 OR ";
}
if(in_array("8",$filtros)){
    $alunos_filtro = $alunos_filtro . "parar_ligar > 0 OR ";
}

$alunos_filtro = substr($alunos_filtro, 0, -3);
$res_alunos = $conexao ->query($alunos_filtro);

$alunos = array();

$i = 0;
while ($n = $res_alunos -> fetch_assoc()) {
    $alunos[$i]["estrela"] = utf8_encode($n['estrela']);
    $alunos[$i]["aluno_codigo"] = utf8_encode($n['aluno_codigo']);
    $alunos[$i]["nome"] = utf8_encode($n['nome']);
    $alunos[$i]["contatos"] = utf8_encode($n['contatos']);
    $alunos[$i]["ultimo_contato"] = utf8_encode($n['ultimo_contato']);
    $alunos[$i]["interesse"] = utf8_encode($n['interesse']);
    $alunos[$i]["sem_interesse"] = utf8_encode($n['sem_interesse']);
    $i = $i + 1;
}

echo json_encode($alunos);

?>