 <?php
error_reporting(E_WARNING);
include "conectasql.php";
session_start();

$var_sessao_aluno_codigo = $_SESSION["aluno_CODIGO"];
if ($var_sessao_aluno_codigo !== "") {
    unset ($_SESSION["aluno_CODIGO"]);   
}

$cursos_filtro = "SELECT DISTINCT e.* FROM `evento` e,turma t where  DATE(t.DATA_INICIAL) > CURDATE() and e.id = t.ID_EVENTO";
$res_curso = $conexao ->query($cursos_filtro);

$voluntarios_filtro = "SELECT DISTINCT v.* FROM voluntario v ORDER BY v.nome ASC";
$res_voluntario = $conexao -> query($voluntarios_filtro);

$cursos_interesse = "SELECT DISTINCT e.* FROM `evento_ativo` e where e.ativo = 1";
$res_curso_ativo = $conexao ->query($cursos_interesse);

$listas_filtro = "SELECT DISTINCT l.* FROM `select_lista` l, turma t  WHERE l.fl_ativo = 1 AND l.id_turma = t.CODIGO AND t.DATA_INICIAL > CURDATE()";  
$res_lista = $conexao ->query($listas_filtro);

$eventos_listas_filtro = "SELECT DISTINCT e.* FROM `select_lista` l, evento e WHERE e.id = l.id_evento and fl_ativo = 1";  
$res_eventos_lista = $conexao ->query($eventos_listas_filtro);

$turmas_ativas_filtro = "SELECT DISTINCT e.NOME, t.CODIGO FROM turma t, evento e, select_lista l WHERE t.DATA_INICIAL > CURDATE() and e.id = t.ID_EVENTO and l.id_turma = t.CODIGO";  
$res_turma = $conexao ->query($turmas_ativas_filtro);



/* // ver se tá logado no iipcnet
$x = file_get_contents("http://iipcnet.iipc.org/sources/formEntryNew.asp?FunctionKey=27&KeyValue=1");    
$a = stripos($x,"Encerrada");

$x = file_get_contents("http://iipcnet.iipc.org/sources/formEntryNew.asp?FunctionKey=27&KeyValue=1");    
$a = stripos($x,"expirou por inatividade");l
var_dump($x);
echo $x;

if($a == false){
    $displayLogadoHide = "none";
    $displayLogadoShow = "block";
}else{

    $displayLogadoHide = "block";
    $displayLogadoShow = "none";
}*/

/*var_dump($_SESSION["pular_login"]);
var_dump($_SESSION["ID_VOLUNTARIO"]);
var_dump($_SESSION["aluno_CODIGO"]);*/
$var_sessao_pular_login = $_SESSION["pular_login"];
if ($var_sessao_pular_login !== "") {
    unset($_SESSION['pular_login']);    
}

$var_sessao_tela_origem = $_SESSION["tela_origem"];
if ($var_sessao_tela_origem !== "") {
    unset($_SESSION['tela_origem']);    
}

$var_sessao_curso_selecionado = $_SESSION["curso_selecionado"];
if ($var_sessao_curso_selecionado !== "") {
    unset($_SESSION['curso_selecionado']);    
}

?>

<HTML>

<HEAD>
        <TITLE>IIPC BH - TMK</TITLE>

        <link rel="stylesheet" href="jQuery-Plugin-For-Multi-Select-Checboxes-multiselect/css/jquery.multiselect.css">

        <link rel="stylesheet" href="bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="estilo.css">

        <script src="jquery-3.3.1.min.js" crossorigin="anonymous"></script>
        <script src="jquery-ui.js"></script>
        <script src="jQuery-Plugin-For-Multi-Select-Checboxes-multiselect/js/jquery.multiselect.js"></script>
        <script src="jquery-ui.js"></script>
        <script src="combobox.js"></script>


        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

        <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>



        <style type="text/css">
            .form-center{   
                width: 20%;
                padding: .375rem .75rem;
                font-size: 1rem;
                line-height: 1.5;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                border-radius: .25rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            }
            .form-radius{  
                font-size: 1rem;
                line-height: 1.5;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                border-radius: .25rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            }

            fieldset.scheduler-border {   
                text-align: center;
                background-color: #f7f9fc;
                min-width: 200px;             
                padding: 10px !important;
                -webkit-box-shadow:  0px 0px 0px 0px #000;
                        box-shadow:  0px 0px 0px 0px #000;
            }
        </style>

        <script>
                window.onbeforeunload = function (evt) {
                    encerrasessao();
                }              

                function filtraTabela() {
                  // Declare variables
                  var input, filter, table, tr, td, i;
                  input = document.getElementById("filtroCursosFeitos");
                  filter = input.value.toUpperCase();
                  table = document.getElementById("tabela_cursos_feitos");
                  tr = table.getElementsByTagName("tr");

                  // Loop through all table rows, and hide those who don't match the search query
                  for (i = 0; i < tr.length; i++) {
                    td = tr[i].getElementsByTagName("td")[1];
                    if (td) {
                      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                      } else {
                        tr[i].style.display = "none";
                      }
                    }
                  }
                }

                function chamaIIPCNet(codigo_aluno){
                    link = "http://iipcnet.iipc.org/sources/formEntryNew.asp?FunctionKey=27&KeyValue="+codigo_aluno;
                    $("#iipcnet").attr('src',link);
                    carrega_informacao(codigo_aluno);
                    $("#aluno_codigo").val(codigo_aluno);
                   recuperaLigacoesAnteriores(codigo_aluno);
                   recuperaCursosJaFeitos(codigo_aluno);
                   $("#div_carregando").hide();
                   $("#div_iipcnet_infos").show();
                   $("#div_ligacoes_anteriores").show();
                   $("#div_cursos_ja_feitos").show();
                   $("#div_relogar").show();
                }

                function carrega_informacao(codigo){
                        $.ajax({
                                url: "informacoes.php",
                                type: "post",
                                data: "codigo="+codigo,
                                success: function(html_informacoes){
                                        $("#div_informacoes").html(html_informacoes);
                                }
                        })
                }
                function recarregaAluno(){
                    document.getElementsByClassName("close")[0].click();
                    if($("#aluno_codigo").val() != ""){
                        chamaIIPCNet($("#aluno_codigo").val());
                    }else if($("#lista_selecionada").val() != ""){
                        comecar();
                    }else{
                        location.reload();
                    }
                   
                }


                function busca_aluno(){
                    if($("#param_tela_origem").val() != ""){
                        chamaIIPCNet($("#param_aluno_CODIGO").val());   
                    }
                    if ($("#param_aluno_CODIGO").val() == "") {
                       $.ajax({
                                url: 'buscaaluno.php',
                                data: $("#frm_filtro").serialize(),
                                type: "post",
                                success: function(codigo_aluno){
                                    if(codigo_aluno == 0){
                                        alert("Essa lista não possui mais alunos! Favor selecionar outra lista.");
                                            $("#div_iipcnet_infos").hide(); 
                                            $("#div_relogar").hide(); 
                                            $("#div_escolhe_lista").show();
                                            $("#div_carregando").hide(); 
                                            $("#ligarsimnao").hide(); 
                                    }else{
                                        chamaIIPCNet(codigo_aluno);
                                    }
                                }
                        }); 
                    }else{
                        chamaIIPCNet($("#param_aluno_CODIGO").val());   
                    }       
                }

                function retorna_curso(){
                       $.ajax({
                                url: 'retorna_curso.php',
                                data: $("#frm_filtro").serialize(),
                                type: "post",
                                success: function(curso_selecionado_lista){
                                        $("#curso_selecionado").val(curso_selecionado_lista);
                                }
                        });      
                }

                

                function recuperaLigacoesAnteriores(codigo_aluno){
                     $.ajax({
                            url: 'recuperaLigacoesAnteriores.php',
                            type: 'post',
                            data: "codigo_aluno="+codigo_aluno,
                            success: function(resultado_recupera_lig){
                                $("#div_ligacoes_anteriores").html(resultado_recupera_lig);
                                $("#div_ligacoes_anteriores").show();
                            }
                        });
                }


                function recuperaCursosJaFeitos(codigo_aluno){
                     $.ajax({
                            url: 'recuperaCursosJaFeitos.php',
                            type: 'post',
                            data: "codigo_aluno="+codigo_aluno,
                            success: function(resultado_recupera_cur){
                                $("#div_cursos_ja_feitos").html(resultado_recupera_cur);
                                $("#div_cursos_ja_feitos").show();
                            }
                        });
                }

                function encerrasessao(){
                    $.ajax({
                        url: 'encerrasessao.php',
                        type: 'post',  
                        async : false,                                      
                        success: function(resultado_encerra_sessao){
                            //Nao precisa fazer nada aqui
                            //O relatorio por email foi enviado na pagina encerrasessao.php
                        }
                    });
                }

                function atualizasessao(){
                    $.ajax({
                        url: 'atualizasessao.php',
                        type: 'post',  
                        async : false,                                      
                        success: function(resultado_encerra_sessao){
                        }
                    });
                }

                function redirecionaSemCodigoAluno(){
                    $.ajax({
                        type: 'GET',
                        url: 'redirecionaSemCodigoAluno.php',
                        success: function() {
                            window.location = 'redirecionaSemCodigoAluno.php';
                        },
                        error: function(xhr, ajaxOptions, thrownerror) { }
                    });
                }

               
                function comecar() {                              
                    
                        var nome = $("#voluntario_selecionado").val();
                        frm_resposta = $("#frm_filtro").serialize();
                        $("#div_carregando").show(); 
                        $("#div_escolhe_lista").hide();
                        $("#div_informacoes_login").hide();

                        retorna_curso()

                        busca_aluno();                       
                        $("#curso_alvo").html($("#curso_selecionado option:selected").text());
                        
                        // Chamar php filtro para selecionar o codigo do aluno
                        $("#ligacoes_anteriores").show();                        
                        $("#ligarsimnao").show();
                        
                        //if ($("#param_pular_login").val() !== "1") { 
                         /*$.ajax({
                            url: 'criasessao.php',
                            type: 'post',
                            data: "voluntario_selecionado="+$("#voluntario_selecionado").val()+"&lista_selecionada="+$("#lista_selecionada").val(),
                            success: function(resultado_cria_sessao){
                                if (resultado_cria_sessao != 1) {
                                    alert("Ocorreu o  erro: "+resultado_cria_sessao);
                                }
                            }
                         });*/  
                        //} 
                        return false;                                                    
                }

                var counter = 1;
                function addInput(divName){
                        counter++;
                        var newdiv = document.createElement('div');
                        newdiv.id = "dynamicInput_child"+counter;
                        newdiv.innerHTML = " <br><input id='select_interesse"+ counter +"' style='background-color: white;' type='text' class='form-control input-lg interesse_extra'> <input type='hidden' id='select_interesseID"+ counter +"' type='text' class='interesse_extra' name='select_interesse[]'>";
                       document.getElementById(divName).appendChild(newdiv);

                        $( "#select_interesse"+counter+"").autocomplete({
                                source: "listacurso.php",
                                minLength: 2, 
                                open: function(event) {
                                valid = false;
                                },
                                close: function(event){
                                    if (!valid) $(this).val('');
                                },
                                select: function (event, ui) {
                                    valid = true;
                                    $("#select_interesseID"+ counter +"").val(ui.item.id);
                                }
                        });  
                }

                function removeInput(){
                        if (counter > 1) {
                                $( "#dynamicInput_child"+counter ).remove();
                                counter--;
                        }else{alert("É necessário preencher ao menos um campo de interesse!");}
                }

                $(document).ready(function () {   

                    // Get the modal
                    var modal = document.getElementById('modal_relogar');
                    // Get the button that opens the modal
                    var btn = document.getElementById("btn_relogar");
                    // Get the <span> element that closes the modal
                    var span = document.getElementsByClassName("close")[0];
                    // When the user clicks the button, open the modal 
                    btn.onclick = function() {
                        modal.style.display = "block";
                    }
                     // When the user clicks on <span> (x), close the modal
                    span.onclick = function() {
                        modal.style.display = "none";
                    }
                    // When the user clicks on <span> (x), close the modal                   
                    // When the user clicks anywhere outside of the modal, close it
                    window.onclick = function(event) {
                        if (event.target == modal) {
                            modal.style.display = "none";
                        }
                    }
                    

                    // se está vindo das telas ligacoes_pendentes ou busca-aluno_por-codigo que vai direto pra ligacao
                    if($("#param_pular_login").val() == "1") {                
                        $("#logado").hide();
                        $("#div_escolhe_lista").hide();
                        $("#div_titulo").hide(); 
                       comecar(); 
                    // se está vindo do Fazer TMK, que vai primeiro pro escolhe listas
                    }else if ($("#param_pular_login").val() == "2") {                
                        $("#div_informacoes_login").hide();
                        $("#div_escolhe_lista").show();
                        $("#div_titulo").hide(); 
                        $("#div_iipcnet_infos").hide(); 
                    }else{
                        $("#menu").hide();
                        $("#div_relogar").hide();
                    }   

                    $("#ligacoes_anteriores").hide();
                   
                    $("#div_cursos_interesse").hide();
                    $("#div_data_ligar_depois").hide();
                   
                    $("#com_interesse").click(function () {
                            $("#div_cursos_interesse").show();                                
                    });


                    $("#seminteresse, #pararligar, #naoatendeu, #numeroinexistente, #ligardepois").click(function () {
                            $("#div_cursos_interesse").hide();                                
                    });

                    $("#seminteresse, #pararligar, #naoatendeu, #numeroinexistente, #com_interesse").click(function () {
                            $("#div_data_ligar_depois").hide();                                
                    });

                    $("#ligardepois").click(function () {
                           $("#div_data_ligar_depois").show();                     
                    });

                    $('.timepicker').timepicker({
                        timeFormat: 'H:mm',
                        interval: 30,
                        minTime: '07',
                        maxTime: '22:00pm',
                        dynamic: false,
                        dropdown: true,
                        scrollbar: true
                    });
                    $('#lista_selecionada').change(function(){
                        $('#restantes').text("Aguarde...");
                        $.ajax({
                            url: 'contaalunos.php',
                            type: 'post',
                            data: "lista_selecionada="+$("#lista_selecionada").val(),
                            success: function(resultado_contagem){
                                $('#restantes').text(resultado_contagem);
                            }
                            });
                    });
                    $('#turma_selecionada').change(function(){
                        $('#restantes').text("");
                        $.ajax({
                            url: 'listasturma.php',
                            type: 'post',
                            data: "turma_selecionada="+$("#turma_selecionada").val(),
                            dataType: 'json',
                            success: function(resultado_listas){
                                //alert(resultado_listas[0]["label"]);
                                $("#lista_selecionada").empty()
                                $("#lista_selecionada").append(
                                        $('<option></option>').val('').html('Escolha...')
                                    );
                                var i;
                                for (i = 0; i < resultado_listas.length; ++i) {
                                    $("#lista_selecionada").append(
                                        $('<option></option>').val(resultado_listas[i]['id']).html(resultado_listas[i]['label'])
                                    );
                                }  
                            }
                            });
                    });



                        var frm_resposta;
                        $("#btn_logado").click(function () {  
                            $("#frm_login")[0].reportValidity();
                            if ($("#frm_login")[0].checkValidity()) {                          
                                $("#logado").hide();
                                $("#div_iipcnet_infos").hide(); 
                                $("#div_relogar").hide(); 
                                $("#div_escolhe_lista").show();
                                $("#div_titulo").hide();   
                                $("#menu").show();
                                $.ajax({
                                    url: 'criasessao.php',
                                    type: 'post',
                                    data: "voluntario_selecionado="+$("#voluntario_selecionado").val()+"&lista_selecionada="+$("#lista_selecionada").val(),
                                    success: function(resultado_cria_sessao){
                                        if (resultado_cria_sessao != 1) {
                                            alert("Ocorreu o  erro: "+resultado_cria_sessao);
                                        }
                                    }
                                 });
                            }                     
                             return false;
                        });
                        $("#comecar").click(function(){
                           $("#frm_filtro")[0].reportValidity();
                           if ($("#frm_filtro")[0].checkValidity()) {
                                comecar(); 
                           }
                        });

                        $("#ligar").click(function () {
                                $("#ligarsimnao").hide();
                                $("#ligou").show();
                                return false;
                        });
                        $("#naoligar").click(function () {
                                $("#ligarsimnao").hide();
                                $("#naoligou").show();
                                return false;
                        });
                        $("#naoligarmais").click(function () {                                
                                $("#div_iipcnet_infos").hide();
                                $("#div_relogar").hide();
                                $.ajax({
                                        url: 'ultimaligacao.php',
                                        type: 'post',
                                        data: "date="+"01/01/5000"+"&codigo="+$("#aluno_codigo").val(),
                                        success: function(resultado_filtro){
                                                $("#naoligou").hide();
                                                atualizasessao();
                                                $("#proximaligacao").show();
                                        }
                                });
                        return false;
                        });
                        $("#lista_selecionada").change(function() {
                            $.ajax({
                                url: 'descricao_lista.php',
                                data: "lista="+$("#lista_selecionada").val(),
                                type: 'post',
                                success: function(resultado_descricao_lista){
                                     $("#descricao_lista").html(resultado_descricao_lista);
                                }
                            })
                        });
                        $("#pularaluno").click(function () {
                                $("#naoligou").hide();
                                $("#div_pularaluno").show();
                                return false;
                        });

                        $("#ligouapoucotempo").click(function () {
                                $("#naoligou").hide();
                                $("#ultimaligacao").show();
                                return false;
                        });
                        $("#btndataultimaligacao").click(function () {
                                if (!$("#datepicker").value){
                                        $.ajax({
                                        url: 'ultimaligacao.php',
                                        type: 'post',
                                        data: "date="+$("#datepicker").val()+"&codigo="+$("#aluno_codigo").val(),
                                        success: function(resultado_filtro){
                                                $("#datepicker").val("");
                                                $("#div_iipcnet_infos").hide();
                                                $("#div_relogar").hide();
                                                $("#ultimaligacao").hide();
                                                atualizasessao();
                                                $("#proximaligacao").show();
                                        }
                                });
                                }
                                return false;
                        });
                        $("#continuar").click(function () {
                                $("#div_carregando").show();                                 
                                $("#proximaligacao").hide(); 
                                $("#ligarsimnao").show();
                                busca_aluno();                               
                                $("#frm_resultado")[0].reset();
                                return false;
                        });
                        $("#encerrar").click(function () {3
                            if(confirm("Tem certeza de que deseja encerrar o TMK?")){
                                encerrasessao();
                                alert("Obrigado pelo empenho e assistência!");
                                location.href = "index.php";
                            }
                            return false;
                        });
                        $("#btn_confirma_pular").click(function () {                            
                            $.ajax({
                                url: 'pularaluno.php',
                                type: 'post',
                                data: "comentarios_pular="+$("#comentarios_pular").val()+"&aluno_codigo="+$("#aluno_codigo").val()+"&curso_selecionado="+$("#curso_selecionado").val()+"&lista_selecionada="+$("#lista_selecionada").val(),
                                success: function(resultado_filtro){
                                        $("#comentarios_pular").val("");
                                        $("#div_pularaluno").hide();
                                        atualizasessao();
                                        $("#proximaligacao").show();
                                        $("#div_iipcnet_infos").hide(); 
                                        $("#div_relogar").hide();                               }
                            });
                            
                            return false;
                        }); 
                        $("#resultados").click(function () {
                                //colocar informaçoes da ligacao na database  
                            if($('#ligardepois').is(':checked')) {
                                $('#datepickerdepois').attr("required","required");
                                $('#timepicker').attr("required","required");
                            }else{
                                $('#datepickerdepois').removeAttr("required");
                                $('#timepicker').removeAttr("required");
                            }
                            if($("#naoatendeu").is(':checked') || $("#numeroinexistente").is(':checked')){
                                $('#comentarios').removeAttr("required");
                            }else{
                                $('#comentarios').attr("required","required");
                            }
                            if($('#com_interesse').is(':checked')) {
                                for (i=1; i < counter+1; i++){
                                        if($("#select_interesse"+i+"").val().trim().length < 1){             
                                                alert("Informe os cursos em que o aluno demonstrou interesse.");
                                                return false;  
                                        }       
                                }
                            }

                            $("#frm_resultado")[0].reportValidity();
                            if ($("#frm_resultado")[0].checkValidity()) {
                                    $.ajax({
                                        url: 'resultadoligacao.php',
                                        type: 'post',
                                        data: $("#frm_resultado").serialize()+"&aluno_codigo="+$("#aluno_codigo").val()+"&"+$("#frm_filtro").serialize()+"&comentarios="+$("#comentarios").val()+"&lista_selecionada="+$("#lista_selecionada").val(),
                                        success: function(resultado_filtro){
                                            if ($("#param_pular_login").val() == "1") {  
                                                alert("Ligação gravada com sucesso!");                                                
                                                location.href = $("#param_tela_origem").val();
                                            }else{
                                                $("#ligou").hide();
                                                atualizasessao();
                                                $("#proximaligacao").show();
                                                $("#div_iipcnet_infos").hide();
                                                $("#div_relogar").hide();
                                                $("#frm_resultado")[0].reset();
                                                $("#div_cursos_interesse").hide();
                                                $("#div_data_ligar_depois").hide();
                                                $( ".interesse_extra" ).remove();
                                                $("#select_interesseID1").val("");
                                                for (i=2;i<counter+1;i++){
                                                        $( "#dynamicInput_child"+i ).remove(); 
                                                } 
                                                counter = 1;   
                                                alert("Ligação gravada com sucesso!");
                                            }
                                        }
                                });
                            }

                                return false;
                        });               
                        $("#datepicker").datepicker({
                                dateFormat: 'dd/mm/yy',
                                dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                                dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                                dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                                monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                                monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                                nextText: 'Próximo',
                                prevText: 'Anterior'
                        }); 
                        $("#datepickerdepois").datepicker({
                                dateFormat: 'dd/mm/yy',
                                dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                                dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                                dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                                monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                                monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                                nextText: 'Próximo',
                                prevText: 'Anterior'
                        });

                        $( "#select_interesse1" ).autocomplete({
                                source: "listacurso.php",
                                minLength: 2,
                                open: function(event) {
                                valid = false;
                                },
                                close: function(event){
                                    if (!valid) $(this).val('');
                                },
                                select: function (event, ui) {
                                    valid = true;
                                    $("#select_interesseID1").val(ui.item.id); 
                                }
                        });   
                        
                });
        </script>
</HEAD>

<BODY class="bg-light">
        <?php
            include "navbar.php";
        ?> 
        <div class="container" style="margin-top: 15px;">
        <input type="hidden" id="aluno_codigo"/>
              <div class="text-center" id="div_titulo">       
                    <img class="d-block mx-auto mb-4" src="Images/ico5.png" alt="" width="72" height="72">          
                    <h2>Sistema de TMK</h2>
                     <p class="lead" style="text-align: center; margin-top: 15px">Faça o login na tela do IIPCNET.<br/><br/>Informe seu nome e clique em Já fiz login.</p>   
                                  
             </div>
        <div class="row">
                <div class="col-md-12">
                        <form id="frm_filtro" name="frm_filtro">
                            <!-- O código do aluno vem da página de retornar ligação ligacoesPendentes.php ou do busca_aluno_por_codigo.php -->
                            <input type="hidden" value="<?=$var_sessao_aluno_codigo?>" name="param_aluno_CODIGO" id="param_aluno_CODIGO">
                            <input type="hidden" value="<?=$var_sessao_pular_login?>" name="param_pular_login" id="param_pular_login">
                            <input type="hidden" value="<?=$var_sessao_tela_origem?>" name="param_tela_origem" id="param_tela_origem">
                            <input type="hidden" value="<?=$var_sessao_curso_selecionado?>" name="param_curso_selecionado" id="param_curso_selecionado">
                                
                            <div id="div_escolhe_lista" class="col-md-12 text-center" style="display: none; text-align: center;">

                                <div class="row" style="text-align: center;">
                                    <div class="col-md-12 text-center">
                                        <label class="required" for="turma_selecionada">Fará TMK para qual curso?</label>
                                    </div>
                                </div>
                                <div class="row mb-5" style="text-align: center;">
                                    <select class="custom-select d-block w-100" name="turma_selecionada" id="turma_selecionada" required>
                                        <option value=''>Escolha...</option>
                                        <?php 
                                            while ($linha_turma = $res_turma -> fetch_assoc()){
                                            ?>
                                                <option value="<?=$linha_turma['CODIGO']?>"><?=UTF8_ENCODE($linha_turma['NOME'])?></option>  
                                            <?php 
                                            }  
                                        ?>
                                    </select>
                                </div>

                                <!-- Pergunta nome e informações básicas -->
                                <div class="row">
                                    <div class="col-md-8 text-center">
                                        <div>
                                            <label class="required" for="curso_selecionado">Fará TMK com qual lista?</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3 offset-1 text-center">
                                        <div>
                                            <label for="restantes">Alunos restantes</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="text-align: center;">
                                    <div class="col-md-8 text-center">
                                        <div class="row mb-3">
                                            <select class="custom-select d-block w-100" name="lista_selecionada" id="lista_selecionada" required>
                                                <option value=''>Escolha...</option>
                                            </select>
                                            <input type='hidden' id='curso_selecionado' type='text' name='curso_selecionado' val="">
                                        </div>
                                    </div>
                                    <div class="col-md-3 offset-1 text-center">
                                        <label id="restantes"></label>
                                    </div>
                                </div>
                                <div class="row" id="descricao_lista"></div>

                                <div class="row" style="margin-top: 40px;">
                                    <div class="text-center col-md-12">
                                        <button class="btn btn-primary btn-lg btn3d" type="button" 
                                            id="comecar">Começar TMK</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <form id="frm_inutil">
                                <div id="ligarsimnao" style="display: none; text-align:center;">
                                        <h4>Você vai ligar para este aluno?</h4>
                                        <div class="row">
                                                <div class="col-md-2 offset-4">
                                                        <button class="btn btn-primary btn-lg btn-block" id="ligar">SIM</button>
                                                </div>
                                                <div class="col-md-2">
                                                        <button class="btn btn-primary btn-lg btn-block" id="naoligar">NÃO</button>
                                                </div>
                                        </div>
                                </div>
                                <div id="naoligou" style="display: none; text-align:center;">
                                        <h4>Por que não é possível ligar?</h4>
                                        <div class="row offset-2">
                                            <div class="col-md-3">
                                                    <button class="btn btn-primary " id="pularaluno">Vou pular esse aluno</button>
                                            </div>
                                            <div class="col-md-3">
                                                    <button class="btn btn-primary " id="ligouapoucotempo">Já teve contato recente</button>
                                            </div>
                                            <div class="col-md-3">
                                                    <button class="btn btn-primary " id="naoligarmais">Pediu para não ligar</button>
                                            </div>
                                        </div>
                                </div>
                                <div class="row col-md-12">
                                    <div id="ultimaligacao" style="display: none; text-align:right;" class="col-md-8 offset-1">
                                        <div class="col-md-12 offset-2" style="text-align: center">
                                            <h4 class="required">Qual a data da última ligação?</h4> 
                                        </div>
                                                    <div class="col-md-12 offset-3" style="text-align:right">
                                                                    <div class="row">
                                                                        <div style="margin-top: 10px;margin-left: 50px; margin-right: 35px">
                                                                            <input id="datepicker" type="text" required name="datepicker"  value="" style="width: 60%" />               
                                                                        </div>
                                                                        <div  style="margin-top: 5px;">
                                                                            <button class="btn btn-primary btn-md" id="btndataultimaligacao">Confirmar</button>
                                                                        </div>                                                            
                                                                    </div>
                                                                    <!-- falta implementar no banco
                                                                        <div class="col-md-10" style="margin-top: 15px;">
                                                                            <textarea type="text" class="form-control input-md" id="comentario_lig_recente" nome="comentario_lig_recente" placeholder="" value="" rows="2" style="overflow:hidden"></textarea> 
                                                                            <h6 style="color:#444547">Comentário (opcional)</h6>           
                                                                        </div>
                                                                    -->
                                                                    
                                                    </div>                                                    
                                    </div>
                                </div>
                                <div id="div_pularaluno" style="display: none; text-align:center;">
                                        <h4 class="required">Por que vai pular esse aluno?</h4> 

                                        <div class="row">
                                            <div class="col-md-4 offset-4" style="margin-top: 15px">
                                                <div class="row">
                                                        <textarea type="text" required class="form-control input-md" id="comentarios_pular" nome="comentarios_pular" placeholder="" value="" required="required" rows="3" style="overflow:hidden"></textarea>   
                                                </div>
                                                            
                                            </div>
                                            <div class="col-md-2" style="padding-top: 30px">
                                             <button class="btn btn-primary btn-lg" id="btn_confirma_pular">Confirmar</button>
                                             </div>
                                        </div><br/>
                                         <h6><i>(Não se preocupe, alunos pulados retornam às listas no dia seguinte para alguém ligar)</i></h6>
                                </div>
                        </form>
                        <form id="frm_resultado">
                                <div id="ligou" style="display: none;">
                                        <div class="row">
                                                <div class="col-md-6">
                                                        <h4 class="required" for="demanda">Resultado da ligação</h4>
                                                        <div class="row" style="margin-left: 1px">
                                                            <div class="column">
                                                                <div class="custom-control custom-radio">
                                                                        <input id="numeroinexistente" name="tipo_resultado" type="radio" class="custom-control-input" required="" value="4">
                                                                        <label class="custom-control-label" for="numeroinexistente">Número não existe</label>
                                                                </div>   

                                                                <div class="custom-control custom-radio">
                                                                        <input id="naoatendeu" name="tipo_resultado" type="radio" class="custom-control-input" required="" value="1">
                                                                        <label class="custom-control-label" for="naoatendeu">Não atendeu/caixa postal</label>
                                                                </div>
                                                                <div class="custom-control custom-radio">
                                                                        <input id="com_interesse" name="tipo_resultado" type="radio" class="custom-control-input" required="" value="3">
                                                                        <label class="custom-control-label" for="com_interesse">Demonstrou interesse</label>
                                                                </div>
                                                            </div>
                                                            <div class="column" style="margin-left: 10px">
                                                                <div class="custom-control custom-radio">

                                                                    
                                                                     <div class="custom-control custom-radio">
                                                                        <input id="pararligar" name="tipo_resultado" type="radio" class="custom-control-input" required="" value="5">
                                                                        <label class="custom-control-label" for="pararligar">Parar de ligar</label>
                                                                     </div>
                                                                     
                                                                <div class="custom-control custom-radio">
                                                                        <input id="seminteresse" name="tipo_resultado" type="radio" class="custom-control-input" required="" value="2">
                                                                        <label class="custom-control-label" for="seminteresse">Não demonstrou interesse</label>
                                                                </div>
                                                                   
                                                                      <div class="custom-control custom-radio">
                                                                        <input id="ligardepois" name="tipo_resultado" type="radio" class="custom-control-input" required="" value="6">
                                                                        <label class="custom-control-label" for="ligardepois">Pediu para ligar depois</label>
                                                                </div>   
                                                                </div>
                                                                                                                         
                                                            </div>
                                                        </div>  
                                                        <div id="div_data_ligar_depois">
                                            <div class="col-md-2 offset-5" style="margin-top: 10px; text-align: right;">
                                                <fieldset class="scheduler-border form-radius">
                                                <table>
                                                    <h7>Quando?</h7>
                                                    <tbody>             
                                                        <tr>
                                                            <td style="font-weight: bold;">Dia</td>
                                                            <td><input id="datepickerdepois" type="text" name="datepickerdepois" value="" style="text-align: center; width: 120; margin-left: 10px; background-color: white" class="form-radius" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-weight: bold;">Hora</td>
                                                            <td> <input id="timepicker" name="timepicker" class="timepicker timepicker-with-dropdown text-center form-radius" style="width: 120px; margin-left: 10px" />
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>    
                                                </fieldset>           
                                            </div>                                        
                                        </div>

                                </div>  
                                        <div class ="col-md-6">
                                                        
                                                     <h4 for="comentarios">Comentários</h4>
                                                        <textarea type="text" required class="form-control input-lg" id="comentarios" nome="comentarios" placeholder="" value="" rows="3" style="overflow:hidden"></textarea>
                                                    <div class="form-check">
                                                              <input class="form-check-input" type="checkbox" value="1" id="muito_interessado" name="muito_interessado">
                                                              <label class="form-check-label" for="defaultCheck1">
                                                                Demonstrou muito interesse pelas ideias da Conscienciologia?
                                                              </label>
                                                    </div>

                                                                                                              
                                        </div>                              
                                </div>
                                                 <div class="row" id="div_cursos_interesse">
                                                        <div class="col-md-6">
                                                                <h6 for="select_interesse1">Insira os cursos de interesse</h6>
                                                                <div>
                                                                        <input type="text" style="background-color: white;" class="form-control input-lg" id="select_interesse1">
                                                                        <input type='hidden' id='select_interesseID1' type='text' name='select_interesse[]'>
                                                                </div>                                                            
                                                                <div id="dynamicInput"></div>
                                                                <input style="color: blue; background-color: #f8f9fa; margin-top: 5px; font-size:13px;" type="button" value="Adicionar outro campo para interesse..." onClick="addInput('dynamicInput');">
                                                                <input style="color: blue; background-color: #f8f9fa; font-size:13px;" type="button" value="Remover último campo de interesse" onClick="removeInput()">
                                                        </div>
                                                </div>     
                                             <div class="row" style="margin-top:10px; margin-left: 5px">
                                                    <button class="btn btn-primary btn-lg" id="resultados">Confirmar</button>
                                            </div>   
                                        </div>

                                <div id="proximaligacao" style="display: none; text-align:center;">
                                        <h4>Deseja realizar outra ligação?</h4>
                                        <div class="row">
                                                <div class="col-md-4 offset-2">
                                                        <button class="btn btn-primary btn-lg btn-block" id="continuar">Sim, continuar TMK!</button>
                                                </div>
                                                <div class="col-md-4">
                                                        <button class="btn btn-primary btn-lg btn-block" id="encerrar">Não, encerrar TMK</button>
                                                </div>
                                        </div>
                                </div>
                        </form>    
                </div>                    
        </div>
            <div class="card"> <!-- background-color: #f8f9fa -->
                <div class="row card-body col-md-12" style="margin-top: 5px; padding-top: 30; display: none; text-align: center" id="div_carregando">
                    <div style="width: 100%; text-align: center; margin-top: 20px"><h5 style="color:#444547">Carregando...</h5></div>
                     <img class="d-block mx-auto mb-4" src="Images/loading.gif" width="280" height="280">       
                </div>
            </div>               
        </div>
            <div class="card"> <!-- background-color: #f8f9fa -->
                <div class="row card-body" style="margin-top: 5px; padding-top: 30;" id="div_iipcnet_infos">
                    <div class="col-md-6" >
                        <div style="height: 500px;">
                            <div id="div_relogar">
                                <button id="btn_relogar">Logar Novamente</button>
                                <!-- The Modal -->
                                <div id="modal_relogar" class="modal">
                                  <!-- Modal content -->
                                  <div class="modal-content">
                                    <span class="close" style="display: none">&times;</span>
                                    <p>Faça login abaixo e depois  
                                    <a href="#" onClick="recarregaAluno(); return false;"><b> CLIQUE AQUI</b></a></p> 

                                     <iframe name="ifriipcnet_relogar" src="http://iipcnet.iipc.org/sources/logonNew.asp" id="ifriipcnet_relogar" width="100%" height="100%"></iframe>                             
                                  </div>
                                </div>
                            </div>
                            <!-- Div com o IIPC Net -->
                            <iframe name="ifriipcnet" src="http://iipcnet.iipc.org/sources/logonNew.asp" id="iipcnet" width="100%" height="80%"></iframe>                                
                        </div>       
                        <div class="card" id="ligacoes_anteriores">
                            <div class="card-header" style="text-align: center;">
                                 <b>Últimas ligações</b></div>
                                 <div id="div_ligacoes_anteriores">                                    
                                  </div>
                        </div>                            
                    </div>

                    <div class="col-md-6 align-items-center" id="div_informacoes">
                        <!-- Div com as informações do contato -->
                        <div id="div_informacoes_login">
                           <form id="frm_login">
                             <div class="row offset-3">
                                <div class="col-md-6 offset-md-1">
                                    <label class="required" for="voluntario_selecionado">Qual o seu nome?</label>
                                </div>
                            </div>
                            <div class="col-md-6 offset-3">
                                <select class="custom-select d-block w-100" name="voluntario_selecionado" id="voluntario_selecionado" required>
                                    <option value=''>Escolha...</option>
                                    <?php 
                                        while ($linha_voluntario = $res_voluntario -> fetch_assoc()){
                                         ?>
                                            <option value="<?=$linha_voluntario['codigo']?>"><?=utf8_encode($linha_voluntario['nome'])?></option>  
                                         <?php 
                                        }  
                                    ?> 
                                </select>
                            </div>
                            <div id="logado">
                                <div class="form-login" style="text-align: left; width: 100%">             
                                    <div class="wrapper" style="text-align: center; margin-top: 30px">
                                        <button id="btn_logado" class="btn btn-primary btn-lg btn3d" style="width: 60%;">Já fiz login</button>
                                    </div>
                                </div>                                     
                            </div> 
                            </form>  
                        </div>                       
                    </div>
                </div>
            </div>               
        </div>
        </div>
        <script src="popper.min.js" crossorigin="anonymous"></script>
        <script src="bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>

        <div id="div_msg" title="Confirmar">

        </div>

      <?php include "rodape.php"; ?>
</BODY>

</HTML>