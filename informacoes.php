<?php
include "conectasql.php";
session_start();

$temas = $conexao->prepare("SELECT t.NOME FROM tema_interesse i, tema t where i.aluno_CODIGO = (?) and t.id = i.tema_id");
$temas -> bind_param("s",$_POST["codigo"]);
$temas -> execute();
$res_temas = $temas->get_result();
$temas -> close();

$cursos = $conexao->prepare("SELECT e.NOME FROM evento_interesse i, evento e where i.aluno_CODIGO = (?) and e.id = i.evento_id");
$cursos -> bind_param("s",$_POST["codigo"]);
$cursos -> execute();
$res_cursos = $cursos->get_result();
$cursos -> close();

$idade = $conexao->prepare("SELECT YEAR(CURDATE()) - YEAR(a.DATA_NASCIMENTO) - IF(STR_TO_DATE(CONCAT(YEAR(CURDATE()), '-', MONTH(a.DATA_NASCIMENTO), '-', DAY(a.DATA_NASCIMENTO)) ,'%Y-%c-%e') > CURDATE(), 1, 0) as IDADE FROM aluno a where a.CODIGO = (?)");
$idade -> bind_param("i",$_POST["codigo"]);
$idade -> execute();
$res_idade = $idade->get_result();
$idade -> close();

$telefones = $conexao->prepare("SELECT telefone1, telefone2, telefone3 FROM aluno a where a.CODIGO = (?)");
$telefones -> bind_param("s",$_POST["codigo"]);
$telefones -> execute();
$res_telefones = $telefones->get_result();
$telefones -> close();

$primeiravez = $conexao->prepare("SELECT distinct a.CODIGO, DATE_FORMAT(MIN(
    CASE t.DATA_INICIAL 
    WHEN t.DATA_INICIAL IS NULL THEN t.DATA_FINAL
    ELSE t.DATA_INICIAL
    end),'%d/%m/%Y') AS PRIMEIRAVEZ
from aluno a left join aluno_turma atu on a.CODIGO = atu.CODIGO_ALUNO 
    left join turma t on t.CODIGO = atu.CODIGO_TURMA
where a.CODIGO = (?)
   group by a.CODIGO");
$primeiravez -> bind_param("s",$_POST["codigo"]);
$primeiravez -> execute();
$res_primeiravez = $primeiravez->get_result();
$primeiravez -> close();

$ultimavez = $conexao->prepare("SELECT distinct a.CODIGO, DATE_FORMAT(MAX(
    CASE t.DATA_FINAL 
    WHEN t.DATA_FINAL <> NULL THEN t.DATA_FINAL
    ELSE t.DATA_INICIAL
    end),'%d/%m/%Y') AS ULTIMAVEZ
from aluno a left join aluno_turma atu on a.CODIGO = atu.CODIGO_ALUNO 
    left join turma t on t.CODIGO = atu.CODIGO_TURMA
where a.CODIGO = (?)
   group by a.CODIGO");
$ultimavez -> bind_param("s",$_POST["codigo"]);
$ultimavez -> execute();
$res_ultimavez = $ultimavez->get_result();
$ultimavez -> close();

$res_horarios = $conexao->prepare("SELECT h.* FROM horario h where h.aluno_CODIGO = (?)");
$res_horarios -> bind_param("s",$_POST["codigo"]);
$res_horarios -> execute();
$resposta_horarios = $res_horarios->get_result();
$horarios = $resposta_horarios -> fetch_assoc();

?>


<html>
<div style="width: 90%; margin:auto;">
    <div class="row" style="text-align: center;">
        <h5 class="mb-3" style="margin:auto;">Informações</h5>
    </div>
    <div class="row" style="text-align: center; margin-top:5px;">
        <table class="table table-sm table-striped table-bordered">
                    <tbody>             
                        <tr>
                            <td rowspan="3" align="left" style="font-weight: bold; width: 50%; vertical-align: middle !important;">Telefones</td>
                            <td><?php $linha_telefones = $res_telefones -> fetch_assoc(); echo utf8_encode($linha_telefones['telefone1'])?></td></tr>
                            <tr><td><?php if($linha_telefones['telefone2'] != ""){
                                    echo utf8_encode($linha_telefones['telefone2']);}else echo "-"; ?>
                                    </td></tr>    
                            <tr><td><?php if($linha_telefones['telefone3'] != ""){
                                    echo utf8_encode($linha_telefones['telefone3']);}else echo "-"; ?>
                                    </td></tr>                          
                        <tr>
                            <td style="font-weight: bold; width: 50%">Idade</td>
                            <td><?php $linha_idade = $res_idade -> fetch_assoc(); echo utf8_encode($linha_idade['IDADE'])?></td>
                        </tr>
                         <tr>
                            <td style="font-weight: bold;">Primeira vez no IIPC</td>
                            <td><?php $linha_primeiravez = $res_primeiravez -> fetch_assoc(); echo utf8_encode($linha_primeiravez['PRIMEIRAVEZ'])?></td>
                        </tr>                        
                        <tr>
                            <td style="font-weight: bold;">Última vez no IIPC</td>
                            <td><?php $linha_ultimavez = $res_ultimavez -> fetch_assoc(); echo utf8_encode($linha_ultimavez['ULTIMAVEZ'])?></td>
                        </tr>

                        <tr>
                            <td style="font-weight: bold;">Cursos de Interesse</td>
                            <td>
                                <?php
                                    while ($linha_curso = $res_cursos -> fetch_assoc()){
                                ?>
                                <?=utf8_encode($linha_curso['NOME'])?>;<br/>
                                <?php
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">Temas de Interesse</td>
                            <td>
                                <?php
                                    while ($linha_temas = $res_temas -> fetch_assoc()){
                                    ?>
                                    <?=utf8_encode($linha_temas['NOME'])?>;<br/> 
                                    <?php
                                        }
                                    ?>
                            </td>
                        </tr>
                    </tbody>
                 </table>    
    </div>
        <div name="curso" class="row"></div>
        <div class="row" style="text-align: center;">
            <h5 class="mb-3" style="margin:auto; margin-top:5px;">Disponibilidade de Horários</h5>
        </div>
        <table class="table table-sm table-striped table-bordered text-center">
        <thead>
          <tr>
            <th>Dia</th>
            <th>Manhã</th>
            <th>Tarde</th>
            <th>Noite</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Segunda</td>
            <td> <?php
                    if($horarios['segunda_manha'] == 1){
                       echo "x";
                    }
                ?>                
            </td>
            <td> <?php
                    if($horarios['segunda_tarde'] == 1){
                       echo "x";
                    }
                ?>                
            </td>
            <td> <?php
                    if($horarios['segunda_noite'] == 1){
                       echo "x";
                    }
                ?>                
            </td>
          </tr>
          <tr>        
            <td>Terça</td>
            <td> <?php
                    if($horarios['terca_manha'] == 1){
                       echo "x";
                    }
                ?>                
            </td>
            <td> <?php
                    if($horarios['terca_tarde'] == 1){
                       echo "x";
                    }
                ?>                
            </td>
            <td> <?php
                    if($horarios['terca_noite'] == 1){
                       echo "x";
                    }
                ?>                
            </td>
          </tr>
          <tr>        
            <td>Quarta</td>
            <td> <?php
                    if($horarios['quarta_manha'] == 1){
                       echo "x";
                    }
                ?>                
            </td>
            <td> <?php
                    if($horarios['quarta_tarde'] == 1){
                       echo "x";
                    }
                ?>                
            </td>
            <td> <?php
                    if($horarios['quarta_noite'] == 1){
                       echo "x";
                    }
                ?>                
            </td>
          </tr>
          <tr>        
            <td>Quinta</td><td> <?php
                    if($horarios['quinta_manha'] == 1){
                       echo "x";
                    }
                ?>                
            </td>
            <td> <?php
                    if($horarios['quinta_tarde'] == 1){
                       echo "x";
                    }
                ?>                
            </td>
            <td> <?php
                    if($horarios['quinta_noite'] == 1){
                       echo "x";
                    }
                ?>                
            </td>
          </tr>
          <tr>        
            <td>Sexta</td>
            <td> <?php
                    if($horarios['sexta_manha'] == 1){
                       echo "x";
                    }
                ?>                
            </td>
            <td> <?php
                    if($horarios['sexta_tarde'] == 1){
                       echo "x";
                    }
                ?>                
            </td>
            <td> <?php
                    if($horarios['sexta_noite'] == 1){
                       echo "x";
                    }
                ?>                
            </td>
          </tr>
          <tr>        
            <td>Sábado</td>
            <td> <?php
                    if($horarios['sabado_manha'] == 1){
                       echo "x";
                    }
                ?>                
            </td>
            <td> <?php
                    if($horarios['sabado_tarde'] == 1){
                       echo "x";
                    }
                ?>                
            </td>
            <td> <?php
                    if($horarios['sabado_noite'] == 1){
                       echo "x";
                    }
                ?>                
            </td>
          </tr>
          
          <tr>        
            <td>Domingo</td>        
            <td> <?php
                    if($horarios['domingo_manha'] == 1){
                       echo "x";
                    }
                ?>                
            </td>
            <td> <?php
                    if($horarios['domingo_tarde'] == 1){
                       echo "x";
                    }
                ?>                
            </td>
            <td> <?php
                    if($horarios['domingo_noite'] == 1){
                       echo "x";
                    }
                ?>                
            </td>
          </tr>     
        </tbody>
      </table>
       <h5 style="margin-top: 20px; margin-bottom: 10px; text-align: center">Cursos Realizados</h5>
      <div class="card" id="cursos_ja_feitos" style="border-style: none">
            <input type="text" class="filterInput form-radius" id="filtroCursosFeitos" name="filtroCursosFeitos" onkeyup="filtraTabela()" placeholder="Digite o curso para pesquisar.." style="padding-left: 6px">
          <div id="div_cursos_ja_feitos"></div>
      </div>
</div>

    <!--
    <div class="row" style="text-align: center; margin-top:12px;">
        <h4 class="mb-3" style="margin:auto;">Livros Adquiridos</h4>
    </div>
    <div name="livros" class="row" id="livros">
    </div>
    !-->
</div>

</html>