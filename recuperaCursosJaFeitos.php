<?php
include "conectasql.php";
session_start();

$cursos_feitos = $conexao->prepare("SELECT e.NOME as curso, e.id, DATE_FORMAT(IFNULL(t.DATA_FINAL, t.DATA_INICIAL),'%d/%m/%Y') AS data FROM aluno_turma atu INNER JOIN turma t on t.CODIGO = atu.CODIGO_TURMA INNER JOIN evento_ativo e on e.id=t.ID_EVENTO WHERE atu.CODIGO_ALUNO = (?) order by IFNULL(t.DATA_FINAL, t.DATA_INICIAL) desc");

$cursos_feitos -> bind_param("s",$_POST["codigo_aluno"]);
$cursos_feitos -> execute();
$res_cursos_feitos = $cursos_feitos->get_result();
$cursos_feitos -> close();


?>

 	<table class="table table-sm  table-bordered table-striped filterableTable" id="tabela_cursos_feitos" name="tabela_cursos_feitos">
        <thead style="text-align: center;"> 
        	<th></th>         
         </thead>
         <tbody> 
                <?php
                    while ($linha_cursos_feitos = $res_cursos_feitos -> fetch_assoc()){
                    	?><tr>
	                  	    <td><?=utf8_encode($linha_cursos_feitos['data'])?></td>
                            <td><?=utf8_encode($linha_cursos_feitos['curso'])?></td>
                         </tr>
                <?php
                    }
                ?>
        </tbody>
     </table>  





