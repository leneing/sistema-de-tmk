<?php
include "conectasql.php";
session_start();


//se tinha no banco ligações pendentes para esse aluno, marca todas como não pendentes mais
$cod = $_POST['aluno_codigo'];
$sqlUpdate = mysqli_query($conexao, "UPDATE ligacoes_tmk set fl_pendente_retorno = 0 where aluno_CODIGO = '$cod'");

// se o resultado foi Pediu pra ligar depois, marca como pendente e grava a data para ligar
if($_POST["tipo_resultado"] == 6){
	$fl_pendente_retorno = 1;

	$d = $_POST["datepickerdepois"];
	$t = $_POST["timepicker"];
	$srt_data_retorno = $d . " " . $t . ":00"; 
	
	$date = $srt_data_retorno;
	$date = str_replace('/', '-', $date);
	$data_retorno = date('Y-m-d H:i:s', strtotime($date));

}else{
	$fl_pendente_retorno = 0;
	$data_retorno = NULL;
	$hora_retorno = NULL;
}

$comentarios = utf8_decode($_POST["comentarios"]);

if($_POST['param_curso_selecionado'] != ""){
	$curso_sel = $_POST['param_curso_selecionado'];
}else{
  	$curso_sel = $_POST["curso_selecionado"];
}

$sql = $conexao->prepare("INSERT INTO ligacoes_tmk (aluno_CODIGO, data, evento_id, comentarios, sessaotmk_id, tipo_resultado, fl_pendente_retorno, data_retorno, id_select_lista)
                            VALUES (?, now(), ?, ?, ?, ?, ?, ?, ?)");  

$sql ->bind_param("iisiiisi",$_POST["aluno_codigo"],$curso_sel,$comentarios,$_SESSION['ID_SESSAO'],$_POST["tipo_resultado"], $fl_pendente_retorno, $data_retorno,$_POST['lista_selecionada']); 

$res = $sql->execute();
$sql->close();

$id_ligacao = mysqli_insert_id($conexao);
mysqli_commit($conexao);
if(is_numeric($id_ligacao) && $id_ligacao > 0){
	echo 1; // Deu certo
}


//grava registro na tabela fecha venda
if($_POST["tipo_resultado"] == 3){
	$turma = $conexao->prepare("SELECT sl.id_turma FROM select_lista sl WHERE sl.id = (?)");
	$turma -> bind_param("i",$_POST["lista_selecionada"]);
	$turma -> execute();
	$res_turma = $turma->get_result();
	$turma -> close();
	//var_dump($res_turma);
	$id_turma = $res_turma-> fetch_assoc();
	//var_dump($id_turma);
	$sql = $conexao->prepare("INSERT INTO fecha_venda (id_turma, id_ligacao_origem) VALUES (?, ?)");  
	$sql ->bind_param("ii",$id_turma['id_turma'],$id_ligacao); 
	$res = $sql->execute();
	$sql->close();
}
//var_dump($curso_sel);
//grava se for uma ligação de retorno para fechar venda
if($_POST["tipo_resultado"] == 3 OR $_POST["tipo_resultado"] == 2 OR $_POST["tipo_resultado"] == 5){

	$verifica_fecha_venda = $conexao->prepare("SELECT f.* FROM fecha_venda f INNER join ligacoes_tmk l on f.id_ligacao_origem=l.id inner join turma t on t.codigo = f.id_turma WHERE l.aluno_CODIGO = ? AND t.DATA_INICIAL >= curdate() AND t.id_evento = ? AND f.id_ligacao_fecha_venda IS NULL");
	$verifica_fecha_venda -> bind_param("ss",$_POST["aluno_codigo"],$curso_sel);
	$verifica_fecha_venda -> execute();
	$res_verifica_fecha_venda = $verifica_fecha_venda->get_result();
	$verifica_fecha_venda -> close();

	//var_dump($res_verifica_fecha_venda);
	if($res_verifica_fecha_venda->num_rows > 0){
		$id_fecha_venda = $res_verifica_fecha_venda-> fetch_assoc();
		//var_dump($id_fecha_venda);

		$sql = $conexao->prepare("UPDATE fecha_venda SET id_ligacao_fecha_venda = (?) WHERE id_fecha_venda = (?)");  
		$sql ->bind_param("ii",$id_ligacao,$id_fecha_venda['id_fecha_venda']); 
		$res = $sql->execute();
		$sql->close();
	}
}


// grava os cursos de interesse do aluno no banco
$sql = $conexao->prepare("INSERT INTO evento_interesse (evento_id, aluno_CODIGO, data_alteracao, id_ligacao) VALUES (?,?,now(),?)");

$i = 0;

if(isset($_POST['select_interesse'])){
	while ($i < count($_POST["select_interesse"])){
	    $sql -> bind_param("isi",$_POST["select_interesse"][$i],$_POST["aluno_codigo"],$id_ligacao);
	    $i++;
	    $sql->execute();	    
	}
}
//var_dump($_POST['select_interesse']);
$sql->close();


// preenche os dados do aluno identificados na ligacao na tabela complementar dados_aluno
$sql = $conexao->prepare("INSERT INTO dados_aluno (aluno_CODIGO, fl_muito_interessado, fl_parar_de_ligar, fl_telefone_nao_existe, data_alteracao)
                            VALUES (?, ?, ?, ?, now())");   


if($_POST["tipo_resultado"] == "4" OR $_POST["tipo_resultado"] == "5" OR isset($_POST['muito_interessado']) ){
	if(isset($_POST['muito_interessado'])){
		$fl_muito_interessado = 1;
	}else{
		$fl_muito_interessado = 0;
	}
	if($_POST["tipo_resultado"] == "5"){
		$fl_parar_de_ligar = 1;
	}else{
		$fl_parar_de_ligar = 0;
	}
	if($_POST["tipo_resultado"] == "4"){
		$fl_telefone_nao_existe = 1;
	}else{
		$fl_telefone_nao_existe = 0;
	}

	$sql ->bind_param("iiii",$_POST["aluno_codigo"], $fl_muito_interessado, $fl_parar_de_ligar, $fl_telefone_nao_existe); 
	$sql->execute();
	$sql->close();

}

 unset ($_SESSION['aluno_CODIGO']);

$conexao->close();
?>