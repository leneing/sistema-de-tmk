<?php
error_reporting(E_WARNING);
include "conectasql.php";
session_start();

$turmas_ativas_filtro = "SELECT DISTINCT e.NOME, e.id FROM turma t, evento e, select_lista l WHERE t.DATA_INICIAL > CURDATE() and e.id = t.ID_EVENTO and l.id_turma = t.CODIGO";  
$res_turma = $conexao ->query($turmas_ativas_filtro);


?>

<HTML>

<HEAD>
        <TITLE>IIPC BH - TMK</TITLE>

        <link rel="stylesheet" href="jQuery-Plugin-For-Multi-Select-Checboxes-multiselect/css/jquery.multiselect.css">

        <link rel="stylesheet" href="bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="estilo.css">

        <script src="jquery-3.3.1.min.js" crossorigin="anonymous"></script>
        <script src="jQuery-Plugin-For-Multi-Select-Checboxes-multiselect/js/jquery.multiselect.js"></script>
        <script src="jquery-ui.js"></script>
        <script src="combobox.js"></script>


        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

        <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

        <style type="text/css">
            .form-center{   
                width: 20%;
                padding: .375rem .75rem;
                font-size: 1rem;
                line-height: 1.5;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                border-radius: .25rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            }
            .form-radius{  
                font-size: 1rem;
                line-height: 1.5;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                border-radius: .25rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            }

            fieldset.scheduler-border {   
                text-align: center;
                background-color: #f7f9fc;
                min-width: 200px;             
                padding: 10px !important;
                -webkit-box-shadow:  0px 0px 0px 0px #000;
                        box-shadow:  0px 0px 0px 0px #000;
            }
        </style>

        <script>
            $(document).ready(function () {   
                $(".filtro_alunos_check").change(function () {
                    if ($(this).is(":checked")){
                        $(this).parent().parent().css("background-color","#adf7aa");
                        $(this).attr('value',1);
                    }else {
                        $(this).parent().parent().css("background-color","white");
                        $(this).attr('value',0);
                    }
                    carrega_contatos();
                });

                $("#turma_selecionada").change(function (){
                    $("#quantidade_1").text('0');
                    $("#quantidade_2").text('0');
                    $("#quantidade_3").text('0');
                    $("#quantidade_4").text('0');
                    $("#quantidade_5").text('0');
                    $("#quantidade_6").text('0');
                    $("#quantidade_7").text('0');
                    $("#quantidade_0").text('0');

                    carrega_contatos();
                });

                $('#curso_selecionado').change(function(){
                    $("#quantidade_1").text('0');
                    $("#quantidade_2").text('0');
                    $("#quantidade_3").text('0');
                    $("#quantidade_4").text('0');
                    $("#quantidade_5").text('0');
                    $("#quantidade_6").text('0');
                    $("#quantidade_7").text('0');
                    $("#quantidade_0").text('0');
                    $.ajax({
                        url: 'Turma_por_Curso.php',
                        type: 'post',
                        data: "curso_selecionado="+$("#curso_selecionado").val(),
                        dataType: 'json',
                        success: function(resultado_turmas){
                            $("#turma_selecionada").empty()
                            $("#turma_selecionada").append(
                                    $('<option></option>').val('0').html('Escolha...')
                                );
                            var i;
                            for (i = 0; i < resultado_turmas.length; ++i) {
                                $("#turma_selecionada").append(
                                    $('<option></option>').val(resultado_turmas[i]['id']).html(resultado_turmas[i]['nome'])
                                );
                            }  
                        }
                    });
                    $("#body_tabela").empty();
                }); 
            });

            carrega_contatos = function(){
                $("#body_tabela").empty();
                if ($("#turma_selecionada").val() != 0){
                    $.ajax({
                        url: 'busca_Alunos_contatados.php',
                        type: 'post',
                        data: "estrela="+$("#check_estrela").attr("value")+"&interesse="+$("#check_interesse").attr("value")+"&sem_interesse="+$("#check_seminteresse").attr("value")+"&ligardepois="+$("#check_ligardepois").attr("value")+"&naoatendeu="+$("#check_naoatendeu").attr("value")+"&pulada="+$("#check_pulada").attr("value")+"&naoexiste="+$("#check_naoexiste").attr("value")+"&parar="+$("#check_parar").attr("value")+"&turma="+$("#turma_selecionada").val(),
                        dataType: 'json',
                        success: function(alunos_contatados){
                            for (i = 0; i < alunos_contatados.length; ++i) {
                                if (alunos_contatados[i]['estrela'] > 0){
                                    linha = "<tr style='background-color: #ffccff'>";
                                }else {if(alunos_contatados[i]['interesse'] > 0) {
                                    linha = "<tr style='background-color: #adf7aa'>";
                                }else {if(alunos_contatados[i]['sem_interesse'] > 0){
                                    linha = "<tr style='background-color: #ff9999'>";
                                }else{
                                    linha = "<tr style='background-color: #e8e8e8'>";
                                }}}

                                var coluna_1;
                                if (alunos_contatados[i]['estrela'] == 0){
                                    coluna_1 = "<td scope='row' style='width: 8%'><button class='btn btn-success btn-sm btn_ativar' aluno_selecionado = "+alunos_contatados[i]['aluno_codigo']+" fl_ativo ='0'>Acompanhar</button></td>";
                                }else{
                                    coluna_1 = "<td scope='row' style='width: 8%'><button class='btn btn-danger btn-sm btn_inativar' aluno_selecionado = "+alunos_contatados[i]['aluno_codigo']+" fl_ativo ='1'>Não acompanhar</button></td>";
                                }

                                coluna_2 = "<td scope='row' style='width: 8%'><button class='btn btn-sm btn_expandir' aluno_selecionado = "+alunos_contatados[i]['aluno_codigo']+" fl_ativo ='0'>Expandir +</button></td>";
                                coluna_3 = "<td style='text-align: center'>"+alunos_contatados[i]['aluno_codigo']+"</td>";
                                coluna_4 = "<td style='font-weight: bold;'>"+alunos_contatados[i]['nome']+"</td>";
                                coluna_5 = "<td style='text-align: center'>"+alunos_contatados[i]['contatos']+"</td>";
                                coluna_6 = "<td style='text-align: center'>"+alunos_contatados[i]['ultimo_contato']+"</td></tr>";

                                $("#body_tabela").append(linha + coluna_1 + coluna_2 + coluna_3 + coluna_4 + coluna_5 + coluna_6);
                            }
                            carrega_butoes();
                            carrega_quantidade();
                        }
                    });
                }  
                return false;
            }

            carrega_butoes = function (){
                $(".btn_ativar").click(function () {
                    $.ajax({
                        url: 'resultadoAcompanharAluno.php',
                        type: 'post',
                        data: "aluno_selecionado="+$(this).attr("aluno_selecionado")+"&fl_ativo="+$(this).attr("fl_ativo")+"&turma="+$("#turma_selecionada").val(),
                        success: function(result_grava_banco){
                            carrega_contatos();
                        }
                        });  
                    return false;
                });

                $(".btn_inativar").click(function () {
                    $.ajax({
                        url: 'resultadoAcompanharAluno.php',
                        type: 'post',
                        data: "aluno_selecionado="+$(this).attr("aluno_selecionado")+"&fl_ativo="+$(this).attr("fl_ativo")+"&turma="+$("#turma_selecionada").val(),
                        success: function(result_grava_banco){
                            carrega_contatos();
                        }
                        });  
                    return false;
                }); 

                $(".btn_expandir").click(function () {
                    if ($(this).attr("fl_ativo") == 0){
                        $(this).text('Ocultar -');
                        $(this).attr("fl_ativo",1);  
                        botao = $(this);                    
                        $.ajax({
                            url: 'busca_ligacoes_aluno.php',
                            type: 'post',
                            data: "aluno_selecionado="+botao.attr("aluno_selecionado")+"&turma="+$("#turma_selecionada").val(),
                            dataType: 'json',
                            success: function(ligacoes_aluno){
                                for (i = 0; i < ligacoes_aluno.length; ++i) {
                                    botao.parent().parent().after("<tr class="+botao.attr("aluno_selecionado")+"><td></td><td></td><td></td><td>"+ligacoes_aluno[i]['value']+"</td><td style='text-align: center'>"+ligacoes_aluno[i]['label']+"</td><td style='text-align: center'>"+ligacoes_aluno[i]['id']+"</td></tr>");
                                }
                                botao.parent().parent().after("<tr class="+botao.attr("aluno_selecionado")+"><td></td><td></td><td></td><td style='text-align: center; background-color:"+botao.parent().parent().css('backgroundColor')+"'>Comentários</td><td style='text-align: center; background-color:"+botao.parent().parent().css('backgroundColor')+"'>Resultado</td><td style='text-align: center; background-color:"+botao.parent().parent().css('backgroundColor')+"'>Data</td></tr>");
                            }
                        }); 
                    }else{
                        $(this).text('Expandir +');
                        $(this).attr("fl_ativo",0);
                        $('.'+$(this).attr("aluno_selecionado")).each(function() {
                            $(this).remove();
                        });
                    }  
                    return false;
                });
            };

            carrega_quantidade = function(){
                $.ajax({
                    url: 'conta_alunos_contatadosturma',
                    type: 'post',
                    data: "turma="+$("#turma_selecionada").val(),
                    dataType: 'json',
                    success: function(quantidade){
                        for (i = 0; i < quantidade.length; ++i) {
                            $("#quantidade_"+quantidade[i]['tipo']).text(quantidade[i]['quantidade']);
                        }
                    }
                });
            }

        </script>
</HEAD>

<BODY class="bg-light">
        <?php
            include "navbar_adm.php";
        ?> 
        <div class="container" style="margin-top: 15px;">
            <input type="hidden" id="aluno_codigo"/>
            <div class="text-center" id="div_titulo">       
                <img class="d-block mx-auto mb-4" src="Images/icolistapan.png" alt="" width="72" height="72">          
                <h2>Controle de Turmas</h2><br/>
                <p class="lead">Permite gerenciar os alunos contactados para cada turma</p>                                    
            </div>    
            
            <form id="frm_filtros">
                <div class="row mb-5">
                    <div class="col-md-4 offset-1">
                        <div class="row" style="text-align: center;">
                            <div class="col-md-12 text-center">
                                <label for="curso_selecionado">Selecione o curso que deseja acompanhar</label>
                            </div>
                        </div>
                        <div class="row mb-3" style="text-align: center;">
                            <select class="custom-select d-block w-100" name="curso_selecionado" id="curso_selecionado">
                                <option value=''>Escolha...</option>
                                <?php 
                                    while ($linha_turma = $res_turma -> fetch_assoc()){
                                    ?>
                                        <option value="<?=$linha_turma['id']?>"><?=UTF8_ENCODE($linha_turma['NOME'])?></option>  
                                    <?php 
                                    }  
                                ?>
                            </select>
                        </div>

                        <div class="row" style="text-align: center;">
                            <div class="col-md-12 text-center">
                                <label for="turma_selecionada">Selecione a turma que deseja acompanhar</label>
                            </div>
                        </div>

                        <div class="row" style="text-align: center;">
                            <select class="custom-select d-block w-100" name="turma_selecionada" id="turma_selecionada" required>
                                <option value=''>Escolha...</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 offset-1">
                        <table class="table table-bordered table-hover" style="font-size: small">
                            <thead style="text-align: center">
                                <tr style="text-align: center"></tr>
                                <tr>
                                    <th></th>
                                    <th style="text-align: center; width: 60%">Tipo de resultado</th>
                                    <th style="text-align: center">Quantidade de alunos</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr style="background-color: #adf7aa">
                                    <th><input type="checkbox" class="filtro_alunos_check" id="check_estrela" value="1" checked></th>
                                    <th style="text-align: center; width: 60%; font-weight:normal">Alunos acompanhados</th>
                                    <th style="text-align: center; font-weight:normal" id="quantidade_0">0</th>
                                </tr>
                                <tr style="background-color: #adf7aa">
                                    <th><input type="checkbox" class="filtro_alunos_check" id="check_interesse" value="1" checked></th>
                                    <th style="text-align: center; width: 60%; font-weight:normal">Demonstraram interesse</th>
                                    <th style="text-align: center; font-weight:normal" id="quantidade_3">0</th>
                                </tr>
                                <tr style="background-color: #adf7aa">
                                    <th><input type="checkbox" class="filtro_alunos_check" id="check_seminteresse" value="1" checked></th>
                                    <th style="text-align: center; width: 60%; font-weight:normal">Não demonstraram interesse</th>
                                    <th style="text-align: center; font-weight:normal" id="quantidade_2">0</th>
                                </tr>
                                <tr>
                                    <th><input type="checkbox" class="filtro_alunos_check" id="check_ligardepois" value="0"></th>
                                    <th style="text-align: center; width: 60%; font-weight:normal">Pediu para ligar depois</th>
                                    <th style="text-align: center; font-weight:normal" id="quantidade_6">0</th>
                                </tr>
                                <tr>
                                    <th><input type="checkbox" class="filtro_alunos_check" id="check_naoatendeu" value="0"></th>
                                    <th style="text-align: center; width: 60%; font-weight:normal">Não atendeu</th>
                                    <th style="text-align: center; font-weight:normal" id="quantidade_1">0</th>
                                </tr>
                                <tr>
                                    <th><input type="checkbox" class="filtro_alunos_check" id="check_pulada" value="0"></th>
                                    <th style="text-align: center; width: 60%; font-weight:normal">Ligação pulada</th>
                                    <th style="text-align: center; font-weight:normal" id="quantidade_7">0</th>
                                </tr>
                                <tr>
                                    <th><input type="checkbox" class="filtro_alunos_check" id="check_naoexiste" value="0"></th>
                                    <th style="text-align: center; width: 60%; font-weight:normal">Número não existe</th>
                                    <th style="text-align: center; font-weight:normal" id="quantidade_4">0</th>
                                </tr>
                                <tr>
                                    <th><input type="checkbox" class="filtro_alunos_check" id="check_parar" value="0"></th>
                                    <th style="text-align: center; width: 60%; font-weight:normal">Parar de ligar</th>
                                    <th style="text-align: center; font-weight:normal" id="quantidade_5">0</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </form>                            

            <div class="row">
                <div class="col-md-12">                            
                    <form id="frm_resultado">  
                        <div class="row" style="text-align: center; margin-top:5px;">
                              <table class="table table-bordered table-hover" style="font-size: small" id="table">
                                <thead style="text-align: center">
                                    <tr>
                                        <th></th>
                                        <th style="width: 8% "></th>
                                        <th style="width: 5% text-align: center">Código IIPC Net</th>
                                        <th style="max-width: 3 5%; width: 35%; text-align: center">Aluno</th>
                                        <th style="width: 8% text-align: center">Número de Contatos</th>
                                        <th style="width: 20% text-align: center">Último Contato</th>
                                    </tr>
                                </thead>
                                <tbody id="body_tabela"> 
                                     
                                </tbody>
                             </table>  
                        </div>
                    </form>         
            </div>
        </div>
        <script src="popper.min.js" crossorigin="anonymous"></script>
        <script src="bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>

      <?php include "rodape.php"; ?>
</BODY>

</HTML>