<?php
error_reporting(E_WARNING);
include "conectasql.php";
session_start();

$cursos_ativos = "SELECT DISTINCT e.* FROM `evento_ativo` e where e.ativo = 1";
$res_curso_ativo = $conexao ->query($cursos_ativos);
$res_curso_ativo2 = $conexao ->query($cursos_ativos);
$res_curso_ativo3 = $conexao ->query($cursos_ativos);
$res_curso_ativo4 = $conexao ->query($cursos_ativos);

$cursos_cap  = "SELECT DISTINCT e.id, e.nome, e.ativo, MAX( t.data_inicial ) 
                    FROM evento_ativo e
                    INNER JOIN turma t ON t.id_evento = e.id
                    WHERE t.cidade = LOWER(  'Belo Horizonte' ) 
                    AND t.data_inicial >= DATE_SUB( NOW( ) , INTERVAL 4 YEAR ) 
                    AND e.nome LIKE  '%CAP%'
                    GROUP BY e.id, e.nome, e.ativo";
$res_cursos_cap = $conexao ->query($cursos_cap);

$cursos_cl = "SELECT DISTINCT e.id, e.nome, e.ativo, MAX( t.data_inicial ) 
                FROM evento_ativo e
                INNER JOIN turma t ON t.id_evento = e.id
                WHERE t.cidade = LOWER(  'Belo Horizonte' ) 
                AND t.data_inicial >= DATE_SUB( NOW( ) , INTERVAL 4 YEAR ) 
                AND e.nome LIKE  '%CL %'
                GROUP BY e.id, e.nome, e.ativo";
$res_cursos_cl = $conexao ->query($cursos_cl);

?>

<HTML>

<HEAD>
        <TITLE>IIPC BH - TMK</TITLE>

        <link rel="stylesheet" href="jQuery-Plugin-For-Multi-Select-Checboxes-multiselect/css/jquery.multiselect.css">

        <link rel="stylesheet" href="bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="estilo.css">

        <script src="jquery-3.3.1.min.js" crossorigin="anonymous"></script>
        <script src="jQuery-Plugin-For-Multi-Select-Checboxes-multiselect/js/jquery.multiselect.js"></script>
        <script src="jquery-ui.js"></script>
        <script src="combobox.js"></script>


        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

        <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

        <style type="text/css">
            .form-center{   
                width: 20%;
                padding: .375rem .75rem;
                font-size: 1rem;
                line-height: 1.5;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                border-radius: .25rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            }
            .form-radius{  
                font-size: 1rem;
                line-height: 1.5;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                border-radius: .25rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            }

            fieldset.scheduler-border {   
                text-align: center;
                background-color: #f7f9fc;
                min-width: 200px;             
                padding: 10px !important;
                -webkit-box-shadow:  0px 0px 0px 0px #000;
                        box-shadow:  0px 0px 0px 0px #000;
            }
        </style>

        <script>
                var counterTC = 1;
                var id_tc = [];
                function addInputTC(divName){
                        counterTC++;
                        var newdiv = document.createElement('div');
                        newdiv.id = "input_todos_cursos_child"+counterTC;
                        newdiv.innerHTML = " <br><input id='select_todos_cursos"+ counterTC +"' style='background-color: white;' type='text' class='form-control input-lg interesse_extra' name='select_todos_cursos[]'>";
                        
                       document.getElementById(divName).appendChild(newdiv);

                        $( "#select_todos_cursos"+counterTC+"").autocomplete({
                                source: "listacurso.php",
                                minLength: 3, 
                                open: function(event) {
                                valid = false;
                                },
                                close: function(event){
                                    if (!valid) $(this).val('');
                                },
                                select: function (event, ui) {
                                    valid = true;
                                        id_tc[counterTC] = ui.item.id;
                                }
                        });  
                }

                function removeInputTC(){
                        if (counterTC > 1) {
                                $( "#input_todos_cursos_child"+counterTC ).remove();
                                id_tc[counterTC] = '';
                                counterTC--;
                        }else{alert("É necessário preencher ao menos um campo de interesse!");}
                }

                var counterNC = 1;
                var id_nc = [];
                function addInputNC(divName){
                        counterNC++;
                        var newdiv = document.createElement('div');
                        newdiv.id = "input_nenhum_curso_child"+counterNC;
                        newdiv.innerHTML = " <br><input id='select_nenhum_curso"+ counterNC +"' style='background-color: white;' type='text' class='form-control input-lg interesse_extra' name='select_nenhum_curso[]'>";
                        
                       document.getElementById(divName).appendChild(newdiv);

                        $( "#select_nenhum_curso"+counterNC+"").autocomplete({
                                source: "listacurso.php",
                                minLength: 3, 
                                open: function(event) {
                                valid = false;
                                },
                                close: function(event){
                                    if (!valid) $(this).val('');
                                },
                                select: function (event, ui) {
                                    valid = true;
                                        id_nc[counterNC] = ui.item.id;
                                }
                        });  
                }

                function removeInputNC(){
                        if (counterNC > 1) {
                                $( "#input_nenhum_curso_child"+counterNC ).remove();
                                id_nc[counterNC] = '';
                                counterNC--;
                        }else{alert("É necessário preencher ao menos um campo de interesse!");}
                }

                var counterAC = 1;
                var id_ac = [];
                function addInputAC(divName){
                        counterAC++;
                        var newdiv = document.createElement('div');
                        newdiv.id = "input_algum_curso_child"+counterAC;
                        newdiv.innerHTML = " <br><input id='select_algum_curso"+ counterAC +"' style='background-color: white;' type='text' class='form-control input-lg interesse_extra' name='select_algum_curso[]'>";
                        
                       document.getElementById(divName).appendChild(newdiv);

                        $( "#select_algum_curso"+counterAC+"").autocomplete({
                                source: "listacurso.php",
                                minLength: 3, 
                                open: function(event) {
                                valid = false;
                                },
                                close: function(event){
                                    if (!valid) $(this).val('');
                                },
                                select: function (event, ui) {
                                    valid = true;
                                        id_ac[counterAC] = ui.item.id;
                                }
                        });  
                }

                function removeInputAC(){
                        if (counterAC > 1) {
                                $( "#input_algum_curso_child"+counterAC ).remove();
                                id_ac[counterAC] = '';
                                counterAC--;
                        }else{alert("É necessário preencher ao menos um campo de interesse!");}
                }

                var counterE = 1;
                var id_e = [];
                function addInputE(divName){
                        counterE++;
                        var newdiv = document.createElement('div');
                        newdiv.id = "input_evento_child"+counterE;
                        newdiv.innerHTML = " <br><input id='select_evento"+ counterE +"' style='background-color: white;' type='text' class='form-control input-lg interesse_extra' name='select_evento[]'>";
                        
                       document.getElementById(divName).appendChild(newdiv);

                        $( "#select_evento"+counterE+"").autocomplete({
                                source: "listacurso.php",
                                minLength: 3, 
                                open: function(event) {
                                valid = false;
                                },
                                close: function(event){
                                    if (!valid) $(this).val('');
                                },
                                select: function (event, ui) {
                                    valid = true;
                                        id_e[counterE] = ui.item.id;
                                }
                        });  
                }

                function removeInputE(){
                        if (counterE > 1) {
                                $( "#input_evento_child"+counterE ).remove();
                                id_e[counterE] = '';
                                counterE--;
                        }else{alert("É necessário preencher ao menos um campo de interesse!");}
                }

                var counterTI = 1;
                var id_TI = [];
                function addInputTI(divName){
                        counterTI++;
                        var newdiv = document.createElement('div');
                        newdiv.id = "input_tema_child"+counterTI;
                        newdiv.innerHTML = " <br><input id='select_tema_interesse"+ counterTI +"' style='background-color: white;' type='text' class='form-control input-lg interesse_extra' name='select_tema_interesse[]'>";
                        
                       document.getElementById(divName).appendChild(newdiv);

                        $( "#select_tema_interesse"+counterTI+"").autocomplete({
                                source: "listatema.php",
                                minLength: 3, 
                                open: function(event) {
                                valid = false;
                                },
                                close: function(event){
                                    if (!valid) $(this).val('');
                                },
                                select: function (event, ui) {
                                    valid = true;
                                        id_TI[counterTI] = ui.item.id;
                                }
                        });  
                }



                 function removeInputTI(){
                        if (counterTI > 1) {
                                $( "#input_tema_child"+counterTI ).remove();
                                id_TI[counterTI] = '';
                                counterTI--;
                        }else{alert("É necessário preencher ao menos um campo de interesse!");}
                }

                var counterCI = 1;
                var id_CI = [];
                function addInputCI(divName){
                        counterCI++;
                        var newdiv = document.createElement('div');
                        newdiv.id = "input_curso_interesse_child"+counterCI;
                        newdiv.innerHTML = " <br><input id='select_curso_interesse"+ counterCI +"' style='background-color: white;' type='text' class='form-control input-lg interesse_extra' name='select_curso_interesse[]'>";
                        
                       document.getElementById(divName).appendChild(newdiv);

                        $( "#select_curso_interesse"+counterCI+"").autocomplete({
                                source: "listacurso.php",
                                minLength: 3, 
                                open: function(event) {
                                valid = false;
                                },
                                close: function(event){
                                    if (!valid) $(this).val('');
                                },
                                select: function (event, ui) {
                                    valid = true;
                                        id_CI[counterCI] = ui.item.id;
                                }
                        });  
                }



                 function removeInputCI(){
                        if (counterCI > 1) {
                                $( "#input_curso_interesse_child"+counterCI ).remove();
                                id_CI[counterCI] = '';
                                counterCI--;
                        }else{alert("É necessário preencher ao menos um curso de interesse!");}
                }

                 var counterC1 = 1;
                var id_C1 = [];
                function addInputC1(divName){
                        counterC1++;
                        var newdiv = document.createElement('div');
                        newdiv.id = "input_curso1_child"+counterC1;
                        newdiv.innerHTML = " <br><input id='select_1curso"+ counterC1 +"' style='background-color: white;' type='text' class='form-control input-lg interesse_extra' name='select_1curso[]'>";
                        
                       document.getElementById(divName).appendChild(newdiv);

                        $( "#select_1curso"+counterC1+"").autocomplete({
                                source: "listacurso.php",
                                minLength: 3, 
                                open: function(event) {
                                valid = false;
                                },
                                close: function(event){
                                    if (!valid) $(this).val('');
                                },
                                select: function (event, ui) {
                                    valid = true;
                                        id_C1[counterC1] = ui.item.id;
                                }
                        });  
                }



                 function removeInputC1(){
                    if (counterC1 > 1) {
                            $( "#input_curso1_child"+counterC1 ).remove();
                            id_C1[counterC1] = '';
                            counterC1--;
                    }else{alert("É necessário preencher ao menos um campo de interesse!");}
                }
                 var counterC2 = 1;
                var id_C2 = [];
                function addInputC2(divName){
                        counterC2++;
                        var newdiv = document.createElement('div');
                        newdiv.id = "input_curso2_child"+counterC2;
                        newdiv.innerHTML = " <br><input id='select_2curso"+ counterC2 +"' style='background-color: white;' type='text' class='form-control input-lg interesse_extra' name='select_2curso[]'>";
                        
                       document.getElementById(divName).appendChild(newdiv);

                        $( "#select_2curso"+counterC2+"").autocomplete({
                                source: "listacurso.php",
                                minLength: 3, 
                                open: function(event) {
                                valid = false;
                                },
                                close: function(event){
                                    if (!valid) $(this).val('');
                                },
                                select: function (event, ui) {
                                    valid = true;
                                        id_C2[counterC2] = ui.item.id;
                                }
                        });  
                }



                function removeInputC2(){
                        if (counterC2 > 1) {
                                $( "#input_curso2_child"+counterC2 ).remove();
                                id_C2[counterC2] = '';
                                counterC2--;
                        }else{alert("É necessário preencher ao menos um campo de interesse!");}
                }

                var counterC1nf = 1;
                var id_C1nf = [];
                function addInputC1nf(divName){
                        counterC1nf++;
                        var newdiv = document.createElement('div');
                        newdiv.id = "input_curso1_child_nao_fez"+counterC1nf;
                        newdiv.innerHTML = " <br><input id='select_1curso_nao_fez"+ counterC1nf +"' style='background-color: white;' type='text' class='form-control input-lg interesse_extra' name='select_1curso_nao_fez[]'>";
                        
                       document.getElementById(divName).appendChild(newdiv);

                        $( "#select_1curso_nao_fez"+counterC1nf+"").autocomplete({
                                source: "listacurso.php",
                                minLength: 3, 
                                open: function(event) {
                                valid = false;
                                },
                                close: function(event){
                                    if (!valid) $(this).val('');
                                },
                                select: function (event, ui) {
                                    valid = true;
                                        id_C1nf[counterC1nf] = ui.item.id;
                                }
                        });  
                }



                 function removeInputC1nf(){
                    if (counterC1nf > 1) {
                            $( "#input_curso1_child_nao_fez"+counterC1nf ).remove();
                            id_C1nf[counterC1nf] = '';
                            counterC1nf--;
                    }else{alert("É necessário preencher ao menos um curso.");}
                }
                 var counterC2nf = 1;
                var id_C2nf = [];
                function addInputC2nf(divName){
                        counterC2nf++;
                        var newdiv = document.createElement('div');
                        newdiv.id = "input_curso2_child"+counterC2nf;
                        newdiv.innerHTML = " <br><input id='select_2curso_nao_fez"+ counterC2nf +"' style='background-color: white;' type='text' class='form-control input-lg interesse_extra' name='select_2curso_nao_fez[]'>";
                        
                       document.getElementById(divName).appendChild(newdiv);

                        $( "#select_2curso_nao_fez"+counterC2nf+"").autocomplete({
                                source: "listacurso.php",
                                minLength: 3, 
                                open: function(event) {
                                valid = false;
                                },
                                close: function(event){
                                    if (!valid) $(this).val('');
                                },
                                select: function (event, ui) {
                                    valid = true;
                                        id_C2nf[counterC2nf] = ui.item.id;
                                }
                        });  
                }



                function removeInputC2nf(){
                        if (counterC2nf > 1) {
                                $( "#input_curso2_child"+counterC2nf ).remove();
                                id_C2nf[counterC2nf] = '';
                                counterC2nf--;
                        }else{alert("É necessário preencher ao menos um curso!");}
                }

                


                

                function filtraTabela() {
                  // Declare variables
                  var input, filter, table, tr, td, i;
                  input = document.getElementById("filtroCursosFeitos");
                  filter = input.value.toUpperCase();
                  table = document.getElementById("tabela_cursos_feitos");
                  tr = table.getElementsByTagName("tr");

                  // Loop through all table rows, and hide those who don't match the search query
                  for (i = 0; i < tr.length; i++) {
                    td = tr[i].getElementsByTagName("td")[0];
                    if (td) {
                      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                      } else {
                        tr[i].style.display = "none";
                      }
                    }
                  }
                }

                function busca_turma(){
                    if(id_e[1] != "" ){
                         $("#select_evento1").val(id_e[1]); 
                         $.ajax({
                                url: 'busca_turma.php',
                                data: "select_evento="+id_e[1],
                                type: "post",
                                success: function(resusltado_busca_turma){
                                     $("#div_turma_lista").html(resusltado_busca_turma);
                                }
                        }); 
                    }
                }

                $(document).ready(function () {   
                    $("#export_data").click(function(){
                       // document.getElementById('export_data_hidden').value = "2";
                        $("#frm_resultado")[0].reportValidity();
                        if ($("#frm_resultado")[0].checkValidity()) {
                            for (i=1;i<counterTC+1;i++){
                                $("#select_todos_cursos"+i+"").val(id_tc[i]);   
                            } 
                            for (i=1;i<counterNC+1;i++){
                                $("#select_nenhum_curso"+i+"").val(id_nc[i]); 
                            } 
                            for (i=1;i<counterAC+1;i++){
                                $("#select_algum_curso"+i+"").val(id_ac[i]); 
                            } 
                            for (i=1;i<counterTI+1;i++){
                                $("#select_tema_interesse"+i+"").val(id_TI[i]); 
                            } 
                            for (i=1;i<counterCI+1;i++){
                                $("#select_curso_interesse"+i+"").val(id_CI[i]); 
                            } 
                            for (i=1;i<counterC1+1;i++){
                                $("#select_1curso"+i+"").val(id_C1[i]); 
                            } 
                            for (i=1;i<counterC2+1;i++){
                                $("#select_2curso"+i+"").val(id_C2[i]); 
                            } 
                            for (i=1;i<counterC1nf+1;i++){
                                $("#select_1curso_nao_fez"+i+"").val(id_C1nf[i]); 
                            } 
                            for (i=1;i<counterC2nf+1;i++){
                                $("#select_2curso_nao_fez"+i+"").val(id_C2nf[i]); 
                            } 
                            $("#select_evento1").val(id_e[1]); 
                           /* $.ajax({
                                url: 'resultadoGeraLista.php/',
                                type: 'post',
                                data: $("#frm_resultado").serialize(),
                                success: function(resultado_filtro){
                                    alert("Lista gerada com sucesso!");
                                    $("#frm_resultado")[0].reset();
                                    //location.href = "geralistas.php";
                                
                                }

                            });*/
                            document.frm_resultado.submit();
                        }
                        return false;
                    });


                    $("#resultados").click(function () {                            
                        $("#frm_resultado")[0].reportValidity();
                    if ($("#frm_resultado")[0].checkValidity()) {
                        for (i=1;i<counterTC+1;i++){
                            $("#select_todos_cursos"+i+"").val(id_tc[i]); 
                        } 
                        for (i=1;i<counterNC+1;i++){
                            $("#select_nenhum_curso"+i+"").val(id_nc[i]); 
                        } 
                        for (i=1;i<counterAC+1;i++){
                            $("#select_algum_curso"+i+"").val(id_ac[i]); 
                        } 
                        for (i=1;i<counterTI+1;i++){
                            $("#select_tema_interesse"+i+"").val(id_TI[i]); 
                        } 
                        for (i=1;i<counterCI+1;i++){
                            $("#select_curso_interesse"+i+"").val(id_CI[i]); 
                        } 
                        for (i=1;i<counterC1+1;i++){
                            $("#select_1curso"+i+"").val(id_C1[i]); 
                        } 
                        for (i=1;i<counterC2+1;i++){
                            $("#select_2curso"+i+"").val(id_C2[i]); 
                        } 
                        for (i=1;i<counterC1nf+1;i++){
                            $("#select_1curso_nao_fez"+i+"").val(id_C1nf[i]); 
                        } 
                        for (i=1;i<counterC2nf+1;i++){
                            $("#select_2curso_nao_fez"+i+"").val(id_C2nf[i]); 
                        } 
                        $("#select_evento1").val(id_e[1]); 
                        $.ajax({
                            url: 'resultadoGeraLista.php/',
                            type: 'post',
                            data: $("#frm_resultado").serialize()+"&resultados=1",
                            success: function(resultado_filtro){
                                alert("Lista gerada com sucesso!");
                                $("#frm_resultado")[0].reset();
                                //location.href = "geralistas.php";
                            
                            }

                        });
                    }
                        return false;
                    });   

                    $("#datacurso1").datepicker({
                            dateFormat: 'dd/mm/yy',
                            dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                            dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                            dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                            monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                            monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                            nextText: 'Próximo',
                            prevText: 'Anterior'
                    }); 
                    $("#datacurso2").datepicker({
                            dateFormat: 'dd/mm/yy',
                            dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                            dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                            dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                            monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                            monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                            nextText: 'Próximo',
                            prevText: 'Anterior'
                    });   
                    $("#datacurso1_nao_fez").datepicker({
                            dateFormat: 'dd/mm/yy',
                            dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                            dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                            dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                            monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                            monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                            nextText: 'Próximo',
                            prevText: 'Anterior'
                    }); 
                    $("#datacurso2_nao_fez").datepicker({
                            dateFormat: 'dd/mm/yy',
                            dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                            dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                            dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                            monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                            monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                            nextText: 'Próximo',
                            prevText: 'Anterior'
                    });   
                    $("#dataultimavisita").datepicker({
                            dateFormat: 'dd/mm/yy',
                            dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                            dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                            dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                            monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                            monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                            nextText: 'Próximo',
                            prevText: 'Anterior'
                    }); 
                    $( "#select_todos_cursos1" ).autocomplete({
                            source: "listacurso.php",
                            minLength: 3, 
                            open: function(event) {
                            valid = false;
                            },
                            close: function(event){
                                if (!valid) $(this).val('');
                            },
                            select: function (event, ui) {
                                valid = true;
                                    id_tc[1] = ui.item.id; 
                            }
                    });   
                    $( "#select_nenhum_curso1" ).autocomplete({
                            source: "listacurso.php",
                            minLength: 3, 
                            open: function(event) {
                            valid = false;
                            },
                            close: function(event){
                                if (!valid) $(this).val('');
                            },
                            select: function (event, ui) {
                                valid = true;
                                    id_nc[1] = ui.item.id; 
                            }
                    });   
                    $( "#select_algum_curso1" ).autocomplete({
                            source: "listacurso.php",
                            minLength: 3, 
                            open: function(event) {
                            valid = false;
                            },
                            close: function(event){
                                if (!valid) $(this).val('');
                            },
                            select: function (event, ui) {
                                valid = true;
                                    id_ac[1] = ui.item.id; 
                            }
                    });  
                    $( "#select_evento1" ).autocomplete({
                            source: "listacurso.php",
                            minLength: 3, 
                            open: function(event) {
                            valid = false;
                            },
                            close: function(event){
                                if (!valid) $(this).val('');
                            },
                            select: function (event, ui) {
                                valid = true;
                                    id_e[1] = ui.item.id; 
                            }
                    });  
                    $( "#select_tema_interesse1" ).autocomplete({
                            source: "listatema.php",
                            minLength: 3, 
                            open: function(event) {
                            valid = false;
                            },
                            close: function(event){
                                if (!valid) $(this).val('');
                            },
                            select: function (event, ui) {
                                valid = true;
                                    id_TI[1] = ui.item.id; 
                            }
                    });
                    $( "#select_curso_interesse1" ).autocomplete({
                            source: "listacurso.php",
                            minLength: 3, 
                            open: function(event) {
                            valid = false;
                            },
                            close: function(event){
                                if (!valid) $(this).val('');
                            },
                            select: function (event, ui) {
                                valid = true;
                                    id_CI[1] = ui.item.id; 
                            }
                    });   
                    $( "#select_1curso1" ).autocomplete({
                            source: "listacurso.php",
                            minLength: 3, 
                            open: function(event) {
                            valid = false;
                            },
                            close: function(event){
                                if (!valid) $(this).val('');
                            },
                            select: function (event, ui) {
                                valid = true;
                                    id_C1[1] = ui.item.id; 
                            }
                    });   
                    $( "#select_2curso1" ).autocomplete({
                            source: "listacurso.php",
                            minLength: 3, 
                            open: function(event) {
                            valid = false;
                            },
                            close: function(event){
                                if (!valid) $(this).val('');
                            },
                            select: function (event, ui) {
                                valid = true;
                                    id_C2[1] = ui.item.id; 
                            }
                    }); 

 
 
                        
                    });
        </script>
</HEAD>

<BODY class="bg-light">
        <?php
            include "navbar_adm.php";
        ?> 
        <div class="container" style="margin-top: 15px;">
            <input type="hidden" id="aluno_codigo"/>
            <div class="text-center" id="div_titulo">       
                <img class="d-block mx-auto mb-4" src="Images/listas.png" alt="" width="72" height="72">          
                <h2>Gerar Listas de TMK</h2><br/>
                <p class="lead" style="font-size: 16">TODOS os filtros são OPCIONAIS. Marque APENAS os filtros NECESSÁRIOS, </p> 
                <p class="lead" style="font-size: 16">pois quanto mais filtros você coloca, mais você  restringe os alunos da lista.</p>                                   
            </div>                                   
            
            <div class="row">
                <div class="col-md-12">                            
                    <form id="frm_resultado" name="frm_resultado" target="_blank" action="resultadoGeraLista.php" method="post">                        
                        <hr class="mb-4">

                        <div class="card">
                            <div class="card-header" style="background-color: #f0ad4e"><b>1. Sobre o ALUNO</b></div>
                        <div class="row card-body" style="background-color: #fffcd6">

                            <div class="col-md-2 mb-3">
                                <label for="voluntario">É Voluntário?</label>
                                <select class="custom-select d-block w-100" id="voluntario" name="voluntario" >
                                  <option value=""> </option>
                                  <option value="1">Sim</option>
                                  <option value="2" selected="selected">Não</option>
                                  <option value="3">Ambos</option>
                                </select>                            
                            </div>

                            <div class="col-md-2 mb-3">
                                <label for="metropolitana">R. Metropolitana?</label>
                                <select class="custom-select d-block w-100" id="metropolitana" name="metropolitana" >
                                  <option value=""> </option>
                                  <option value="1" selected="selected">Sim</option>
                                  <option value="2">Não</option>
                                  <option value="2">Ambos</option>                        
                                </select>                              
                            </div>
                            <div class="col-md-2 md-3">
                                <label for="dataultimavisita">Última vez no IIPC</label>
                                <input class="custom-select d-block w-100" id="dataultimavisita" style="background-color: white;"  type="text" name="dataultimavisita"/>               
                            </div>
                            <div class="col-md-2 mb-3">
                                <label for="idademinima">Idade mínima</label>
                                <input class="form-radius" style="background-color: white;height: 37px" type="text" name="idademinima" id="idademinima">                           
                            </div>
                            <div class="col-md-2 mb-3">
                                <label for="idademaxima">Idade máxima</label>
                                <input class="form-radius" style="background-color: white;height: 37px" type="text"  name="idademaxima" id="idademaxima">                           
                            </div>
                        </div>  
                        </div>

                        
                        <hr class="mb-4">

                        <div class="card">
                            <div class="card-header" style="background-color: #5bc0de"><b>2. Sobre os CURSOS</b></div>
                            <div class="card-body" style="background-color: #eff2f7">
                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="curso_entrada">Possui curso de entrada</label>
                                <select class="custom-select d-block w-100" id="curso_entrada" name="curso_entrada" >
                                  <option value=""> </option>
                                  <option value="1">Sim</option>
                                  <option value="2">Não</option>
                                  <option value="3" selected="selected">Tanto faz</option>
                                </select>
                           
                            </div>
                            <div class="custom-control custom-checkbox row" style="margin-top: 30px;margin-left: 30px">
                                <input type="checkbox" class="custom-control-input" id="check_cap" name="check_cap">
                                <label class="custom-control-label" style="margin-top: 3px" for="check_cap">Já fez <b>QUALQUER</b> CAP</label>
                            </div>
                            <div class="custom-control custom-checkbox" style="margin-top: 30px; margin-left: 45px">
                                <input type="checkbox" class="custom-control-input" id="check_cl" name="check_cl">
                                <label class="custom-control-label" style="margin-top: 3px" for="check_cl">Já fez <b>QUALQUER</b> CL</label>
                            </div>
                        </div>  
                        <div class="row" style="margin-top: 20px;">
                        
                        <div class="col-md-4 mb-2">
                            <label for="num_eventos">Esteve presente em no mínimo quantos eventos?</label>
                        </div>
                        
                        <div class="col-md-1 mb-2">
                            <input type="number" style="background-color: white; width: 80px; margin-left:-60px" class="form-control" id="num_eventos" placeholder="" value="" name="num_eventos">                                  
                        </div>
                        </div>

                         <hr class="mb-4">
                             <br/>


                        
                        <div class="row">
                            <div class="col-md-4">

                                <h6 for="select_todos_cursos1">Fez <b>TODOS</b> os cursos abaixo:</h6>
                                <div>
                                        <input type="text" style="background-color: white;" class="form-control input-lg" id="select_todos_cursos1" name="select_todos_cursos[]">
                                </div>                                                            
                                <div id="input_todos_cursos"></div>
                                <input style="color: #272828; background-color: #eaeef2; margin-top: 10px;" type="button" value="Adicionar" onClick="addInputTC('input_todos_cursos');">
                                <input style="color: #272828; background-color: #eaeef2; margin-top: 10px;" type="button" value="Remover" onClick="removeInputTC()">
                            </div>
                            <div class="col-md-4">
                                <h6 for="select_algum_curso1">Fez <b>QUALQUER UM</b> dos cursos abaixo:</h6>
                                <div>
                                        <input type="text" style="background-color: white;" class="form-control input-lg" id="select_algum_curso1" name="select_algum_curso[]">
                                </div>                                                            
                                <div id="input_algum_curso"></div>
                                <input style="color: #272828; background-color: #eaeef2; margin-top: 10px;" type="button" value="Adicionar" onClick="addInputAC('input_algum_curso');">
                                <input style="color: #272828; background-color: #eaeef2; margin-top: 10px;" type="button" value="Remover" onClick="removeInputAC()">
                            </div>
                            <div class="col-md-4">
                                <h6 for="select_nenhum_curso1">Não fez <b>NENHUM</b> dos cursos abaixo:</h6>
                                <div>
                                        <input type="text" style="background-color: white;" class="form-control input-lg" id="select_nenhum_curso1" name="select_nenhum_curso[]">
                                </div>                                                            
                                <div id="input_nenhum_curso"></div>
                                <input style="color: #272828; background-color: #eaeef2; margin-top: 10px;" type="button" value="Adicionar" onClick="addInputNC('input_nenhum_curso');">
                                <input style="color: #272828; background-color: #eaeef2; margin-top: 10px;" type="button" value="Remover" onClick="removeInputNC()">
                            </div>
                            
                        </div>

                             <br/>
                         <hr class="mb-4">
                             <br/>
                        <h6 style="margin-bottom: 25px">Fez <b>TODOS</b> os cursos abaixo após a data x (Ex: Fez ECP2 após 01/01/2015)</h6>

                        <div class="row">
                             <div class="col-md-4  mb-3">
                                <h6 for="select_1curso1">Curso</h6>
                                <div>
                                    <input type="text" style="background-color: white;" class="form-control input-lg" id="select_1curso1" name="select_1curso[]">
                                </div>                                                            
                                <div id="input_curso1"></div>
                            </div>

                            <div class="col-md-2 mb-3">
                                <label for="datacurso1">Data máxima</label>
                                <input id="datacurso1"  style="background-color: white;" type="text" class="custom-select d-block w-100"  name="datacurso1" value="" class="form-radius"/>
                            </div>
                        </div> 

                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <h6 for="select_2curso1">Curso</h6>
                                <div>
                                    <input type="text" style="background-color: white;" class="form-control input-lg" id="select_2curso1" name="select_2curso[]">
                                </div>                                                            
                                <div id="input_curso2"></div>

                            </div>

                            <div class="col-md-2 mb-3">
                                <label for="datacurso2">Data máxima</label>
                                <input id="datacurso2"  style="background-color: white;" type="text" class="custom-select d-block w-100"  name="datacurso2" value="" class="form-radius"/>
                            </div>
                        </div>

                                                    <br/>
                         <hr class="mb-4">
                             <br/>
                        <h6 style="margin-bottom: 25px"><b>NÃO</b> fez <b>NENHUM</b> dos cursos abaixo após a data x (Ex: Não fez CIP após 01/01/2015)</h6>

                        <div class="row">
                             <div class="col-md-4  mb-3">
                                <h6 for="select_1curso1_nao_fez">Curso</h6>
                                <div>
                                    <input type="text" style="background-color: white;" class="form-control input-lg" id="select_1curso1_nao_fez" name="select_1curso_nao_fez[]">
                                </div>                                                            
                                <div id="input_curso1_nao_fez"></div>
                            </div>

                            <div class="col-md-2 mb-3">
                                <label for="datacurso1_nao_fez">Data máxima</label>
                                <input id="datacurso1_nao_fez"  style="background-color: white;" type="text" class="custom-select d-block w-100"  name="datacurso1_nao_fez" value="" class="form-radius"/>
                            </div>
                        </div> 

                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <h6 for="select_2curso1_nao_fez">Curso</h6>
                                <div>
                                    <input type="text" style="background-color: white;" class="form-control input-lg" id="select_2curso1_nao_fez" name="select_2curso[]_nao_fez">
                                </div>                                                            
                                <div id="input_curso2_nao_fez"></div>

                            </div>

                            <div class="col-md-2 mb-3">
                                <label for="datacurso2_nao_fez">Data máxima</label>
                                <input id="datacurso2_nao_fez"  style="background-color: white;" type="text" class="custom-select d-block w-100"  name="datacurso2_nao_fez" value="" class="form-radius"/>
                            </div>
                        </div>
                        </div>
                        </div>


                        <hr class="mb-4">

                    <div class="card">
                            <div class="card-header" style="background-color: #5cb85c;color: #474646"><b>3. Sobre os INTERESSES</b></div>
                            
                        <div class="row card-body" style="background-color: #f2ffef">
                             <div class="col-md-5">
                                <h6 for="select_curso_interesse1">Tem interesse em <b>QUALQUER UM</b> dos <b>CURSOS</b>:</h6>
                                <div>
                                    <input type="text" style="background-color: white;" class="form-control input-lg" id="select_curso_interesse1" name="select_curso_interesse[]">
                                </div>                                                            
                                <div id="input_curso_interesse"></div>
                                <input style="color: #272828; background-color: #eaeef2; margin-top: 10px;" type="button" value="Adicionar" onClick="addInputCI('input_curso_interesse');">
                                <input style="color: #272828; background-color: #eaeef2; margin-top: 10px;" type="button" value="Remover" onClick="removeInputCI()">
                            </div>
                            <div class="col-md-5">
                                <h6 for="select_tema_interesse1">Tem interesse em <b>QUALQUER UM</b> dos <b>TEMAS</b>:</h6>
                                <div>
                                    <input type="text" style="background-color: white;" class="form-control input-lg" id="select_tema_interesse1" name="select_tema_interesse[]">
                                </div>                                                            
                                <div id="input_tema"></div>
                                <input style="color: #272828; background-color: #eaeef2; margin-top: 10px;" type="button" value="Adicionar" onClick="addInputTI('input_tema');">
                                <input style="color: #272828; background-color: #eaeef2; margin-top: 10px;" type="button" value="Remover" onClick="removeInputTI()">
                            </div>
                            <div class="custom-control custom-checkbox" style="margin-top: 35px;margin-left: 13px">                                
                                <input type="checkbox" class="custom-control-input" id="check_muito_interesse" name="check_muito_interesse">
                                <label class="custom-control-label" style="margin-top: 4px" for="check_muito_interesse">Demonstrou muito interesse pela conscienciologia</label>
                             </div>
                        </div>
                    </div>

                        <hr class="mb-4">

                        <div class="card">
                            <div class="card-header" style="background-color: #d9534f;color: #474646"><b>4. Sobre a LISTA</b></div>
                        <div class="card-body" style="background-color: #fcebea">
                            <div class="row">
                                <div class="col-md-3 mb-3">
                                    <label class="required" for="nome_lista">Nome da Lista:</label>
                                    <input type="text" style="background-color: white;" class="form-control" id="nome_lista" required="" placeholder="" value="" name="nome_lista">       
                                 </div>
                            </div>
                            <div class="row">
                               <div class="col-md-4 mb-3">
                                    <h6 class="required" for="select_evento1">Essa lista é para qual evento?</h6>
                                    <div>
                                        <input type="text" style="background-color: white;" class="form-control input-lg" id="select_evento1" name="select_evento[]" onblur="busca_turma();" required="">
                                    </div>                                                            
                                    
                                </div>
                                <div class="col-md-3 mb-3" id="div_turma_lista"> </div>
                            </div>

                            <div class="row">
                               <div class="col-md-4 mb-3">
                                    <h6 class="required" for="descricao">Descrição da Lista</h6>
                                    <div>
                                        <textarea type="text" class="form-control input-lg"  id="descricao" name="descricao" required="" rows="5" style="overflow:hidden"></textarea>
                                    </div>                                                            
                                </div>
                            </div>
                        
                        
                        <hr class="mb-4">

                        <div class="row" style="margin-top:10px; margin-left: 5px">
                            <button class="btn btn-primary btn-lg" id="resultados" name="resultados" value="1" style="margin-right: 30px">Gerar Lista</button>                        

                            <button class="btn btn-primary btn-lg" id="export_data" value="1" name='export_data'>Exportar Emails</button>
                        </div>

                        </div> 
                        </div>
                         <hr class="mb-4">
                         <div style="margin-left: 30px">
                        <h6>Filtro livre - USO EXCLUSIVO DA EQUIPE DE TI</h6>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="addcondicao">Condição AND:</label>
                                <textarea type="text" style="background-color: white;" class="form-control" id="addcondicao" placeholder="" value="" name="addcondicao">  </textarea>     
                             </div>
                        </div>
                        </div>
                        <hr class="mb-4">

                    </form>    
                                    
            </div>
        </div>
        <script src="popper.min.js" crossorigin="anonymous"></script>
        <script src="bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>

      <?php include "rodape.php"; ?>
</BODY>

</HTML>