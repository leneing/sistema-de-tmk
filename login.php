<?php

include "conectasql.php";
session_start();

$cursos_filtro = "SELECT DISTINCT e.* FROM `evento` e,turma t where  DATE(t.DATA_INICIAL) > CURDATE() and e.id = t.ID_EVENTO";
$voluntarios_filtro = "SELECT DISTINCT v.* FROM voluntario v";
$res_curso = $conexao ->query($cursos_filtro);
$res_voluntario = $conexao -> query($voluntarios_filtro);
$cursos_interesse = "SELECT DISTINCT e.* FROM `evento_ativo` e where e.ativo = 1";
$res_curso_ativo = $conexao ->query($cursos_interesse);

?>

<HTML>

<HEAD>
        <TITLE>IIPC BH - TMK</TITLE>
        <link rel="stylesheet" href="bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="jquery-ui.css">
        <script src="jquery-3.3.1.min.js" crossorigin="anonymous"></script>
        <script src="jquery-ui.js"></script>
        <link href="gijgo.min.css" rel="stylesheet" type="text/css"/>
        <script src="gijgo.min.js" type="text/javascript"></script>
        <script src="combobox.js"></script>

        <style type="text/css">
            .form-center{   
                width: 20%;
                padding: .375rem .75rem;
                font-size: 1rem;
                line-height: 1.5;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                border-radius: .25rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            }
        </style>

        <script>
                window.onbeforeunload = function (evt) {
                    encerrasessao();
                } 

                $(document).ready(function () {
                    $("#btnLogin").click(function(){
                         $.ajax({
                            url: "http://iipcnet.iipc.org/sources/logonNew.asp",
                            data:"TxtUserId="+$('#userName').val()+"&TxtUserPsw="+$('#userPassword').val(),
                            crossDomain: true,
                            type: 'post',
                            dataType: 'jsonp',
                            success:function (resposta){
                                if (resposta == "") {
                                    location.href="index.php";
                                    return false;
                                }else{
                                    alert("Usuário ou senha incorretos.");
                                    return false;
                                }
                            },
                            error: function(erro){
                                alert("Ocorreu um erro ao logar no IIPCNET. Logue no IIPCNET e acesse essa página novamente.");
                            }
                        });
                         return false;
                    });
                });

        </script>
</HEAD>

<BODY class="bg-light">

     <?php
            include "navbar.php";
     ?> 
        <div class="container" style="margin-top: 15px;">
            <input type="hidden" id="aluno_codigo"/>
            <div class="row">
                    <div class="col-md-12" style="margin-top: 30px">
                            <form id="frm_filtro" name="frm_filtro" action="efetua_login_iipcnet.php">
                                    <div id="logado" style="width: 100%; text-align: center">
                                        <h3>Sistema de TMK</h3>
                                        <p>Digite seu usuário e senha do IIPCNET para fazer o Login.</p>
                                        <div class="form-login" style="text-align: center; width: 100%; margin-top: 20px">
                                            <input type="text" id="userName" class="form-center input-sm chat-input" placeholder="usuário" />
                                            </br>
                                            <input type="text" id="userPassword" class=" input-sm chat-input form-center" placeholder="senha" style="margin-top: 5px" />
                                            </br>
                                            <div class="wrapper">
                                                <input type="button" id="btnLogin"  class="btn btn-primary btn-md" value="Login" btn-primary btn-md" style="margin-top: 12px; width: 20%"/>
                                            </div>
                                        </div>                                     
                                    </div>
                                    
                            </form>
                             
                    </div>                    
            </div>
                         
        </div>    

      <?php include "rodape.php"; ?>
          
        <script src="popper.min.js" crossorigin="anonymous"></script>
        <script src="bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>
</BODY>

</HTML>