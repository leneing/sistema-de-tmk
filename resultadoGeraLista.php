<?php
include "conectasql.php";
include "Selects/queries.php";
session_start();


$tipo_parametros = "";
$parametros = "";

$sql_query_select = "select * from (
        							SELECT DISTINCT a.codigo ";
$sql_query_from = " FROM   (SELECT a.* 
				                FROM   aluno a ";

$sql_query_where = " WHERE UF = 'MG' ";

$sql_query_fechamento = " ) a ) alunos ";
/*
var_dump($_POST);
var_dump($_POST['voluntario']);
$true =  $_POST['select_e'][0];// isset($_POST['select_nenhum_curso']);
var_dump($true);
var_dump($_POST['datacurso2']);
*/
	if($_POST["metropolitana"] == 1){
		$sql_query_where .= utf8_decode($and_cidades_regiao_metropolitana);
	}
	if ($_POST["voluntario"] == 1) {
		$sql_query_where .= $and_voluntario;
	}else if ($_POST["voluntario"] == 2) {
		$sql_query_where .= $and_nao_voluntario;
	}
	if($_POST["dataultimavisita"] != ""){
		$sql_query_where .= $and_ultima_visita_iipc;
		$tipo_parametros .= "s";
			//$parametros .= date('Y-m-d H:i:s', strtotime($_POST['dataultimavisita']));
			$parametros .= date('Y-m-d H:i:s', strtotime($_POST['dataultimavisita'])) . ";";
	}
	if($_POST["idademinima"] != ""){
		$sql_query_where .= $and_idade_minima;
		$tipo_parametros .= "s";
		$parametros .= $_POST['idademinima'] . ";";
	}
	if($_POST["idademaxima"] != ""){
		$sql_query_where .= $and_idade_maxima;
		$tipo_parametros .= "s";
		$parametros .= $_POST['idademaxima'] . ";";
	}
	if($_POST["num_eventos"] != ""){
		$sql_query_where .= $and_veio_no_minimo_x_eventos;
		$tipo_parametros .= "s";
		$parametros .= $_POST['num_eventos'] . ";";
	}
	if ($_POST["curso_entrada"] == 1) {
		$sql_query_where .= $and_possui_curso_entrada;
	}else if ($_POST["curso_entrada"] == 2) {
		$sql_query_where .= $and_nao_possui_curso_entrada;
	}
	if (isset($_POST['check_cap'])) {
		$sql_query_where .= $and_ja_fez_cap;
	}
	if (isset($_POST['check_cl'])) {
		$sql_query_where .= $and_ja_fez_cl;
	}
	if (isset($_POST['check_muito_interesse'])) {
		$sql_query_where .= $and_demonstrou_muito_interesse;
	}
	if(!($_POST['select_1curso'][0] == "")){
		$sql_query_where .= $and_fez_curso_x_ha_n_tempo;
		$tipo_parametros .= "ss";
		
		$parametros .= $_POST['select_1curso'][0] . ";" . date('Y-m-d H:i:s', strtotime($_POST['datacurso1'])) . ";";
	}
	if(!($_POST['select_2curso'][0] == "")){
		$sql_query_where .= $and_fez_curso_x_ha_n_tempo;
		$tipo_parametros .= "ss";

		$parametros .= $_POST['select_2curso'][0] . ";" . date('Y-m-d H:i:s', strtotime($_POST['datacurso2'])) . ";";
	}
	if(!($_POST['select_1curso_nao_fez'][0] == "")){
		$sql_query_where .= $and_fez_curso_x_ha_n_tempo;
		$tipo_parametros .= "ss";
		
		$parametros .= $_POST['select_1curso_nao_fez'][0] . ";" . date('Y-m-d H:i:s', strtotime($_POST['datacurso1_nao_fez'])) . ";";
	}
	if(!($_POST['select_2curso_nao_fez'][0] == "")){
		$sql_query_where .= $and_fez_curso_x_ha_n_tempo;
		$tipo_parametros .= "ss";

		$parametros .= $_POST['select_2curso_nao_fez'][0] . ";" . date('Y-m-d H:i:s', strtotime($_POST['datacurso2_nao_fez'])) . ";";
	}

	if(!($_POST['select_todos_cursos'][0] == "")){
		$str_cursos_feitos = "";
		foreach ($_POST['select_todos_cursos'] as $selectedOption){
			$sql_query_where .= $and_fez_o_curso_x;
			$tipo_parametros .= "s";
	  		$parametros .= $selectedOption . ";";   
	   } 	   
	}

	if(!($_POST['select_nenhum_curso'][0] == "")){
		$str_cursos_nao_feitos = "";
		foreach ($_POST['select_nenhum_curso'] as $selectedOption){					
			$str_cursos_nao_feitos .= $selectedOption . ",";	   
	   }
	   $sql_query_where .= $and_nao_fez_nenhum_curso_x;
	   $tipo_parametros .= "s";
	   $str_cursos_nao_feitos = rtrim($str_cursos_nao_feitos, ',');
	   $str_cursos_nao_feitos .= ";";
	  
	   $parametros .= $str_cursos_nao_feitos;
	}

	if(!($_POST['select_algum_curso'][0] == "")){
		$str_qualquer_curso_feito = "";
		foreach ($_POST['select_algum_curso'] as $selectedOption){				
			$str_qualquer_curso_feito .= $selectedOption . ",";	 
	   }
	   $sql_query_where .= $and_fez_algum_curso_x;
	   $tipo_parametros .= "s";
	   $str_qualquer_curso_feito = rtrim($str_qualquer_curso_feito, ',');
	   $str_qualquer_curso_feito .= ";";	  
	   $parametros .= $str_qualquer_curso_feito;
	}
	if(!($_POST['select_tema_interesse'][0] == "")){
		$str_interesse_algum_tema = "";
		foreach ($_POST['select_tema_interesse'] as $selectedOption){					
			$str_interesse_algum_tema .= $selectedOption . ",";	   
	   }
	   $sql_query_where .= $and_tem_interesse_algum_tema;
	   $tipo_parametros .= "s";	  
	   $str_interesse_algum_tema = rtrim($str_interesse_algum_tema, ',');
	   $str_interesse_algum_tema .= ";";
	   $parametros .= $str_interesse_algum_tema;
	}

	if(!($_POST['select_curso_interesse'][0] == "")){
		$str_interesse_algum_curso = "";
		foreach ($_POST['select_curso_interesse'] as $selectedOption){					
			$str_interesse_algum_curso .= $selectedOption . ",";	   
	   }
	   $sql_query_where .= $and_tem_interesse_algum_curso;
	   $tipo_parametros .= "s";	  
	   $str_interesse_algum_curso = rtrim($str_interesse_algum_curso, ',');
	   $str_interesse_algum_curso .= ";";
	   $parametros .= $str_interesse_algum_curso;
	}

	$sql_query_where .= $and_restricoes_padrao_nao_ligar;

	if($_POST["addcondicao"] != ""){
		$sql_query_where .= utf8_decode($_POST["addcondicao"]);
	}

	while(substr($parametros, -1) == ";" ){
		$parametros = rtrim($parametros, ';');
	}


//$true = isset($_POST['export_data']);
//var_dump($true);

if(isset($_POST['resultados'])){

	$sql_query_completa = $sql_query_select . $sql_query_from . $sql_query_where . $sql_query_fechamento;

	$evento = $_POST['select_evento'][0];
	$nome_lista = utf8_decode($_POST["nome_lista"]);

	//var_dump($_POST{'select_evento'});
		
	$sql = $conexao->prepare("INSERT INTO select_lista (nome_lista, conteudo_select, tipo_parametros, parametros, data_criacao, id_evento, id_turma, fl_ativo, descricao) VALUES (?, ?, ?, ?, now(), ?, ?, 1, ?)");  

	$sql ->bind_param("ssssiss",$nome_lista,$sql_query_completa,$tipo_parametros,$parametros, $evento, $_POST['turma_lista'],$_POST['descricao']); 
	//var_dump($sql);
	$res = $sql->execute(); 

	echo mysqli_error($conexao);
	$sql->close();

//echo $sql_query_completa;
//echo $tipo_parametros;
//echo $parametros;
//var_dump($_POST['export_data']);
//f(isset($_POST['export_data'])){
}else{
	$sql_query_select_export = "select * from (
		SELECT DISTINCT a.nome, a.email";
	$sql_query_where .= "AND a.email <> ''";

	$sql_query_export =  $sql_query_select_export . $sql_query_from . $sql_query_where . $sql_query_fechamento;

	// se for um select com parametros, executa assim  
	if($parametros != ""){  
	//	$sql_query_completa = html_entity_decode($sql, ENT_QUOTES);
		$select = $conexao ->prepare($sql_query_export);
		$parametros = explode(";",$parametros);

		if ($tipo_parametros != "") {
		    mysqli_stmt_bind_param($select, $tipo_parametros,...$parametros);
		}

		$select -> execute();
		$select_resultado = $select -> get_result();

	}else{
		$select_resultado = $conexao ->query($sql_query_export);
	}
	//$res_select = $select_resultado -> fetch_assoc();

	while( $rows = mysqli_fetch_assoc($select_resultado) ) {
		$table_records[] = $rows;
	}	
	//if(isset($_POST["export_data"])) {	
		$filename = "emails.xls";			

		header('Content-Type: application/xlsx');
		header('Content-Disposition: attachment;filename="'+ $nome_lista +'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
 
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		
		$show_coloumn = false;
		if(!empty($table_records)) {
		  foreach($table_records as $record) {
			if(!$show_coloumn) {
			  // display field/column names in first row
			  echo implode("\t", array_keys($record)) . "\n";
			  $show_coloumn = true;
			}
			echo implode("\t", array_values($record)) . "\n";
		  }
		}
		exit;  
	//}
	//}
}	 