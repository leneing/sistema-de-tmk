<?php

include "conectasql.php";
session_start();

$cod_voluntario = $_POST['voluntario_selecionado'];
$lista_selecionada = $_POST["lista_selecionada"];

$insert = $conexao->prepare("INSERT INTO sessaotmk values (null, (?), NOW(), null, (?))");
$insert -> bind_param("si",$_POST["voluntario_selecionado"],$lista_selecionada);
$insert -> execute();
$id_sessao = mysqli_insert_id($conexao);
mysqli_commit($conexao);
if(is_numeric($id_sessao) && $id_sessao > 0){
	echo 1; // Deu certo
}

$_SESSION['ID_SESSAO'] = $id_sessao;
if($cod_voluntario != ""){
	$_SESSION['ID_VOLUNTARIO'] = $cod_voluntario;
}
?>