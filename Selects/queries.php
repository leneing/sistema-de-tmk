<?php


$and_nao_registro_parar_ligar_tb_ultima_lig = " AND NOT EXISTS (SELECT 1
						                           FROM ultima_ligacao u
						                           WHERE u.aluno_CODIGO = a.CODIGO
						                           AND (u.data > DATE_SUB(curdate(), INTERVAL 3 WEEK)
						                           OR u.data > curdate())) ";

$and_nao_pediu_parar_de_ligar = " AND NOT EXISTS (SELECT 1
						                           FROM dados_aluno d
						                           WHERE d.aluno_CODIGO = a.CODIGO
						                           AND d.fl_parar_de_ligar = 1)  ";			


$and_nao_possui_ligacao_recente = " AND NOT EXISTS (SELECT 1
						                           FROM ligacoes_tmk l
						                           WHERE l.aluno_CODIGO = a.CODIGO
						                           AND (l.data > DATE_SUB(curdate(), INTERVAL 3 WEEK))
						                           AND l.tipo_resultado <> 7) ";

$and_so_recupera_alunos_pulados_no_dia_seguinte = " AND NOT EXISTS (SELECT 1
										                           FROM ligacoes_tmk l
										                           WHERE l.aluno_CODIGO = a.CODIGO
										                           AND (l.data > DATE_SUB(curdate(), INTERVAL 1 DAY))
										                           AND l.tipo_resultado = 7) ";


// ====================================================================================================


$and_restricoes_padrao_nao_ligar = $and_nao_registro_parar_ligar_tb_ultima_lig . $and_nao_pediu_parar_de_ligar . $and_nao_possui_ligacao_recente . $and_so_recupera_alunos_pulados_no_dia_seguinte; 


$and_cidades_regiao_metropolitana_resumido = " AND CIDADE IN ('BELO HORIZONTE','CONTAGEM','NOVA LIMA', 'BETIM','SANTA LUZIA', 'SETE LAGOAS','BELO 													HORIZNTE','DIVINOPOLIS') ";

$and_cidades_regiao_metropolitana = " AND CIDADE in ('Belo Horizonte', 'Betim', 'Caeté', 'Contagem', 'Ibirité', 'Lagoa Santa', 'Nova Lima', 'Pedro Leopoldo', 'Raposos', 'Ribeirão das Neves', 'Rio Acima', 'Sabará', 'Santa Luzia', 'Vespasiano', 'Brumadinho', 'Esmeraldas', 'Igarapé', 'Mateus Leme', 'Juatuba', 'São José da Lapa', 'Florestal', 'Rio Manso', 'Confins', 'Mário Campos', 'São Joaquim de Bicas', 'Sarzedo', 'Baldim', 'Capim Branco', 'Jaboticatubas', 'Taquaraçu de Minas', 'Itaguara', 'Matozinhos', 'Nova União', 'Itatiaiuçu') ";

$and_voluntario = " AND EXISTS (SELECT 1 
                                       FROM   voluntario v 
                                       WHERE  v.codigo = a.codigo) ";

$and_nao_voluntario = " AND NOT EXISTS (SELECT 1 
                                       FROM   voluntario v 
                                       WHERE  v.codigo = a.codigo) ";

$and_idade_minima = " AND EXISTS (SELECT 1
									FROM aluno a1
									WHERE a1.CODIGO = a.CODIGO
										AND (YEAR(CURDATE()) - YEAR(a1.DATA_NASCIMENTO) - IF(STR_TO_DATE(CONCAT(YEAR(CURDATE()), '-', MONTH(a1.DATA_NASCIMENTO), '-', DAY(a1.DATA_NASCIMENTO)) ,'%Y-%c-%e') > CURDATE(), 1, 0) ) >= (?) ) ";

$and_idade_maxima = " AND EXISTS (SELECT 1
									FROM aluno a1
									WHERE a1.CODIGO = a.CODIGO
										AND (YEAR(CURDATE()) - YEAR(a1.DATA_NASCIMENTO) - IF(STR_TO_DATE(CONCAT(YEAR(CURDATE()), '-', MONTH(a1.DATA_NASCIMENTO), '-', DAY(a1.DATA_NASCIMENTO)) ,'%Y-%c-%e') > CURDATE(), 1, 0) ) <= (?) ) ";


$and_possui_curso_entrada = " AND EXISTS (SELECT 1 
				                           FROM   aluno_turma at, 
				                                  turma t, 
				                                  evento e 
				                           WHERE  at.codigo_turma = t.codigo 
				                                  AND e.id = t.id_evento
				                                  AND ( lower(e.nome) LIKE 'cip %'
				                                         OR lower(e.nome) LIKE 'cpc %'
				                                         OR lower(e.nome) LIKE 'pacifismologia'
				                                         OR lower(e.nome) LIKE 'assistenciologia')
				                                  AND at.codigo_aluno = a.codigo) ";

$and_nao_possui_curso_entrada = " AND NOT EXISTS (SELECT 1 
						                           FROM   aluno_turma at, 
						                                  turma t, 
						                                  evento e 
						                           WHERE  at.codigo_turma = t.codigo 
						                                  AND e.id = t.id_evento
						                                  AND ( lower(e.nome) LIKE 'cip %'
						                                         OR lower(e.nome) LIKE 'cpc %'
						                                         OR lower(e.nome) LIKE 'pacifismologia'
						                                         OR lower(e.nome) LIKE 'assistenciologia')
						                                  AND at.codigo_aluno = a.codigo) ";

$and_fez_curso_x_ha_n_tempo = " AND EXISTS (SELECT 1 
		                           FROM   aluno_turma at, 
		                                  turma t, 
		                                  evento e 
		                           WHERE  at.codigo_turma = t.codigo 
		                                  AND e.id = t.id_evento
		                                  AND at.CODIGO_ALUNO = a.CODIGO
		                                  AND e.id = (?)
		                                  AND IFNULL(t.DATA_FINAL, t.DATA_INICIAL) BETWEEN (?) AND curdate() ) "; // substituir pela data maxima do curso e pelo id do curso		

$and_nao_fez_curso_x_apos_n_data = " AND NOT EXISTS (SELECT 1 
		                           FROM   aluno_turma at, 
		                                  turma t, 
		                                  evento e 
		                           WHERE  at.codigo_turma = t.codigo 
		                                  AND e.id = t.id_evento
		                                  AND at.CODIGO_ALUNO = a.CODIGO
		                                  AND e.id = (?)
		                                  AND IFNULL(t.DATA_FINAL, t.DATA_INICIAL) BETWEEN (?) AND curdate() ) "; // substituir pela data maxima do curso e pelo id do curso		                                  




$and_ja_fez_cap = " AND EXISTS (SELECT 1 
		                           FROM   aluno_turma at, 
		                                  turma t, 
		                                  evento e 
		                           WHERE  at.codigo_turma = t.codigo 
		                                  AND e.id = t.id_evento
		                                  AND ( lower(e.nome) LIKE 'cap %') 
		                                  AND at.codigo_aluno = a.codigo) ";

$and_ja_fez_cl = " AND EXISTS (SELECT 1 
		                           FROM   aluno_turma at, 
		                                  turma t, 
		                                  evento e 
		                           WHERE  at.codigo_turma = t.codigo 
		                                  AND e.id = t.id_evento
		                                  AND ( lower(e.nome) LIKE 'cl %') 
		                                  AND at.codigo_aluno = a.codigo) ";

$and_ultima_visita_iipc = " AND EXISTS (SELECT 1 
		                           FROM   aluno_turma at, 
		                                  turma t, 
		                                  evento e 
		                           WHERE  at.codigo_turma = t.codigo 
		                                  AND e.id = t.id_evento
		                                  AND at.CODIGO_ALUNO = a.CODIGO
		                                  AND IFNULL(t.DATA_FINAL, t.DATA_INICIAL) BETWEEN (?) AND curdate() ) ";

		                               
$and_demonstrou_muito_interesse = " AND EXISTS (SELECT 1 
						                           FROM   dados_aluno d
						                           WHERE  d.aluno_CODIGO = a.CODIGO;
						                           	AND d.fl_muito_interessado = 1 ";	

$and_fez_o_curso_x = " AND EXISTS (SELECT 1 
		                           FROM   aluno_turma at, 
		                                  turma t, 
		                                  evento e 
		                           WHERE  at.codigo_turma = t.codigo 
		                                  AND e.id = t.id_evento
		                                  AND e.id = (?) 
		                                  AND at.codigo_aluno = a.codigo) ";	                                  

$and_nao_fez_nenhum_curso_x = " AND NOT EXISTS (SELECT 1 
		                           FROM   aluno_turma at, 
		                                  turma t, 
		                                  evento e 
		                           WHERE  at.codigo_turma = t.codigo 
		                                  AND e.id = t.id_evento
		                                  AND e.id in (?) 
		                                  AND at.codigo_aluno = a.codigo) ";	

$and_fez_algum_curso_x = " AND EXISTS (SELECT *
		                           FROM   aluno_turma at, 
		                                  turma t, 
		                                  evento e 
		                           WHERE  at.codigo_turma = t.codigo 
		                                  AND e.id = t.id_evento
		                                  AND e.id in (?) 
		                                  AND at.codigo_aluno = a.codigo) ";

$and_tem_interesse_algum_tema = " AND EXISTS (SELECT *
		                           FROM  tema_interesse ti 
		                           WHERE  ti.tema_id in (?) 
		                                  AND ti.aluno_CODIGO = a.codigo) ";

$and_tem_interesse_algum_curso = " AND EXISTS (SELECT *
					                           FROM  evento_interesse ei 
					                           WHERE  ei.evento_id in (?) 
					                                  AND ei.aluno_CODIGO = a.codigo) ";

$and_veio_no_minimo_x_eventos = " AND EXISTS (SELECT 1
												FROM aluno_turma atu
												WHERE atu.CODIGO_ALUNO = a.codigo
												GROUP by atu.CODIGO_ALUNO
												HAVING count(atu.CODIGO_TURMA) > ?) ";	
