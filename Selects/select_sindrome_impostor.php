<?php

ob_start();
?>
SELECT DISTINCT nome                                   AS NOME_SIMPLES, 
                email, 
                codigo, 
                cidade, 
                Concat( 
'=hyperlink("http://iipcnet.iipc.org/sources/formEntryNew.asp?FunctionKey=27&KeyValue=' 
, codigo, '";"', nome, '")')         AS nome, 
(SELECT Coalesce(Count(*), 0) 
 FROM   aluno_turma at2 
 WHERE  at2.codigo_aluno = a.codigo 
 GROUP  BY a.codigo 
 HAVING Count(at2.codigo_turma) > 1)     AS quantidade, 
(SELECT Max(t2.data_inicial) 
 FROM   aluno_turma at2, 
        turma t2 
 WHERE  at2.codigo_turma = t2.codigo 
        AND at2.codigo_aluno = a.codigo) AS dt_ultimo_evento 

FROM   (SELECT a.* 
        FROM   aluno a 
        WHERE  Lower(a.cidade) IN ( "belo horizonte", "nova lima", "contagem", 
                                    "betim", 
                                    "ribeirão das neves", "sabará", "sabara", 
                                            "ribeirao das neves", 
                                                               "santa luzia" ) 

               /* PARTICIPOU DE QUALQUER EVENTO A PARTIR DE 2016 */ 
               AND EXISTS (SELECT 1 
                           FROM   aluno_turma at2, 
                                  turma t2, 
                                  evento e2 
                           WHERE  at2.codigo_turma = t2.codigo 
                                  AND e2.id = t2.id_evento 
                                  AND at2.codigo_aluno = a.codigo 
                                  AND t2.data_inicial >= '2016-01-01')
        and not exists (select 1 from voluntario v where v.codigo = a.codigo)       

                
AND EXISTS (SELECT 1 
                             FROM   aluno_turma at, 
                                    turma t, 
                                    evento e2 
                             WHERE  at.codigo_turma = t.codigo 
                                    AND e2.id = t.id_evento                                   
                                    AND (  Lower(e2.nome) LIKE "ecp2%" 
                                        OR Lower(e2.nome) LIKE "cpc %" or LOWER(e2.nome) LIKE "SEMINARIO DE PESQUISAS" or lower(e2.nome) like "AUTOPESQUISA PROJECIOLOGICA" or lower(e2.nome) like "ESCOLA DE PROJEÇAO LUCIDA") 
                              
                                     AND at.codigo_aluno = a.codigo)                         
        AND EXISTS (SELECT 1 
                             FROM   aluno_turma at, 
                                    turma t, 
                                    evento e3 
                             WHERE  at.codigo_turma = t.codigo 
                                    AND e3.id = t.id_evento 
                
                                    AND ( Lower(e3.nome) LIKE "cip%" OR  Lower(e3.nome) LIKE "ecp1%"
                                         OR Lower(e3.nome) LIKE "pacifismologia%"
                                        OR Lower(e3.nome) LIKE "assistenciologia%"  ) 
                                    AND at.codigo_aluno = a.codigo) 

        
                                   AND NOT EXISTS (SELECT 1
                                       FROM ultima_ligacao u
                                       WHERE u.aluno_CODIGO = a.CODIGO
                                       AND (u.data > DATE_SUB(curdate(), INTERVAL 3 WEEK)
                                       OR u.data > curdate()))      

                                   AND NOT EXISTS (SELECT 1
                                       FROM ligacoes_tmk l
                                       WHERE l.aluno_CODIGO = a.CODIGO
                                       AND (l.data > DATE_SUB(curdate(), INTERVAL 3 WEEK))) 

       
       ) a   


ORDER  BY dt_ultimo_evento DESC
<?php
$sql = ob_get_clean();
?>