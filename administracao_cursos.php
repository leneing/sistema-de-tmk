<?php
	error_reporting(E_WARNING);
	include "conectasql.php";
	session_start();

	$turmas_ativas_filtro = "SELECT DISTINCT e.NOME, e.id FROM turma t, evento e, select_lista l WHERE t.DATA_INICIAL > CURDATE() and e.id = t.ID_EVENTO and l.id_turma = t.CODIGO";  
	$res_turma = $conexao ->query($turmas_ativas_filtro);


	?>

	<HTML>

	<HEAD>
			<TITLE>IIPC BH - TMK</TITLE>


			<link rel="stylesheet" href="bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
			<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
			<link rel="stylesheet" href="estilo.css">

			<script src="jquery-3.3.1.min.js" crossorigin="anonymous"></script>
			<script src="jquery-ui.js"></script>
			<script src="combobox.js"></script>
			<script src="popper.min.js" crossorigin="anonymous"></script>


			<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

			<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

			<link rel="stylesheet" href="multiple-select-master/multiple-select.css">
			<script src="multiple-select-master/multiple-select.js"></script>	


			<style type="text/css">
				.form-center{   
					width: 20%;
					padding: .375rem .75rem;
					font-size: 1rem;
					line-height: 1.5;
					color: #495057;
					background-color: #fff;
					background-clip: padding-box;
					border: 1px solid #ced4da;
					border-radius: .25rem;
					transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
				}
				.form-radius{  
					font-size: 1rem;
					line-height: 1.5;
					color: #495057;
					background-color: #fff;
					background-clip: padding-box;
					border: 1px solid #ced4da;
					border-radius: .25rem;
					transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
				}

				fieldset.scheduler-border {   
					text-align: center;
					background-color: #f7f9fc;
					min-width: 200px;             
					padding: 10px !important;
					-webkit-box-shadow:  0px 0px 0px 0px #000;
							box-shadow:  0px 0px 0px 0px #000;
				}
			</style>

			<script>
                
				$(document).ready(function () {  
					document.getElementById('btn_detalhes').disabled = true; 

					
					$('#filtro_alunos_check').multipleSelect({
						selectAll: true,
						width: 350,
						onClick: function(view) {							
							carrega_contatos();
						},
						onCheckAll: function(view) {							
							carrega_contatos();
						},
						onUncheckAll: function(view) {							
							carrega_contatos();
						}
					});

					$("#turma_selecionada").change(function (){
						$("#quantidade_1").text('0');
						$("#quantidade_2").text('0');
						$("#quantidade_3").text('0');
						$("#quantidade_4").text('0');
						$("#quantidade_5").text('0');
						$("#quantidade_6").text('0');
						$("#quantidade_7").text('0');
						$("#quantidade_0").text('0');

						
						$("#filtro_alunos_check").multipleSelect("setSelects", [1,2,3]);						

						carrega_contatos();
					});

					$('#curso_selecionado').change(function(){
						$("#quantidade_1").text('0');
						$("#quantidade_2").text('0');
						$("#quantidade_3").text('0');
						$("#quantidade_4").text('0');
						$("#quantidade_5").text('0');
						$("#quantidade_6").text('0');
						$("#quantidade_7").text('0');
						$("#quantidade_0").text('0');
						$.ajax({
							url: 'Turma_por_Curso.php',
							type: 'post',
							data: "curso_selecionado="+$("#curso_selecionado").val(),
							dataType: 'json',
							success: function(resultado_turmas){
								$("#turma_selecionada").empty()
								$("#turma_selecionada").append(
										$('<option></option>').val('0').html('Escolha...')
									);
								var i;
								for (i = 0; i < resultado_turmas.length; ++i) {
									$("#turma_selecionada").append(
										$('<option></option>').val(resultado_turmas[i]['id']).html(resultado_turmas[i]['nome'])
									);
								}  
							}
						});
						$("#body_tabela").empty();
					}); 
				});


				carrega_contatos = function(){
					$("#body_tabela").empty();
					if ($("#turma_selecionada").val() != 0){

					document.getElementById('btn_detalhes').disabled = false; 

						var filtros_checked = $('#filtro_alunos_check').multipleSelect('getSelects');

						$.ajax({
							url: 'busca_Alunos_contatados.php',
							type: 'post',
							data: "filtros_checked="+filtros_checked+"&turma="+$("#turma_selecionada").val(),
							dataType: 'json',
							success: function(alunos_contatados){
								var textcolor="";
								for (i = 0; i < alunos_contatados.length; ++i) {
									if (alunos_contatados[i]['estrela'] > 0){
										linha = "<tr style='background-color:#ffe59b'>";
										
									}else {if(alunos_contatados[i]['interesse'] > 0) {
										linha = "<tr style='background-color:#6ae887'>";
								
									}else {if(alunos_contatados[i]['sem_interesse'] > 0){
										linha = "<tr style='background-color:#ffadb5'>";
									
									}else{
										linha = "<tr style='background-color:#dee2e6'>";
									}}}

									var coluna_1;
									if (alunos_contatados[i]['estrela'] == 0){
										coluna_1 = "<td scope='row' style='width: 8%'><button class='btn btn-info btn-sm btn_ativar' aluno_selecionado = "+alunos_contatados[i]['aluno_codigo']+" fl_ativo ='0'>Acompanhar</button></td>";
									}else{
										coluna_1 = "<td scope='row' style='width: 8%'><button class='btn btn-danger btn-sm btn_inativar' aluno_selecionado = "+alunos_contatados[i]['aluno_codigo']+" fl_ativo ='1'>Não acompanhar</button></td>";
									}

									coluna_2 = "<td scope='row' style='width: 8%'><button class='btn btn-light btn-sm btn_expandir' aluno_selecionado = "+alunos_contatados[i]['aluno_codigo']+" fl_ativo ='0'>expandir +</button></td>";
									coluna_3 = "<td style='text-align: center'>"+alunos_contatados[i]['aluno_codigo']+"</td>";
									coluna_4 = "<td class='"+textcolor+"'>"+alunos_contatados[i]['nome']+"</td>";
									coluna_5 = "<td style='text-align: center'>"+alunos_contatados[i]['contatos']+"</td>";
									coluna_6 = "<td style='text-align: center'>"+alunos_contatados[i]['ultimo_contato']+"</td></tr>";

									$("#body_tabela").append(linha + coluna_1 + coluna_2 + coluna_3 + coluna_4 + coluna_5 + coluna_6);
								}
								carrega_butoes();
								carrega_quantidade();
							}
						});
					} else{
						$("btn_detalhes").attr("disabled","true");
					} 
					return false;
				}

				carrega_butoes = function (){
					$(".btn_ativar").click(function () {
						$.ajax({
							url: 'resultadoAcompanharAluno.php',
							type: 'post',
							data: "aluno_selecionado="+$(this).attr("aluno_selecionado")+"&fl_ativo="+$(this).attr("fl_ativo")+"&turma="+$("#turma_selecionada").val(),
							success: function(result_grava_banco){
								carrega_contatos();
							}
							});  
						return false;
					});

					$(".btn_inativar").click(function () {
						$.ajax({
							url: 'resultadoAcompanharAluno.php',
							type: 'post',
							data: "aluno_selecionado="+$(this).attr("aluno_selecionado")+"&fl_ativo="+$(this).attr("fl_ativo")+"&turma="+$("#turma_selecionada").val(),
							success: function(result_grava_banco){
								carrega_contatos();
							}
							});  
						return false;
					}); 

					$(".btn_expandir").click(function () {
						if ($(this).attr("fl_ativo") == 0){
							$(this).text('Ocultar -');
							$(this).attr("fl_ativo",1);  
							botao = $(this);                    
							$.ajax({
								url: 'busca_ligacoes_aluno.php',
								type: 'post',
								data: "aluno_selecionado="+botao.attr("aluno_selecionado")+"&turma="+$("#turma_selecionada").val(),
								dataType: 'json',
								success: function(ligacoes_aluno){
									for (i = 0; i < ligacoes_aluno.length; ++i) {
										botao.parent().parent().after("<tr class="+botao.attr("aluno_selecionado")+"><td></td><td></td><td></td><td style='border:2px solid #375b5b'>"+ligacoes_aluno[i]['value']+"</td><td style='text-align: center;border:2px solid #375b5b'>"+ligacoes_aluno[i]['label']+"</td><td style='text-align: center;border:2px solid #375b5b'>"+ligacoes_aluno[i]['id']+"</td></tr>");
									}
									botao.parent().parent().after("<tr class="+botao.attr("aluno_selecionado")+"><td></td><td></td><td></td><td style='text-align: center; background-color:#e9ecef; border:2px solid #375b5b'>Comentários</td><td style='text-align: center;  background-color:#e9ecef; border:2px solid #375b5b'>Resultado</td><td style='text-align: center; background-color:#e9ecef; border:2px solid #375b5b'>Data</td></tr>");
								}
							}); 
						}else{
							$(this).text('Expandir +');
							$(this).attr("fl_ativo",0);
							$('.'+$(this).attr("aluno_selecionado")).each(function() {
								$(this).remove();
							});
						}  
						return false;
					});
				};

				carrega_quantidade = function(){
					$.ajax({
						url: 'conta_alunos_contatadosturma.php',
						type: 'post',
						data: "turma="+$("#turma_selecionada").val(),
						dataType: 'json',
						success: function(quantidade){
							for (i = 0; i < quantidade.length; ++i) {
								$("#quantidade_"+quantidade[i]['tipo']).text(quantidade[i]['quantidade']);
							}
						}
					});
				}
                

			</script>
	</HEAD>

	<BODY class="bg-light">
			<?php
				include "navbar_adm.php";
			?> 
			<div class="container" style="margin-top: 15px;">
				<input type="hidden" id="aluno_codigo"/>
				<div class="text-center" id="div_titulo">       
					<img class="d-block mx-auto mb-4" src="Images/icolistapan.png" alt="" width="72" height="72">          
					<h2>Controle de Turmas</h2><br/><br/>                               
				</div>    
				
				<form id="frm_filtros">
					<div class="row mb-5"  style="text-align: left;">                    
						<div class="col-md-4">                
							<label for="curso_selecionado">Selecione o curso</label>
							<div class="row mb-3" style="text-align: center;">
								<select class="custom-select d-block w-100" name="curso_selecionado" id="curso_selecionado">
									<option value=''>Escolha...</option>
									<?php 
										while ($linha_turma = $res_turma -> fetch_assoc()){
										?>
											<option value="<?=$linha_turma['id']?>"><?=UTF8_ENCODE($linha_turma['NOME'])?></option>  
										<?php 
										}  
									?>
								</select>
							</div>
						</div>
						<div class="col-md-6">
						<div clas="col-md-4">
						<label for="turma_selecionada">Selecione a turma</label>
						</div>
						<div class="row">  
						<div class="col-md-4" style="text-align: center;">
							<select class="custom-select d-block w-100" name="turma_selecionada" id="turma_selecionada" required>
								<option value=''>Escolha...</option>
							</select>
							</div>
							<div class="col-md-2">  
								<button id="btn_detalhes" type="button" data-toggle="modal" data-target="#modal_detalhes" class="btn btn-sm btn-info" style="height:35px;disabled:true" >Ver Resumo
								</button>
							</div>
							</div>
						</div>						
                    </div>
                    

                </form>   
                
                <div id="modal_detalhes" class="modal" style="height:730px">
                    <div class="modal-content col-md-8 offset-2" style="padding-top: 0;">
                        <div class="row">
                            <div class="col-md-1 offset-11">
                                <span style="margin-top:10px" class="close">&times;</span>
                            </div>
                        </div>
                        <div class="col-md-10 offset-1">
                        <table class="table table-bordered table-striped" style="font-size:small">
                            <thead style="text-align: center">
                                <tr style="text-align: center"></tr>
                                <tr>
                                    <th style="text-align: center;">Tipo de resultado</th>
                                    <th style="text-align: center">Quantidade de alunos</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th style="text-align: center; font-weight:normal">Alunos acompanhados</th>
                                    <th style="text-align: center; font-weight:normal" id="quantidade_0">0</th>
                                </tr>
                                <tr>
                                    <th style="text-align: center; font-weight:normal">Demonstraram interesse</th>
                                    <th style="text-align: center; font-weight:normal" id="quantidade_3">0</th>
                                </tr>
                                <tr>
                                    <th style="text-align: center; font-weight:normal">Não demonstraram interesse</th>
                                    <th style="text-align: center; font-weight:normal" id="quantidade_2">0</th>
                                </tr>
                                <tr>
                                    <th style="text-align: center; font-weight:normal">Pediu para ligar depois</th>
                                    <th style="text-align: center; font-weight:normal" id="quantidade_6">0</th>
                                </tr>
                                <tr>
                                    <th style="text-align: center; font-weight:normal">Não atendeu</th>
                                    <th style="text-align: center; font-weight:normal" id="quantidade_1">0</th>
                                </tr>
                                <tr>
                                    <th style="text-align: center; font-weight:normal">Ligação pulada</th>
                                    <th style="text-align: center; font-weight:normal" id="quantidade_7">0</th>
                                </tr>
                                <tr>
                                    <th style="text-align: center;font-weight:normal">Número não existe</th>
                                    <th style="text-align: center; font-weight:normal" id="quantidade_4">0</th>
                                </tr>
                                <tr>
                                    <th style="text-align: center; font-weight:normal">Parar de ligar</th>
                                    <th style="text-align: center; font-weight:normal" id="quantidade_5">0</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                            <button type="button" class="btn btn-md btn-info" id="close_modal">Fechar</button>
                       
                    </div>
                </div>
				
				<div class="col-md-12">
					<div class="row">
						<div class="row col-6" style="">
								<p style="vertical-align: middle;;margin-right:15px;font-weight:bold">Filtrar Resultados</p>
								<select name="filtro_alunos_check" id="filtro_alunos_check" class="filtro_alunos_check" multiple="multiple">
									<option id="check_estrela"  value="1">Alunos acompanhados</option>
									<option id="check_interesse"value="2">Demonstraram interesse</option>
									<option id="check_seminteresse" value="3">Não demonstraram interesse</option>
									<option id="check_ligardepois"value="4">Pediu para ligar depois</option>
									<option id="check_naoatendeu" value="5">Não atendeu</option>
									<option id="check_pulada" value="6">Ligação pulada</option>
									<option id="check_naoexiste" value="7">Número não existe</option>
									<option id="check_parar" value="8">Parar de ligar</option>
								</select>							
						</div>
						<div class="row col-push-6 ml-auto" style="margin-left:15px;font-size: 13px;" >                    
							<p style="margin-right:15px;">Legenda:</p>
							<img src="Images/legazul.png" alt="" width="17" height="17" style="margin-right:10px">  
							<p style="margin-right:15px">Acompanhados</p>
							<img src="Images/legverde.png" alt="" width="17" height="17" style="margin-right:10px">  
							<p style="margin-right:15px">Interessados</p>
							<img src="Images/legvermelho.png" alt="" width="17" height="17" style="margin-right:10px">  
							<p style="margin-right:15px">Sem interesse</p>
						</div>
					</div>
				</div>
                
                    
                

				<div class="row">
					<div class="col-md-12">                            
						<form id="frm_resultado">  
							<div class="row" style="background-color:#f9fcfc;text-align: center; margin-top:5px;">
								  <table class="table" style="font-size: small; border: 1px solid #375b5b" id="table">
									<thead class="thead-dark table-solid" style="text-align: center;">
										<tr>
											<th></th>
											<th style="width: 8% "></th>
											<th style="width: 5% text-align: center">IIPC Net</th>
											<th style="max-width: 3 5%; width: 35%; text-align: center">Aluno</th>
											<th style="width: 8% text-align: center">Contatos feitos</th>
											<th style="width: 20% text-align: center">Último Contato</th>
										</tr>
									</thead>
									<tbody id="body_tabela"> 
										 
									</tbody>
								 </table>  
							</div>
						</form>         
				</div>
			</div>
            <script>
                var background = document.getElementById('background_detalhes');
                var modal = document.getElementById('modal_detalhes');
                var btn = document.getElementById("btn_detalhes");
                var span = document.getElementsByClassName("close")[0];
                var span1 = document.getElementById("close_modal");
                btn.onclick = function() {
                    modal.style.display = "block";
                }
                span.onclick = function() {
                    modal.style.display = "none";
                }
                span1.onclick = function() {
                    modal.style.display = "none";
                }
                window.onclick = function(event) {
                    if (event.target == modal) {
                        modal.style.display = "none";
                    }
                }
            </script>

		  <?php include "rodape.php"; ?>
	</BODY>

	</HTML>