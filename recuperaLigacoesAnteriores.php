<?php
include "conectasql.php";
session_start();

$ligacoes_anteriores = $conexao->prepare("SELECT DATE_FORMAT(data,'%d/%m/%Y') as data, DATE_FORMAT(data,'%H:%i') as hora,
	CASE l.tipo_resultado
    WHEN 1 THEN t.descricao 
    WHEN 6 THEN t.descricao 
	WHEN 4 THEN t.descricao
    ELSE 'Atendeu' END AS atendeu, comentarios, l.tipo_resultado AS tpresultado 
	from ligacoes_tmk l INNER JOIN tipo_resultado_tmk t on t.id = l.tipo_resultado WHERE aluno_CODIGO = (?) AND l.tipo_resultado != 7 order by UNIX_TIMESTAMP(data) desc ");
$ligacoes_anteriores -> bind_param("s",$_POST["codigo_aluno"]);
$ligacoes_anteriores -> execute();
$res_ligacoes_anteriores = $ligacoes_anteriores->get_result();
$ligacoes_anteriores -> close();

?>

 	<table class="table table-sm  table-bordered">
                    <thead style="text-align: center;"> 
                    	<th>Data</th>
                    	<th>Resultado</th>
                    	<th>Comentário</th>           
                     </thead>
                     <tbody> 
                            <?php
                                while ($linha_lig = $res_ligacoes_anteriores -> fetch_assoc()){
                                	if ($linha_lig['tpresultado'] == 1) {
                                	?>	<tr class="table-danger">
                                	<?php
                                	}else if($linha_lig['tpresultado'] == 6 || $linha_lig['tpresultado'] == 7){
                                	?>	<tr class="table-light">
                                	<?php

                                	}else{
                                	?>	<tr class="table-success">
                                	<?php

                                	}
							?>
                            	<td><?=utf8_encode($linha_lig['data'])?><br/><b><?=utf8_encode($linha_lig['hora'])?></b></td>
                            	<td><?=utf8_encode($linha_lig['atendeu'])?></td>
                            	<td><?=utf8_encode($linha_lig['comentarios'])?></td>
                            </tr>
                            <?php
                                }
                            ?>
                    </tbody>
                 </table>  

