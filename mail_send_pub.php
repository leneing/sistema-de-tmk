#!/usr/bin/php
<?php
include "conectasql.php";

$inicio = date("Y-m-d", strtotime('-7 days')) . ' 00:00:00';
$fim = date("Y-m-d", strtotime("yesterday")) . ' 23:59:00';
$dt =  date("d/m/Y", strtotime("-7 days"));
$dtf =  date("d/m/Y", strtotime("yesterday"));
$cabecalho = "Relatorio da Semana: $dt a $dtf" . "\r\n\r\nJornada Total de TMK:\r\n\r\n";

var_dump($inicio);
var_dump($fim);

/*  colocar nos vardupms e echos o que quiser, para debugar via relatorio do cron*/

$jornada = $conexao->prepare("SELECT 
	SUM(TIMESTAMPDIFF(MINUTE, s.inicio, s.termino)) DIV 60 AS horas,
	SUM(TIMESTAMPDIFF(MINUTE, s.inicio, s.termino)) % 60 AS minutos    
	FROM
	sessaotmk s 
	WHERE s.inicio > (?) AND s.termino < (?) 
	AND EXISTS (SELECT 1 FROM ligacoes_tmk l WHERE l.sessaotmk_id = s.id)");

$jornada -> bind_param("ss", $inicio,$fim);
$jornada -> execute();

$res_jornada = $jornada->get_result();
var_dump($res_jornada);
$contar_jornada = mysqli_num_rows($res_jornada);
$jornada -> close(); 

$html = "";
$i = 1;
while($ret = $res_jornada -> fetch_assoc()){  	 
	 $r2 = $ret['horas'];
	 $r3 = $ret['minutos'];
    $html .= "   - $r2 horas $r3 min\r\n";
}

$html .= "\r\n_______________________________\r\n";

$html .= "\r\nResultados: \r\n\r\n\r\n";

/*
total de ligações por voluntário entre os dias x e y
total de ligações não atendidas por voluntário entre os dias x e y
total de ligações atendidas por voluntário entre os dias x e y
total de ligações puladas por voluntário entre os dias x e y
total de ligações parar de ligar por voluntário entre os dias x e y
*/
$ligacoes = $conexao->prepare("
SELECT e.NOME as curso,
    count(l.id) as ligacoesfeitas, e.id as evento_id,
     IFNULL(( SELECT count(l1.id)
     FROM
        sessaotmk s1 inner join ligacoes_tmk l1 on l1.sessaotmk_id = s1.id
        inner join voluntario v1 on v1.codigo = s1.voluntario_id
        INNER join evento e1 on e1.id = l1.evento_id
        WHERE l1.data > (?) AND l.data < (?)
	        AND e1.id = e.id
	     	and l1.tipo_resultado in (1,4) 
		GROUP BY e.NOME),0) as naoatendeu,
   IFNULL((SELECT count(l1.id)
     FROM
        sessaotmk s1 inner join ligacoes_tmk l1 on l1.sessaotmk_id = s1.id
        inner join voluntario v1 on v1.codigo = s1.voluntario_id
        INNER join evento e1 on e1.id = l1.evento_id
        WHERE l1.data > (?) AND l.data < (?)
	        AND e1.id = e.id
	     	and l1.tipo_resultado in (2,3,6) 
		GROUP BY e.NOME),0) as atendeu,
   IFNULL((SELECT count(l1.id)
     FROM
        sessaotmk s1 inner join ligacoes_tmk l1 on l1.sessaotmk_id = s1.id
        inner join voluntario v1 on v1.codigo = s1.voluntario_id
        INNER join evento e1 on e1.id = l1.evento_id
        WHERE l1.data > (?) AND l.data < (?)
	        AND e1.id = e.id
	     	and l1.tipo_resultado in (5) 
		GROUP BY e.NOME),0) as pararligar,
   IFNULL((SELECT count(l1.id)
     FROM
        sessaotmk s1 inner join ligacoes_tmk l1 on l1.sessaotmk_id = s1.id
        inner join voluntario v1 on v1.codigo = s1.voluntario_id
        INNER join evento e1 on e1.id = l1.evento_id
        WHERE l1.data > (?) AND l.data < (?)
	        AND e1.id = e.id
	     	and l1.tipo_resultado = 7 
		GROUP BY e.NOME), 0) as pulou 
       
FROM
sessaotmk s inner join ligacoes_tmk l on l.sessaotmk_id = s.id
INNER join evento_ativo e on e.id = l.evento_id
WHERE l.data > (?) AND l.data < (?)
GROUP BY e.NOME");

$ligacoes -> bind_param("ssssssssss", $inicio,$fim,$inicio,$fim,$inicio,$fim,$inicio,$fim,$inicio,$fim);
$ligacoes -> execute();

$res_ligacoes = $ligacoes->get_result();
$contar = mysqli_num_rows($res_ligacoes);
$ligacoes -> close(); 


$i = 1;
while($ret = $res_ligacoes -> fetch_assoc()){ 
	 $r2 = $ret['curso'];
	 $r4 = $ret['ligacoesfeitas'];
	 $r5 = $ret['naoatendeu'];
	 $r6 = $ret['atendeu'];
	 $r7 = $ret['pararligar'];
	 $r8 = $ret['pulou'];
	 $ev = $ret['evento_id'];

	$jornada_curso = $conexao->prepare("SELECT 
	SUM(TIMESTAMPDIFF(MINUTE, s.inicio, s.termino)) DIV 60 AS horas,
	SUM(TIMESTAMPDIFF(MINUTE, s.inicio, s.termino)) % 60 AS minutos    
	FROM
	sessaotmk s 
	WHERE s.inicio > (?) AND s.termino < (?) 
	AND EXISTS (SELECT 1 FROM ligacoes_tmk l WHERE l.sessaotmk_id = s.id and l.evento_id = ?)");

	$jornada_curso -> bind_param("sss",$inicio,$fim,$ev);
	$jornada_curso -> execute();

	$res_jornada_curso = $jornada_curso->get_result();
	$contar = mysqli_num_rows($res_jornada_curso);
	$jornada_curso -> close(); 

	$res_lista = $res_jornada_curso -> fetch_assoc();
	$hora = $res_lista['horas'];
	$minuto = $res_lista['minutos'];
    
    $html .= "Foco: $r2\r\n";
	$html .= "Tempo de TMK: $hora horas e $minuto minutos\r\n\r\n";
    $html .= "Total ligacoes: $r4\r\n";
    $html .= "Nao Atendidas/Numero nao existe: $r5\r\n";
    $html .= "Atendidas: $r6\r\n";
    $html .= "Parar de ligar: $r7\r\n";
    $html .= "Ligacoes puladas: $r8\r\n\r\n";


    $ligacoes_do_dia_por_foco =  $conexao->prepare("SELECT l.id as id_ligacao FROM ligacoes_tmk l inner join sessaotmk s on l.sessaotmk_id = s.id WHERE l.data > (?) AND l.data < (?) AND l.evento_id = (?)");
    
	$ligacoes_do_dia_por_foco -> bind_param("sss", $inicio,$fim,$ret['evento_id']);
	$ligacoes_do_dia_por_foco -> execute();

	$res_ligacoes_do_dia_por_foco = $ligacoes_do_dia_por_foco->get_result();
	$ligacoes_do_dia_por_foco -> close(); 

	$zero_interessados = true;

	while($lig = $res_ligacoes_do_dia_por_foco -> fetch_assoc()){  
	    $interessados = $conexao->prepare("SELECT a.CODIGO as codigo, a.nome as nome, e.NOME as evento, l.comentarios as comentario
		FROM ligacoes_tmk l inner join sessaotmk s on s.id = l.sessaotmk_id
		INNER JOIN evento_interesse ei on ei.id_ligacao = l.id
		INNER JOIN evento e on e.id = ei.evento_id
		INNER JOIN aluno a on a.CODIGO = l.aluno_CODIGO
		WHERE l.id = (?)");
		$interessados -> bind_param("i", $lig['id_ligacao']);
		$interessados -> execute(); 
		$res_interessados = $interessados->get_result();
		if(mysqli_num_rows($res_interessados) > 0){
			$zero_interessados = false;			
		}
		while($res = $res_interessados -> fetch_assoc()){  
			$ei1 = $res['codigo'];
			$ei2 = $res['nome'];
			$ei3 = $res['evento'];
			$ei4 = $res['comentario'];	
			
		    $html .= "$ei1 - $ei2 - $ei3 \r\n - $ei4 \r\n\r\n";    
		}
		$interessados -> close(); 
	}
	if($zero_interessados){
		$html .= "Interessados: 0 \r\n\r\n";
	}

$html .= "_______________________________\r\n\r\n";

    $i++;
}


$to = "lene.ingrid@gmail.com, joaomonteiro234@gmail.com, fabioiipcbh@gmail.com, moneisa123@gmail.com ";
$subject = "IIPC/Coordenação - Relatório Detalhado do TMK - $dt a $dtf";

$message = $cabecalho . $html;
//echo $message;
//echo $subject;
var_dump($message);
// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/plain;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From: IIPC-BH TMK <iipc.tmk.pc@gmail.com>' . "\r\n";
//$headers .= 'Cc: myboss@example.com' . "\r\n";

mail($to,$subject,$message,$headers);
