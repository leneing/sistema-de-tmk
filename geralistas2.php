 <?php
error_reporting(E_WARNING);
include "conectasql.php";
session_start();

//unset ($_SESSION["aluno_CODIGO"]);


$cursos_interesse = "SELECT DISTINCT e.* FROM `evento_ativo` e where e.ativo = 1";
$res_curso_ativo = $conexao ->query($cursos_interesse);
$res_curso_ativo2 = $conexao ->query($cursos_interesse);
$res_curso_ativo3 = $conexao ->query($cursos_interesse);
$res_curso_ativo4 = $conexao ->query($cursos_interesse);

?>

<!doctype html>
<html">
  <head>
    <meta charset="utf-8">

    <title>Gera Lista TMK</title>

       <link rel="stylesheet" href="jQuery-Plugin-For-Multi-Select-Checboxes-multiselect/css/jquery.multiselect.css">

        <link rel="stylesheet" href="bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="estilo.css">

        <script src="jquery-3.3.1.min.js" crossorigin="anonymous"></script>
        <script src="jQuery-Plugin-For-Multi-Select-Checboxes-multiselect/js/jquery.multiselect.js"></script>
        <script src="jquery-ui.js"></script>
        <script src="combobox.js"></script>


        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

        <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

        <style type="text/css">
            .form-center{   
                width: 20%;
                padding: .375rem .75rem;
                font-size: 1rem;
                line-height: 1.5;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                border-radius: .25rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            }
            .form-radius{  
                font-size: 1rem;
                line-height: 1.5;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                border-radius: .25rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            }

            fieldset.scheduler-border {   
                text-align: center;
                background-color: #f7f9fc;
                min-width: 200px;             
                padding: 10px !important;
                -webkit-box-shadow:  0px 0px 0px 0px #000;
                        box-shadow:  0px 0px 0px 0px #000;
            }
        </style>


      <script>

        $(function(){        
            interesse = $('#cursos_feitos');            
            interesse.multiselect({
                Bar: true,
                addSearchBox: false,
                addActionBox: true,
                animateSearch: false, // Can be 'normal', 'slow', 'fast', or int number
                showCheckboxes: true,
                showSelectedItems: false,
                overwriteName: false,
                submitDataAsArray: true,
                preferIdOverName: true, 
                initSelection: $.noop(),
                cssContainer: 'checklistContainer',
                cssChecklist: 'checklist',
                cssChecklistHighlighted : 'checklistHighlighted',
                cssLeaveRoomForCheckbox : 'leaveRoomForCheckbox', // For label elements
                cssEven: 'even',
                cssOdd: 'odd',
                cssChecked: 'checked',
                cssDisabled: 'disabled',
                cssShowSelectedItems: 'showSelectedItems',
                cssFocused: 'focused', // This cssFocused is for the li's in the checklist
                cssFindInList: 'findInList',
                cssBlurred: 'blurred', // This cssBlurred is for the findInList divs.
                cssOptgroup: 'optgroup',
                listWidth: 500,
                itemWidth: 500
            });          
          }); 
      
        $(document).ready(function (){   

          $("#btn_gera_lista").click(function () {                          
            $.ajax({
                url: 'index.php',
                type: 'post',
                data: $("#frm_filtro").serialize(),
                success: function(resultado_filtro){
                    window.location = 'index.php';
                }
            });
        
            return false;
          });           
        
          $("#dataultimavisita").datepicker({
                dateFormat: 'dd/mm/yy',
                dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                nextText: 'Próximo',
                prevText: 'Anterior'
          }); 
       };
      </script>
  </head>

  <body class="bg-light">

    <?php
            include "navbar.php";
        ?> 

    <div class="container">
      <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="Images/listas.png" alt="" width="72" height="72">
        <h2>Gerar Listas</h2>
        <p class="lead">Todos os filtros são opcionais. Preencha apenas aqueles necessários para sua lista.</p>
      </div>
        <div class="col-md-12 order-md-1">
          <h4 class="mb-3">Filtros</h4>
          <form id="frm_filtro" name="frm_filtro">

            <hr class="mb-4">
            <h6 style="margin-bottom: 25px">Sobre o aluno</h6>
            <div class="row">
              <div class="col-md-2 mb-3">
                <label for="voluntario">É Voluntário?</label>
                <select class="custom-select d-block w-100" id="voluntario" name="voluntario" >
                  <option value=""> </option>
                  <option value="1">Sim</option>
                  <option value="0">Não</option>
                  <option value="3">Ambos</option>
                </select>
                
              </div>
              <div class="col-md-3 mb-3">
                <label for="metropolitana">Mora na região metropolitana?</label>
                <select class="custom-select d-block w-100" id="metropolitana" name="metropolitana" >
                  <option value=""> </option>
                  <option value="1">Sim</option>
                  <option value="0">Não</option>
                </select>
              
              </div>
              <div class="col-md-3 md-3">
                <label for="dataultimavisita">Data máxima da última ida ao IIPC</label>
                <input id="dataultimavisita" type="text" name="dataultimavisita" value="" style="text-align: center; width: 120; margin-left: 10px" class="form-control" />
             
              </div>
            </div>
            <hr class="mb-4">
            <h6 style="margin-bottom: 25px">Sobre os cursos</h6>

            <div class="row">
              <div class="col-md-4 mb-3">
                <label for="curso_entrada">Possui curso de entrada</label>
                <select class="custom-select d-block w-100" id="curso_entrada" name="curso_entrada" >
                  <option value=""> </option>
                  <option value="1">Sim</option>
                  <option value="0">Não</option>
                  <option value="3">Tanto faz</option>
                </select>
              
              </div>


            </div>

            <div class="mb-3">
              <label for="cursos_feitos">Cursos feitos</label>
              <div>
                <select id="cursos_feitos" name="cursos_feitos" multiple="multiple" size="15" style="height: 150px" >
                <?php 
                    while ($linha_curso_ativo = $res_curso_ativo -> fetch_assoc()){
                     ?>
                        <option value="<?=$linha_curso_ativo['id']?>"><?=$linha_curso_ativo['NOME']?></option>  
                     <?php 
                    }  
                ?>   
              </select>  
             
              </div>
            </div>

            <div class="mb-3">
              <label for="cursos_nao_feitos">Cursos não feitos</label>
              <div>
                <select id="cursos_nao_feitos" name="cursos_nao_feitos" multiple="multiple" size="15" style="height: 150px" >
                <?php 
                    while ($linha_curso_ativo = $res_curso_ativo2 -> fetch_assoc()){
                     ?>
                        <option value="<?=$linha_curso_ativo['id']?>"><?=$linha_curso_ativo['NOME']?></option>  
                     <?php 
                    }  
                ?>   
              </select>  
             
              </div>
            </div>
            <hr class="mb-4">
            <h6 style="margin-bottom: 25px">Filtro: fez determinado curso há no máximo x tempo</h6>


            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="curso1">Curso</label>
                <select class="custom-select d-block w-100" id="curso1" name="curso1" >
                  <option value=""> </option>
                  <?php 
                    while ($linha_curso_ativo = $res_curso_ativo3 -> fetch_assoc()){
                     ?>
                        <option value="<?=$linha_curso_ativo['id']?>"><?=$linha_curso_ativo['NOME']?></option>  
                     <?php 
                    }  
                ?>   
                </select>
                
              </div>
              <div class="col-md-6 mb-3">
                <label for="tempo1">Data máxima</label>
                  <input id="tempo1" type="text" name="tempo1" value="" style="text-align: center; width: 120; margin-left: 10px" class="form-control" />

             
              </div>
            </div>

            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="curso2">Curso</label>
                <select class="custom-select d-block w-100" id="curso2" name="curso2" >
                  <option value=""> </option>
                  <?php 
                    while ($linha_curso_ativo = $res_curso_ativo4 -> fetch_assoc()){
                     ?>
                        <option value="<?=$linha_curso_ativo['id']?>"><?=$linha_curso_ativo['NOME']?></option>  
                     <?php 
                    }  
                ?>   
                </select>
               
              </div>
              <div class="col-md-6 mb-3">
                <label for="tempo2">Data máxima</label>
                  <input id="tempo2" type="text" name="tempo2" value="" style="text-align: center; width: 120; margin-left: 10px" class="form-control" />

               
              </div>
            </div>

           
            <hr class="mb-4">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="check_muito_interesse" name="check_muito_interesse">
              <label class="custom-control-label" for="check_muito_interesse">Demonstrou muito interesse pela conscienciologia</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="check_cap" name="check_cap">
              <label class="custom-control-label" for="check_cap">Já fez CAP</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="check_cl" name="check_cl">
              <label class="custom-control-label" for="check_cl">Já fez CL</label>
            </div>
            <hr class="mb-4">

            
            <hr class="mb-4">
            <button id="btn_gera_lista" name="btn_gera_lista" class="btn btn-primary btn-lg btn-block">Gerar Lista</button>
          </form>
        </div>
        <script src="popper.min.js" crossorigin="anonymous"></script>
        <script src="bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>

      
      <?php include "rodape.php"; ?>
    </div>

   
  </body>
</html>
