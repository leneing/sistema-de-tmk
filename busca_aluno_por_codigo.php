<?php
include "conectasql.php";
session_start();

$ligacoes_filtro = "SELECT DATE_FORMAT(data,'%d/%m/%Y') as data, 
  DATE_FORMAT(data,'%H:%i') as hora,
    e.NOME as nmevento,
    e.id as id_evento,
  a.NOME as nmaluno,
  l.aluno_CODIGO as aluno_CODIGO,
    v.nome as nmvoluntario,
    comentarios,
    DATE_FORMAT(l.data_retorno, '%d/%m/%Y - %H:%i') as data_retorno
  from ligacoes_tmk l INNER JOIN aluno a on a.CODIGO = l.aluno_CODIGO
    inner join evento_ativo e on e.id = l.evento_id
    inner join sessaotmk s on s.id = l.sessaotmk_id
    inner join voluntario v on v.codigo = s.voluntario_id    
  WHERE fl_pendente_retorno = 1
  order by data_retorno";



$res_ligacoes_pendentes = $conexao ->query($ligacoes_filtro);

?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>IIPC BH - TMK - Busca Aluno</title>

        <link rel="stylesheet" href="bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="jquery-ui.css">
        <script src="jquery-3.3.1.min.js" crossorigin="anonymous"></script>
        <script src="jquery-ui.js"></script>
        <link href="gijgo.min.css" rel="stylesheet" type="text/css"/>
        <script src="gijgo.min.js" type="text/javascript"></script>
        <script src="combobox.js"></script>
        
        <link rel="stylesheet" href="estilo.css">

    <script>

    $(document).ready(function () {
        $( "#select_evento" ).autocomplete({
          source: "listacurso.php",
          minLength: 1,
          open: function(event) {
          valid = false;
          },
          close: function(event){
              if (!valid) $(this).val('');
          },
          select: function (event, ui) {
              valid = true;
              $("#select_eventoID1").val(ui.item.id); 
          }
        });  

        $(".btn_buscar").click(function () {
          if ($("#frm_ligacoes")[0].checkValidity()) {
            location.href = "redirecionaComCodigoAluno.php?aluno_CODIGO="+$("#aluno_codigo").val()+"&curso_selecionado="+$("#select_eventoID1").val()+"&tela_origem=busca_aluno_por_codigo.php"; 
            return false;
          }
        });
      });
    </script>
  </head>

  <body class="bg-light">

    <?php
            include "navbar.php";
        ?> 

    <div class="container">
      <div class="col-md-12">
      <div class="py-5 text-center">
        <img class="d-block mx-auto mb-2" src="Images/icobuscaaluno.png" alt="" width="72" height="72">
        <h2>Buscar Aluno</h2>
        <p class="lead">Informe o curso e o código IIPCNET do aluno:</p>
      
        <form id="frm_ligacoes">
            <div class="row">
              <div class="col-md-4 offset-4" style="margin-top: 45px">
                  <h6 class="required" for="select_evento">Divulgação de qual curso?</h6>
                  <div>
                      <input type="text" style="background-color: white;" class="form-control input-lg" id="select_evento" value="" required="">
                      <input type='hidden' id='select_eventoID1' type='text' name='select_evento[]'>
                  </div>                                                            
                  
              </div>    
              <div class="col-md-4 mb-2 offset-4" style="margin-top: 25px">
                  <label class="required" for="aluno_codigo">Código do IIPCNET:</label>
                  <div class="row">
                    <input type="text" style="background-color: white; width: 50%" class="form-control offset-3" id="aluno_codigo" required="" placeholder="" value="" name="aluno_codigo">
                    <button class="btn btn-primary btn-sm btn_buscar" style="margin-left: 20px">Buscar</button>
                  </div>       
               </div>                
           </div>
        </form>
        </div>
        </div>
      </div>

      <?php include "rodape.php"; ?>
    </div>
  </body>
</html>




      