
<nav class="navbar navbar-default navbar-expand-sm navbar-light" style="background-color: #e8eaed">
    <ul class="navbar-nav">
          <li class="nav-item active" style="border-right: solid; border-color: #f9fcff; border-width: 0.3px">
               <a class="nav-link" href="configuracoes_home.php">Home</a>
          </li>         
          <li class="nav-item" style="border-right: solid; border-color: #f9fcff; border-width: 0.3px">
                <a class="nav-link" href="geralistas.php">Gerar Listas de TMK</a>
          </li>
          <li class="nav-item" style="border-right: solid; border-color: #f9fcff; border-width: 0.3px">
                <a class="nav-link" href="listar_alunos_contactados.php">Lista de Alunos Contactados</a>
          </li>
          <li class="nav-item" style="border-right: solid; border-color: #f9fcff; border-width: 0.3px">
               <a class="nav-link" href="controle_listas.php">Controle de Listas</a>
          </li>
          <li class="nav-item" style="border-right: solid; border-color: #f9fcff; border-width: 0.3px">
               <a class="nav-link" href="administracao_cursos.php">Administração de Turmas</a>
          </li>
    </ul>
</nav>