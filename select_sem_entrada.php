<?php

ob_start();
?>
select * from (
        SELECT DISTINCT a.codigo
            
                          
                                          
        FROM   (SELECT a.* 
                FROM   aluno a, ultima_ligacao u
                WHERE  UF = 'MG'
                AND CIDADE IN ('BELO HORIZONTE','CONTAGEM','NOVA LIMA', 'BETIM','SANTA LUZIA', 'SETE LAGOAS','BELO HORIZNTE','DIVINOPOLIS')
                       AND NOT EXISTS (SELECT 1 
                                       FROM   voluntario v 
                                       WHERE  v.codigo = a.codigo)   
                
                
              AND EXISTS (SELECT 1 
                           FROM   aluno_turma at, 
                                  turma t, 
                                  evento e 
                           WHERE  at.codigo_turma = t.codigo 
                                  AND e.id = t.id_evento
                                            AND at.CODIGO_ALUNO = a.CODIGO
                                  AND t.data_final BETWEEN '2016-06-01' AND '2019-01-01') 
                
                AND NOT EXISTS (SELECT 1 
                           FROM   aluno_turma at, 
                                  turma t, 
                                  evento e 
                           WHERE  at.codigo_turma = t.codigo 
                                  AND e.id = t.id_evento
                                  AND ( lower(e.nome) LIKE "cip %" 
                                         OR lower(e.nome) LIKE "cpc %" 
                                         OR lower(e.nome) LIKE "pacifismologia" 
                                         OR lower(e.nome) LIKE "assistenciologia"
                                                                           OR lower(e.nome) LIKE "ecp1%"                                
                                         OR lower(e.nome) LIKE "ecp2%") 
                                  AND at.codigo_aluno = a.codigo)
                
                AND NOT EXISTS (SELECT 1
                           FROM ultima_ligacao u
                           WHERE u.aluno_CODIGO = a.CODIGO
                           AND (u.data > DATE_SUB(curdate(), INTERVAL 3 WEEK)
                           OR u.data > curdate()))
                
                AND NOT EXISTS (SELECT 1
                           FROM ligacoes_tmk l
                           WHERE l.aluno_CODIGO = a.CODIGO
                           AND l.data > DATE_SUB(curdate(), INTERVAL 3 WEEK) )
                                  
        ) a ) alunos



<?php
$sql = ob_get_clean();
?>