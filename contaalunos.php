<?php
include "conectasql.php";
error_reporting(E_ALL);

$lista_selecionada = $_POST["lista_selecionada"];

$lista = $conexao->prepare("SELECT DISTINCT * FROM `select_lista` where id = ?");
$lista -> bind_param("i",$lista_selecionada);
$lista -> execute();
$lista_resultado = $lista ->get_result();

$res_lista = $lista_resultado -> fetch_assoc();
$sql = $res_lista['conteudo_select'];
  
// se for um select com parametros, executa assim  
if($res_lista['parametros'] != ""){  
	$sql = html_entity_decode($sql, ENT_QUOTES);
	$select = $conexao ->prepare($sql);
	$parametros = explode(";",$res_lista['parametros']);
	$tipo_parametros = array($res_lista['tipo_parametros']);

	if (!empty($res_lista['tipo_parametros'])) {
	    mysqli_stmt_bind_param($select, $res_lista['tipo_parametros'],...$parametros);
	}

	$select -> execute();
	$select_resultado = $select -> get_result();

}else{
	$select_resultado = $conexao ->query($sql);
}

$num_alunos_lista = mysqli_num_rows($select_resultado);

echo $num_alunos_lista;