<?php 
include "conectasql.php";
error_reporting(E_ALL);

$curso = $_POST["curso_selecionado"];


$turmas = $conexao->prepare("SELECT t.CODIGO,  DATE_FORMAT(t.DATA_INICIAL,'%d/%m/%Y') AS DATA_INICIAL, t.id_evento from turma t where t.DATA_INICIAL > CURDATE() AND t.id_evento = (?)");
$turmas -> bind_param("i", $curso);
$turmas -> execute();
$res_turmas = $turmas->get_result();
$turmas -> close();

$turmas = array();

$i = 0;
while ($n = $res_turmas -> fetch_assoc()) {
    $turmas[$i]["id"] = utf8_encode($n['CODIGO']);
    $turmas[$i]["nome"] = utf8_encode($n['DATA_INICIAL'])." - ".utf8_encode($n['CODIGO']);
    $i = $i + 1;
}

echo json_encode($turmas);

?>