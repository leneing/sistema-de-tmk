
<?php

include "conectasql.php";
$turma = $_POST["turma_selecionada"];

$listas_filtro = $conexao->prepare("SELECT l.* FROM `select_lista` l, turma t  WHERE l.fl_ativo = 1 AND l.id_turma = t.CODIGO AND t.CODIGO = (?)");
$listas_filtro -> bind_param("i",$turma);
$listas_filtro -> execute();
$listas_resultado = $listas_filtro ->get_result();
$listas = array();

$i = 0;
while ($n = $listas_resultado -> fetch_assoc()) {
    $listas[$i]["id"] = utf8_encode($n['id']);
    $listas[$i]["label"] = utf8_encode($n['nome_lista']);
    $listas[$i]["value"] = utf8_encode($n['nome_lista']);
    $i = $i + 1;
}


echo json_encode($listas);
?>