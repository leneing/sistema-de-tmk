<?php
error_reporting(E_WARNING);
include "conectasql.php";
session_start();

$listas_filtro = "SELECT s.id as id, s.nome_lista, DATE_FORMAT(s.data_criacao, '%d/%m/%Y') as data_criacao, s.id_turma, e.NOME as evento, s.fl_ativo FROM select_lista s INNER JOIN evento e on e.id = s.id_evento INNER JOIN turma t on t.CODIGO = s.id_turma AND t.DATA_INICIAL > CURDATE() ORDER BY s.fl_ativo DESC,s.data_criacao DESC";
$res_listas = $conexao ->query($listas_filtro);

?>

<HTML>

<HEAD>
        <TITLE>IIPC BH - TMK</TITLE>

        <link rel="stylesheet" href="jQuery-Plugin-For-Multi-Select-Checboxes-multiselect/css/jquery.multiselect.css">

        <link rel="stylesheet" href="bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="estilo.css">

        <script src="jquery-3.3.1.min.js" crossorigin="anonymous"></script>
        <script src="jQuery-Plugin-For-Multi-Select-Checboxes-multiselect/js/jquery.multiselect.js"></script>
        <script src="jquery-ui.js"></script>
        <script src="combobox.js"></script>


        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

        <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

        <style type="text/css">
            .form-center{   
                width: 20%;
                padding: .375rem .75rem;
                font-size: 1rem;
                line-height: 1.5;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                border-radius: .25rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            }
            .form-radius{  
                font-size: 1rem;
                line-height: 1.5;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                border-radius: .25rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            }

            fieldset.scheduler-border {   
                text-align: center;
                background-color: #f7f9fc;
                min-width: 200px;             
                padding: 10px !important;
                -webkit-box-shadow:  0px 0px 0px 0px #000;
                        box-shadow:  0px 0px 0px 0px #000;
            }
        </style>

        <script>
                function filtraTabela() {
                  // Declare variables
                  var input, filter, table, tr, td, i;
                  input = document.getElementById("filtroCursosFeitos");
                  filter = input.value.toUpperCase();
                  table = document.getElementById("tabela_cursos_feitos");
                  tr = table.getElementsByTagName("tr");

                  // Loop through all table rows, and hide those who don't match the search query
                  for (i = 0; i < tr.length; i++) {
                    td = tr[i].getElementsByTagName("td")[0];
                    if (td) {
                      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                      } else {
                        tr[i].style.display = "none";
                      }
                    }
                  }
                }               

                $(document).ready(function () {   

                    $(".btn_ativar").click(function () {
                        $.ajax({
                            url: 'resultadoControleListas.php',
                            type: 'post',
                            data: "lista_selecionada="+$(this).attr("lista_selecionada")+"&fl_ativo="+$(this).attr("fl_ativo"),
                            success: function(result_grava_banco){
                                location.reload();
                            }
                         });  
                          return false;
                    });

                    $(".btn_inativar").click(function () {
                        $.ajax({
                            url: 'resultadoControleListas.php',
                            type: 'post',
                            data: "lista_selecionada="+$(this).attr("lista_selecionada")+"&fl_ativo="+$(this).attr("fl_ativo"),
                            success: function(result_grava_banco){
                                location.reload();
                            }
                         });  
                          return false;
                    });  
                });
        </script>
</HEAD>

<BODY class="bg-light">
        <?php
            include "navbar_adm.php";
        ?> 
        <div class="container" style="margin-top: 15px;">
            <input type="hidden" id="aluno_codigo"/>
            <div class="text-center" id="div_titulo">       
                <img class="d-block mx-auto mb-4" src="Images/icolistapan.png" alt="" width="72" height="72">          
                <h2>Controle de Listas</h2><br/>
                <p class="lead">Permite ativar e desativar as listas de TMK</p>                                    
            </div>                                   
            
            <div class="row">
                <div class="col-md-12">                            
                    <form id="frm_resultado">  
                        <div class="row" style="text-align: center; margin-top:5px;">
                              <table class="table table-bordered table-hover" style="font-size: small">
                                <thead style="text-align: center">
                                <tr>
                                  <th></th>
                                  <th style="width: 8%">Data de criação</th>
                                  <th>Lista</th>
                                  <th>Evento</th>
                                  <th style="width: 6%">Turma</th>
                                </tr>
                                </thead>
                                <tbody> 
                                     <?php
                                        while ($linha_listas = $res_listas -> fetch_assoc()){
                                            if ($linha_listas['fl_ativo'] == 0) {
                                            ?>  <tr style="background-color: #e8e8e8">
                                            <?php
                                            }else{
                                            ?>  <tr style="background-color: #adf7aa">
                                            <?php
                                            }?> 
                                            <td scope="row" style="width: 8%">
                                                <?php
                                                if ($linha_listas['fl_ativo'] == 0) {
                                                ?>  <button class="btn btn-success btn-sm btn_ativar"
                                                lista_selecionada = "<?=$linha_listas['id']?>" fl_ativo ="1">Ativar</button>
                                                <?php
                                                }else{
                                                ?>  <button class="btn btn-danger btn-sm btn_inativar"  lista_selecionada = "<?=$linha_listas['id']?>" fl_ativo ="0">Inativar</button>
                                                <?php
                                                }?> 
                                            </td>
                                                <td><?=utf8_encode($linha_listas['data_criacao'])?></td>
                                                <td><?=utf8_encode($linha_listas['nome_lista'])?></td>
                                                <td><?=utf8_encode($linha_listas['evento'])?></td>
                                                <td><?=utf8_encode($linha_listas['id_turma'])?></td>
                                            </tr>
                                        <?php
                                            }
                                        ?>
                                </tbody>
                             </table>  
                        </div>
                    </form>         
            </div>
        </div>
        <script src="popper.min.js" crossorigin="anonymous"></script>
        <script src="bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>

      <?php include "rodape.php"; ?>
</BODY>

</HTML>