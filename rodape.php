

<script>
  $(document).ready(function () {
    $("#manual").click(function () {
      window.open("documentos/ManualdoUsuario.pdf", "Manual - Sistema de TMK", "fullscreen=yes");                   
    });
  });
</script>

      <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">&copy; 2018 IIPC Belo Horizonte</p>
        <ul class="list-inline">
          <li class="list-inline-item" id="manual"><a href="#">Manual</a></li>
          <li class="list-inline-item"><a href="#">Suporte</a></li>
        </ul>
      </footer>