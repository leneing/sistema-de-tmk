<?php
include "conectasql.php";
include "Selects/queries.php";
session_start();


$tipo_parametros = "";
$parametros = "";
//var_dump($_POST);
//var_dump($_POST["data_inicial"]);
//var_dump($_POST["data_fim"]);

$sql_query = "select * from (
	        			SELECT DISTINCT a.codigo	
	        				FROM   (SELECT a.*
										FROM  fecha_venda f INNER join ligacoes_tmk l on l.id = f.id_ligacao_origem INNER JOIN turma t on t.CODIGO = f.id_turma INNER JOIN aluno a on a.codigo = l.aluno_CODIGO
										WHERE l.data BETWEEN (?) AND (?) 
										AND t.ID_EVENTO = ?
									       AND t.DATA_INICIAL >= curdate()
									                AND NOT EXISTS (SELECT 1
									        	FROM ligacoes_tmk l1
									            WHERE l1.aluno_CODIGO = l.aluno_CODIGO
									            AND (l1.data > DATE_SUB(curdate(), INTERVAL 1 DAY))) ) a ) alunos ";
	
$srtdti = $_POST["data_inicial"] . " 00:00:00";
$srtdte = $_POST['data_fim'] . " 23:59:00";

$srtdti = str_replace('/', '-', $srtdti);
$dti = date('Y-m-d H:i:s', strtotime($srtdti));	

$srtdte = str_replace('/', '-', $srtdte);
$dte = date('Y-m-d H:i:s', strtotime($srtdte));

$tipo_parametros .= "sss";
$parametros .= $dti . ";";
$parametros .= $dte . ";";
$parametros .= $_POST['select_curso_interesse'][0];


$evento = $_POST['select_evento'][0];
$nome_lista = utf8_decode($_POST["nome_lista"]);
	
$sql = $conexao->prepare("INSERT INTO select_lista (nome_lista, conteudo_select, tipo_parametros, parametros, data_criacao, id_evento, id_turma, fl_ativo, descricao) VALUES (?, ?, ?, ?, now(), ?, ?, 1, ?)");  

$sql ->bind_param("ssssiss",$nome_lista,$sql_query,$tipo_parametros,$parametros, $evento, $_POST['turma_lista'], $_POST['descricao']); 
//var_dump($sql);
$res = $sql->execute();

echo mysqli_error($conexao);
$sql->close();

//echo $sql_query_completa;
//echo $tipo_parametros;
//echo $parametros;
