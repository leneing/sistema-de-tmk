<?php
include "conectasql.php";
session_start();

$ligacoes_filtro = "SELECT DATE_FORMAT(data,'%d/%m/%Y') as data, 
  DATE_FORMAT(data,'%H:%i') as hora,
    e.NOME as nmevento,
    e.id as id_evento,
  a.NOME as nmaluno,
  l.aluno_CODIGO as aluno_CODIGO,
    v.nome as nmvoluntario,
    comentarios,
    DATE_FORMAT(l.data_retorno, '%d/%m/%Y - %H:%i') as data_retorno
  from ligacoes_tmk l INNER JOIN aluno a on a.CODIGO = l.aluno_CODIGO
    inner join evento_ativo e on e.id = l.evento_id
    inner join sessaotmk s on s.id = l.sessaotmk_id
    inner join voluntario v on v.codigo = s.voluntario_id    
  WHERE fl_pendente_retorno = 1 AND data_retorno > curdate()
  order by data_retorno";



$res_ligacoes_pendentes = $conexao ->query($ligacoes_filtro);

?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>IIPC BH - TMK - Ligações Pendentes</title>

        <link rel="stylesheet" href="bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="jquery-ui.css">
        <script src="jquery-3.3.1.min.js" crossorigin="anonymous"></script>
        <script src="jquery-ui.js"></script>
        <link href="gijgo.min.css" rel="stylesheet" type="text/css"/>
        <script src="gijgo.min.js" type="text/javascript"></script>
        <script src="combobox.js"></script>
        
        <link rel="stylesheet" href="estilo.css">
    <link href="form-validation.css" rel="stylesheet">

    <script>
      $(document).ready(function () { 
        $(".btn_ligar").click(function () {
          location.href = "redirecionaComCodigoAluno.php?aluno_CODIGO="+$(this).attr("aluno_CODIGO")+"&curso_selecionado="+$(this).attr("curso_selecionado")+"&tela_origem=ligacoesPendentes.php"; 

          return false;
        });
      });
    </script>
  </head>

  <body class="bg-light">

    <?php
            include "navbar.php";
        ?> 

    <div class="container">
      <div class="col-md-12">
      <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="Images/icoligpen.png" alt="" width="72" height="72">
        <h2>Ligações Pendentes</h2>
        <p class="lead">Verifique quando o aluno pediu para retornar a ligação antes de ligar.</p>
      </div>

      <div class="row">
        <form id="frm_ligacoes">
          <div class="row">
            <table class="table table-bordered table-hover" style="font-size: small">
            <thead style="text-align: center">
            <tr>
              <th></th>
              <th>Data da Ligação</th>
              <th>Curso a divulgar</th>
              <th>Aluno</th>
              <th>Voluntário que ligou</th>
              <th>Comentário</th>
              <th>Ligar de novo em</th>
            </tr>
            </thead>
            <tbody>
               <?php
                  while ($linha_lig = $res_ligacoes_pendentes -> fetch_assoc()){                    
                ?> 
                    <tr>
                      <th scope="row">
                        <button class="btn btn-primary btn-md btn_ligar" curso_selecionado = "<?=$linha_lig['id_evento']?>" 
                        aluno_CODIGO = "<?=$linha_lig['aluno_CODIGO']?>">Ligar</button>
                      </th>
                      <td><?=utf8_encode($linha_lig['data'])?> - <?=utf8_encode($linha_lig['hora'])?></td>
                      <td><?=utf8_encode($linha_lig['nmevento'])?></td>
                      <td><?=utf8_encode($linha_lig['nmaluno'])?></td>
                      <td><?=utf8_encode($linha_lig['nmvoluntario'])?></td>
                      <td><?=utf8_encode($linha_lig['comentarios'])?></td>
                      <td><?=utf8_encode($linha_lig['data_retorno'])?></td>
                </tr>
              <?php
                  }
              ?>
            </tbody>
          </table>
        </form>
        </div>
      </div>

      <?php include "rodape.php"; ?>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../../../assets/js/vendor/popper.min.js"></script>
    <script src="../../../../dist/js/bootstrap.min.js"></script>
    <script src="../../../../assets/js/vendor/holder.min.js"></script>
    <script>
      // Example starter JavaScript for disabling form submissions if there are invalid fields
      (function() {
        'use strict';

        window.addEventListener('load', function() {
          // Fetch all the forms we want to apply custom Bootstrap validation styles to
          var forms = document.getElementsByClassName('needs-validation');

          // Loop over them and prevent submission
          var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
              if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
              }
              form.classList.add('was-validated');
            }, false);
          });
        }, false);
      })();
    </script>
  </body>
</html>




      